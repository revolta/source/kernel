/*
 * stack_allocator.hpp
 *
 *  Created on: 11. 7. 2020
 *      Author: martin
 */

#pragma once

#include "cstddef.hpp"

namespace ReVolta::Kernel {

	/* TODO: ALLOCATORS MUST BE STATELESS */

	/* TODO: In order to implement the shared storage common to all the StackAllocators the const reference to an object
	 * might be used as template parameter once the reference is to static object instance.
	 * This might work but then the storage is common to ALL instances, maybe existing in different stacks of different threads and so...
	 * --> how to identify the stack to which the storage belongs? TID?, Stack address (maybe known at compile time - template argument?)?
	 * --> Or use thread local storage somehow (https://wiki.osdev.org/Thread_Local_Storage)?
	 */

	/* Inspired by Howard Hinnant's stack allocator (http://howardhinnant.github.io/stack_alloc.h) */
	template<typename T, size_t CAPACITY>
	class StackAllocator {
	public:
		using value_type = T;
		using pointer_type = value_type *;
		using size_type = size_t;

		constexpr StackAllocator( void ) noexcept : m_ptr( reinterpret_cast<T *>( m_storage ) ) {}

		StackAllocator( const StackAllocator & ) noexcept : m_ptr( reinterpret_cast<T *>( m_storage ) ) {}

		template<typename U>
		StackAllocator( const StackAllocator<U, CAPACITY> & ) noexcept : m_ptr( reinterpret_cast<T *>( m_storage ) ) {}

		template<typename U>
		struct Rebind {
			using other = StackAllocator<U, CAPACITY>;
		};

		T * allocate( size_t n, const T * hint = nullptr ) noexcept {
			/* Check whether there is still enough space to allocate 'n' instances of T.
			 * If so return internaly tracked pointer and move it by 'n' newly allocated instances */
			if( reinterpret_cast<T *>( m_storage ) + CAPACITY - m_ptr >= n ) {
				T * retval = m_ptr;
				m_ptr += n;
				return retval;
			}
#if false
			/* TODO: Optionaly remove as heap dynamic allocation is forbidden */
			/* TODO: Check this - this is weird - how to allocate on stack while using new?
			 * Isn't it just backup solution once the required amount 'n' of instances of 'T' do not fit to the storage? */
			return static_cast<T *>(::operator new( n * sizeof(T) ));
#endif
		}

		template<size_t N>
		T * allocate( void ) noexcept {
			return allocate( N );	/* TODO: Worst case or it shall be as simple as that? */
		}

		void deallocate( T * ptr, size_t n ) noexcept {
			if( reinterpret_cast<T *>( m_storage ) <= ptr && ptr < reinterpret_cast<T *>( m_storage ) + CAPACITY ) {
				if( ptr + n == m_ptr ) {
					m_ptr = ptr;
				}
			}
#if false
			/* TODO: Optionaly remove as heap dynamic allocation is forbidden */
			else {
				::operator delete( ptr );
			}
#endif
		}

		template<size_t N>
		void deallocate( T * ptr ) noexcept {
			deallocate( ptr, N );	/* TODO: Worst case or it shall be as simple as that? */
		}

		bool operator==( StackAllocator & other ) const noexcept {
			return ( m_storage == other.m_storage );
		}

		bool operator!=( StackAllocator & other ) const noexcept {
			return ( m_storage != other.m_storage );
		}

	private:
		StackAllocator & operator=( const StackAllocator & ) = delete;

		/* Create aligned storage big enough to hold the maximum of 'CAPACITY' instances of 'T' */
		alignas( alignof(T) ) byte m_storage [ sizeof(T) * CAPACITY ];

		T * m_ptr;
	};

}
