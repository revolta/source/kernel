set( TARGET_MEMORY "memory" )

add_library( ${TARGET_MEMORY} STATIC )

target_include_directories( ${TARGET_MEMORY}
INTERFACE
	${CMAKE_CURRENT_LIST_DIR}
)

target_sources( ${TARGET_MEMORY}
PUBLIC
#	${CMAKE_CURRENT_LIST_DIR}/stack_allocator.hpp
)

target_link_libraries( ${TARGET_MEMORY}
PUBLIC
	memory-${CMAKE_TARGET_PROCESSOR}	# Architecture specific memory
PRIVATE
	kernel-build-config					# Kernel configuration 
)