#pragma once

#include "cstddef.hpp"
#include "memory_pool.hpp"
#include "forward_list.hpp"
#include "config.hpp"

namespace ReVolta::Kernel {

    class thread {
    public:
        using tid_type = size_t;

        tid_type get_tid( void ) const noexcept {
            return m_tid;
        }

    private:
        tid_type m_tid;
    };

}

namespace ReVolta {

    /* Specialize the memory pool traits for kernel thread management */
    template<>
    struct memory_pool_traits<forward_list_node<Kernel::thread>> {
        using element_type = forward_list_node<Kernel::thread>;
        static constexpr size_t capacity { Kernel::max_thread_count };
    };

    /* Specialize default allocator for kernel thread usage */
    /* TODO: Check how the default allocator is used and whether it is possible to switch one */
    template<>
    struct default_allocator<Kernel::thread> {
        using type = pool_allocator<Kernel::thread>;
    };

}