#include "thread.hpp"

namespace ReVolta {

    namespace Kernel {

        memory_pool<forward_list_node<thread>> thread_storage;

    }
    
    template<>
    memory_pool<forward_list_node<Kernel::thread>> & pool_allocator<forward_list_node<Kernel::thread>>::get_pool( void ) noexcept {
        return Kernel::thread_storage;
    }

}