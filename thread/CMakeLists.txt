set( TARGET_THREAD "thread" )

add_library( ${TARGET_THREAD} STATIC )

target_include_directories( ${TARGET_THREAD}
PUBLIC
    $<$<COMPILE_LANGUAGE:C,CXX>:${CMAKE_CURRENT_LIST_DIR}>
)

target_sources( ${TARGET_THREAD}
PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/thread.hpp
PRIVATE
#    ${CMAKE_CURRENT_LIST_DIR}/thread.cpp
)

target_link_libraries( ${TARGET_THREAD}
PRIVATE
    config
    kernel-build-config
)