#pragma once

#include "cpu.hpp"

#include <list.hpp>

namespace ReVolta::Kernel {

    /* Generic symmetric multiprocessing (SMP) for generic cpu type */
    class symmetric_multiprocessing {
    public:
        /* Allocator to allocate the instances of generic 'cpu_type' */
        using cpu_allocator = pool_allocator<Kernel::cpu_type>;
        /* The pointer type pointing to particular instance of 'cpu_type' allocated using 'cpu_allocator' */
        using cpu_pointer = unique_ptr<cpu_type, deleter<cpu_allocator>>;
        /* The container type in which the pointers to particular CPUs are initially collected */
        using cpu_pointer_container_type = list<cpu_pointer, pool_allocator<cpu_pointer>>;
        /* Container element storage type i.e. list nodes */
        using cpu_pointer_container_storage_type = cpu_pointer_container_type::node_type;

    private:
        /* Container holding the pointers to all allocated CPUs available in the system */
        cpu_pointer_container_type m_available_cores;

    public:

        /* Default constructor - allocates the bootstrap (BSP) processor and wakes up all the APs */
        symmetric_multiprocessing( void ) noexcept;

        /* Return the number of available CPU cores initialized but unassigned to process */
        size_t available_cpu_count( void ) const noexcept {
            return this->m_available_cores.size();
        }

        /* Return the maximum number of CPU cores supported */
        size_t max_cpu_count( void ) const noexcept {
            return this->m_available_cores.max_size();
        }

    };

}

