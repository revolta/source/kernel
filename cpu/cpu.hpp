#pragma once

/* TODO: Get rid of such arch specific stuff */
#include "x86_64.hpp"

namespace ReVolta::Kernel {

    namespace Interface {

        template<typename T>
        concept Cpu = requires ( T core ) {
            core.is_bootstrap();
            /* TODO: Extend the CPU interface */
        };

    }

    /* Generic CPU implementation based on any CPU implementing the 'Interface::Cpu' */
    template<Interface::Cpu CPU>
    class cpu : public CPU {};

    /* TODO: Get rid of such arch specific stuff */
    using cpu_type = cpu<x86>;

}
