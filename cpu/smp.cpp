#include "smp.hpp"

#include "acpi.hpp"

#include <memory_pool.hpp>

namespace ReVolta {

    /* Setup the memory pool to store the CPU cores */
    template<>
    struct memory_pool_traits<Kernel::cpu_type> {
        using element_type = Kernel::cpu_type;
        /* FIXME: Replace hard coded value */
        static constexpr size_t capacity { 16 };
    };

    /* Setup the memory pool to store the core pointers */
    template<>
    struct memory_pool_traits<Kernel::symmetric_multiprocessing::cpu_pointer_container_storage_type> {
        using element_type = Kernel::symmetric_multiprocessing::cpu_pointer_container_storage_type;
        /* FIXME: Replace hard coded value */
        static constexpr size_t capacity { 16 };
    };

    /* TODO: Try to make the storage the member of SMP class itself rather than a global variable */
    /* Define the core storage itself */
    memory_pool<Kernel::cpu_type> cpu_core_storage;

    /* Link the previously defined core storage to the pool allocator used as the default one */
    template<>
    memory_pool<Kernel::cpu_type> & pool_allocator<Kernel::cpu_type>::get_pool( void ) noexcept {
        return cpu_core_storage;
    }

    /* TODO: Try to make the storage the member of SMP class itself rather than a global variable */
    memory_pool<Kernel::symmetric_multiprocessing::cpu_pointer_container_storage_type> cpu_pointer_collection_storage;

    template<>
    memory_pool<Kernel::symmetric_multiprocessing::cpu_pointer_container_storage_type> & pool_allocator<Kernel::symmetric_multiprocessing::cpu_pointer_container_storage_type>::get_pool( void ) noexcept {
        return cpu_pointer_collection_storage;
    }

    namespace Kernel {


/* The full "AP CPU startup" sequence might go like this:
 *  - BSP allocates stack for AP CPU, and stores the "top of stack" in the trampoline
 *  - BSP sends INIT IPI to AP CPU
 *  - BSP waits for 10 ms
 *  - BSP sends first SIPI IPI to AP CPU
 *  - BSP checks an "AP CPU started" flag in a loop, with a 200 us timeout
 *  If "AP CPU started" flag was not set:
 *      - BSP sends second SIPI IPI to AP CPU
 *      - BSP checks an "AP CPU started" flag in a loop, with a much longer timeout (e.g. 1 second)
 *  If "AP CPU started" flag was not set:
 *      - BSP reports "AP CPU failed to start" error and gives up on this AP
 *      - BSP sets a "AP can continue" flag (so that AP CPU knows BSP has noticed that it started)
 *      - BSP waits for AP to set an "AP ready" flag (or increment a "number of CPUs" counter), 
 *        so that it knows the AP CPU doesn't need the trampoline any more (and that it's safe start the next AP CPU).
 * 
 * The AP CPU goes a little like this:
 *  - AP sets the "AP CPU started" flag to tell BSP its running
 *  - AP waits for BSP to set the "AP can continue" flag
 *  - AP switches to protected mode or long mode (possibly including enabling paging)
 *  - AP loads its stack from the trampoline
 *  - AP sets an "AP ready" flag (or increments a "number of CPUs" counter), so BSP knows its finished using the trampoline
 */

        symmetric_multiprocessing::symmetric_multiprocessing( void ) noexcept {
            /* Emplace the CPU core which performs the symmetrical multiprocessing initialization (construction). Usually the bootstrap CPU core */
            this->m_available_cores.emplace_front( allocate_unique<cpu_type>( cpu_allocator{} ) );

            /* TODO: Once the BSP processor is initialized, wake up all the other processor cores (the APs - Application Processors) */
        }

    }

}