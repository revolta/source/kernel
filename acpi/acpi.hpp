#pragma once

#include <cstdint.hpp>
#include <memory.hpp>

#include "madt.hpp"
#include "fadt.hpp"

namespace ReVolta::Kernel {

    namespace Detail {

        /* TODO: Derive both RSDT and XSDT out of common base type as the only difference is the 32bit vs 64bit pointers */

        /* Root System Description Table - Used in 32bit environment for ACPI v1.0 */
        class rsdt : public system_description_table {
        public:
            constexpr static const char * rsdt_signature { "RSDT" };

            using size_type = size_t;

            /* The RSDT table stores the array of system description table header pointers */
            using table_header_pointer = const system_description_table *;

            /* Right after the system description table header (SDT header) there comes an contiguous memory of SDT headers.
             * So to iterate through all the tables is the iteration through this array of pointers */
            using const_iterator = const table_header_pointer *;

        private:
            /* Pointer to the first SDT header in row. It represents the dynamic array of table pointers which size is defined
             * in RSDT header and available using 'this->size()'.
             * Is declared as pointer to constant table header so that the content cannot be modified (is read only) */
            table_header_pointer m_tables;

        public:
            /* Returns the iterator to the first ACPI table */
            const_iterator begin( void ) const noexcept {
                return const_iterator { addressof( m_tables ) };
            }

            const_iterator end( void ) const noexcept {
                return const_iterator{ addressof( m_tables ) + this->size() };
            }

            constexpr size_type size( void ) const noexcept {
                return ( this->m_length - sizeof( system_description_table ) ) / sizeof( uint32_t );
            }

        };

        /* Extended Root System Description Table - Used in 64bit environment for ACPI v2.0 and above */
        class xsdt : public system_description_table {
            constexpr static const char * xsdt_signature { "XSDT" };
            /* TODO */
        } __attribute__(( packed ));

        template<size_t BOUNDARY>
        struct alignas( BOUNDARY ) boundary_type {};

        /* ACPI RSDP data structure. Implements SINGLETON design pattern */
        /* RSDP is mapped at 16byte boundary */
        class rsdp : private boundary_type<16> {
            constexpr static const char * rsdp_signature { "RSD PTR " };

            /* ACPI v1.0 root system descriptor */
            struct rsdp_descriptor {
                char            m_signature [ 8 ];        /* Must contain "RSD PTR ". It stands on 16-byte boundary */
                uint8_t         m_checksum;
                char            m_oem_id [ 6 ];
                uint8_t         m_revision;
                uint32_t        m_rsdt_address;
            } __attribute__(( packed ));

            /* ACPI v2.0 and above root system descriptor */
            struct rsdp_descriptor_20 : public rsdp_descriptor {
                uint32_t        m_length;
                uint64_t        m_xsdt_address;
                uint8_t         m_extended_checksum;
                uint8_t         m_reserved [ 3 ];
            } __attribute__(( packed ));

            rsdp_descriptor_20 m_descriptor;

            constexpr rsdp( void ) {}

            inline bool check_signature( void ) const {
                return equal( ( this->m_descriptor ).m_signature, ( this->m_descriptor ).m_signature + 7, rsdp_signature );
            }

            /* Validate the RSDP found by static get() function */
            [[nodiscard]] bool is_valid( void ) const {
                size_t sum { 0 };
                const unsigned char * it { reinterpret_cast<const unsigned char *>( addressof( this->m_descriptor ) ) };
                size_t count { ( ( this->m_descriptor ).m_revision == 0 ) ? sizeof( rsdp_descriptor ) : sizeof( rsdp_descriptor_20 ) };
                while( count --> 0 ) {
                    sum += *it++;
                }
                return ( sum & 0xFF ) == 0;
            }

            static observer_ptr<rsdp> & get( void );

        public:
            constexpr rsdp( const rsdp & ) = delete;

            constexpr rsdp & operator=( const rsdp & ) = delete;

            static const rsdt & get_rsdt( void ) {
                return *( reinterpret_cast<const rsdt *>( ( rsdp::get()->m_descriptor ).m_rsdt_address ) );
            }

        } __attribute__(( packed )); 

    }  

    /* Concept used to constrain the types representing the ACPI table */
    template<typename T>
    concept ACPITable = 
        /* The ACPI table shall be derived from system description table containing the mandatory header */
        is_base_of_v<system_description_table, T> &&
        requires( T table ) {
            /* Shall define the signature */
            { T::signature };
        }
    ;

    /* ACPI is mapped by the FW uniquely to memory. Uses SINGLETON design pattern */
    class acpi {
    public:
        using size_type = size_t;

    private:
        /* The ACPI tables shall be located within the constructor once the acpi instance is accessed by get() for the first time */
        acpi( void ) {}

    public:
        acpi( const acpi & ) = delete;

        acpi & operator=( const acpi & ) = delete;

        static inline version_t version( void ) noexcept {
            return acpi::table<fadt>()->acpi_version();
        }

        template<ACPITable TABLE>
        static observer_ptr<const TABLE> table( void ) {
            observer_ptr<const TABLE> retval { nullptr };
            /* Iterate over all the tables and search for the one identified by the signature */
            for( Detail::rsdt::table_header_pointer table : Detail::rsdp::get_rsdt() ) {
                if( table->check_signature( TABLE::signature ) && ( table->is_valid() ) ) {
                    retval = make_observer<const TABLE>( table );
                    break;
                }
            }
            return retval;
        }

    };

}