#include "acpi.hpp"

#include <algorithm.hpp>
#include <memory.hpp>
#include <span.hpp>

namespace ReVolta::Kernel {

    namespace Detail {

        extern "C" boundary_type<16> ACPI_start;
        extern "C" boundary_type<16> ACPI_end;

        observer_ptr<rsdp> & rsdp::get( void ) {
            static observer_ptr<rsdp> root_system_descriptor_pointer { nullptr };
            /* Search for RSDP and set 'root_system_descriptor_pointer' once found */
            if( root_system_descriptor_pointer == nullptr ) {
                /* Make the given memory range iterable */
                span<boundary_type<16>> mapping { &ACPI_start, &ACPI_end };
                /* Search for RSDP signature in the defined memory mapping range */
                for( Detail::boundary_type<16> & ptr : mapping ) {
                    if( ( static_cast<rsdp *>( &ptr ) )->check_signature() && ( static_cast<rsdp *>( &ptr ) )->is_valid() ) {
                        root_system_descriptor_pointer = make_observer<rsdp>( static_cast<rsdp *>( &ptr ) );
                        break;
                    }
                }
            }       
            return root_system_descriptor_pointer;
        }

    }

}