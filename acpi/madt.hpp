#pragma once

#include "system_description_table.hpp"

#include <cstddef.hpp>
#include <cstdint.hpp>
#include <iterator.hpp>
#include <memory.hpp>

namespace ReVolta::Kernel {

    /* TODO: Optionally, move it to somewhere else */
    /* TODO: Add descriptions */
    using acpi_processor_uid_t = named_type<uint8_t, struct acpi_processor_uid_tag>;

    /* The processor's local APIC ID */
    using apic_id_t = named_type<uint8_t, struct apic_id_tag>;

    /* The I/O APIC ID */
    using io_apic_id_t = named_type<uint8_t, struct io_apic_id_tag>;

    /* Global system interrupts can be thought as ACPI Plug'n'play IRQ numbers. They are used to virtualize interrupts in tables.
     * In ACPI interrupt model (in ACPI enabled systems) the number of interrupt inputs supported by each I/O APIC can vary.
     * The mapping of Global System Interrupts is determined by defining how many each I/O APIC supports using it's global system interrupt base
     * within the I/O APIC structure ('interrupt_controller_structure<interrupt_controller_structure_base::type::IO_APIC>') */
    using global_system_interrupt_t = named_type<uint32_t, struct global_system_interrupt_tag>;

    /*  */
    using bus_relative_interrupt_t = named_type<uint8_t, struct bus_relative_interrupt_tag>;

    /* Forward declarations due to pointer to such class instances used within MADT */
    class xapic;
    class io_apic;

    /* TODO: Try to change the name here to make it shorter */
    class interrupt_controller_structure_base {
    public:
        enum class type : uint8_t {
            PROCESSOR_LOCAL_APIC                                = 0x00,     /* Supported */
            IO_APIC                                             = 0x01,     /* Supported */
            INTERRUPT_SOURCE_OVERRIDE                           = 0x02,     /* Supported */
            NMI_SOURCE                                          = 0x03,     /* Supported */
            LOCAL_APIC_NMI                                      = 0x04,     /* Supported */
            LOCAL_APIC_ADDRESS_OVERRIDE                         = 0x05,
            IO_SAPIC                                            = 0x06,
            LOCAL_SAPIC                                         = 0x07,
            PLATFORM_INTERRUPT_SOURCES                          = 0x08,
            PROCESSOR_LOCAL_X2APIC                              = 0x09,
            LOCAL_X2APIC_NMI                                    = 0x0A,
            GIC_CPU_INTERFACE                                   = 0x0B,
            GIC_DISTRIBUTOR                                     = 0x0C,
            GIC_MSI_FRAME                                       = 0x0D,
            GIC_REDISTRIBUTOR                                   = 0x0E,               
            GIC_INTERRUPT_TRANSLATION_SERVICE                   = 0x0F,
            MULTIPROCESSOR_WAKEUP                               = 0x10,     /* Supported */
            CORE_PROGRAMMABLE_INTERRUPT_CONTROLLER              = 0x11,
            LEGACY_IO_PROGRAMMABLE_INTERRUPT_CONTROLLER         = 0x12,
            HYPERTRANSPORT_PROGRAMMABLE_INTERRUPT_CONTROLLER    = 0x13,
            EXTEND_IO_PROGRAMMABLE_INTERRUPT_CONTROLLER         = 0x14,
            MSI_PROGRAMMABLE_INTERRUPT_CONTROLLER               = 0x15,
            BRIDGE_IO_PROGRAMMABLE_INTERRUPT_CONTROLLER         = 0x16,
            LOW_PIN_COUNT_PROGRAMMABLE_INTERRUPT_CONTROLLER     = 0x17,
            ANY                                                 = 0xFF      /* This type is not defined by ACPI, but is used to iterate over any type of MADT entry */
        };

    protected:
        /* Make the default constructor protected in order to avoid this base structure standalone construction */
        constexpr interrupt_controller_structure_base( void ) {}

    private:
        uint8_t m_type;
        uint8_t m_length;       /* Record length in bytes */

    public:
        const interrupt_controller_structure_base * next( void ) const noexcept {
            /* Calculate consecutive base structure pointer */
            return bit_cast<const interrupt_controller_structure_base *>( bit_cast<const uint8_t *>( this ) + this->m_length );
        }

        type entry_type( void ) const noexcept {
            return static_cast<type>( this->m_type );
        }

    } __attribute__(( packed ));


    enum class enable_state : uint8_t {
        UNUSABLE,
        ENABLED,            /* The processor is ready for use */
        ONLINE_CAPABLE      
    };

    /* Generic interrupt controller structure just wrapping it's base (capable of just providing the entry type and the pointer to the next one) */
    template<interrupt_controller_structure_base::type T>
    class interrupt_controller_structure : public interrupt_controller_structure_base {};

    /* PROCESSOR LOCAL APIC */
    /* Represents a single logical processor and its local interrupt controller. The length of this entry is 8 */
    template<>
    class interrupt_controller_structure<interrupt_controller_structure_base::type::PROCESSOR_LOCAL_APIC> : public interrupt_controller_structure_base {
    public:
        static constexpr interrupt_controller_structure_base::type type { interrupt_controller_structure_base::type::PROCESSOR_LOCAL_APIC };

    private:
        class local_apic_flags {
        private:
            bool    m_enabled           : 1;    /* If this bit is set the processor is ready for use. */
            bool    m_online_capable    : 1;
            uint32_t                    : 30;

        public:
            inline enable_state is_enabled( void ) const {
                if( m_enabled == true ) 
                    return enable_state::ENABLED;
                else if( m_online_capable == true )
                    return enable_state::ONLINE_CAPABLE;
                else 
                    return enable_state::UNUSABLE;
            }

        } __attribute__(( packed ));

        uint8_t             m_acpi_processor_uid;
        uint8_t             m_apic_id;
        local_apic_flags    m_flags;

    public:
        acpi_processor_uid_t processor_id( void ) const noexcept {
            return acpi_processor_uid_t{ this->m_acpi_processor_uid };
        }

        apic_id_t apic_id( void ) const noexcept {
            return apic_id_t{ this->m_apic_id };
        }

        enable_state is_enabled( void ) const noexcept {
            return ( this->m_flags ).is_enabled();
        }

    } __attribute__(( packed ));

    /* IO APIC */
    /* This type represents a I/O APIC. The length of this entry is 12 */
    template<>
    class interrupt_controller_structure<interrupt_controller_structure_base::type::IO_APIC> : public interrupt_controller_structure_base {
    public:
        static constexpr interrupt_controller_structure_base::type type { interrupt_controller_structure_base::type::IO_APIC };

    private:
        uint8_t         m_io_apic_id;
        uint8_t : 8;    
        uint32_t        m_io_apic_address;
        uint32_t        m_gloal_system_interrupt_base;

    public:
        /* I/O APIC's ID */
        io_apic_id_t io_apic_id( void ) const noexcept {
            return io_apic_id_t{ this->m_io_apic_id };
        }

        /* Physical 32bit address ot access this I/O APIC. Each I/O APIC resides at a unique address */
        io_apic * io_apic_address( void ) const noexcept {
            return reinterpret_cast<io_apic *>( this->m_io_apic_address );
        }

        /* Global system interrupt base is the first interrupt number that this I/O APIC handles  The number of interrupt inputs
         * is determined bz the I/O APIC's Max Redir Entry register */
        global_system_interrupt_t global_system_interrupt_base( void ) const noexcept {
            return global_system_interrupt_t{ this->m_gloal_system_interrupt_base };
        }

    } __attribute__(( packed ));

    enum class polarity : uint8_t {
        BUS_SPECIFICATION   = 0b00,
        ACTIVE_HIGH         = 0b01,
        ACTIVE_LOW          = 0b11
    };

    enum class trigger_mode : uint8_t {
        BUS_SPECIFICATION   = 0b00,
        EDGE_TRIGGERED      = 0b01,
        LEVEL_TRIGERRED     = 0b11
    };

    namespace Detail {

        class mps_inti_flags {
            uint8_t m_polarity      : 2;
            uint8_t m_trigger_mode  : 2;
            uint16_t                : 12;

        public:
            inline polarity input_signal_polarity( void ) const noexcept {
                return static_cast<polarity>( this->m_polarity );
            }

            inline trigger_mode input_signal_trigger_mode( void ) const noexcept {
                return static_cast<trigger_mode>( this->m_trigger_mode );
            }

        } __attribute__(( packed ));

    }

    /* Interrupt source overrides are necessary to describe variances between the IA-PC standard dual 8259 interrupt definition and the platform's implementation.
     * The specification only supports overriding ISA interrupt sources.
     * For example, if the machine has the ISA Programmable Interrupt Timer (PIT) connected to ISA IRQ 0, but in APIC mode it is connected to I/O APIC interrupt
     * input 2, then you would need an Interrrupt Source Override where the source entry is '0' and Global System Interrupt is '2'.
     * Interrupt Source Overrides are also necessary when an identity mapped interrupt has a non-standard polarity */
    template<>
    class interrupt_controller_structure<interrupt_controller_structure_base::type::INTERRUPT_SOURCE_OVERRIDE> : public interrupt_controller_structure_base {
    public:
        static constexpr interrupt_controller_structure_base::type type { interrupt_controller_structure_base::type::INTERRUPT_SOURCE_OVERRIDE };

    private:
        uint8_t                 m_bus;                      /* '0' Constant. Meaning ISA */
        uint8_t                 m_source;                   /* Bus-relative interrupt source (IRQ) */
        uint32_t                m_global_system_interrupt;  /* The Global System Interrupt that this bus-relative interrupt source will signal */
        Detail::mps_inti_flags  m_flags;                    /* MPS INTI flags */
    
    public:
        bus_relative_interrupt_t source( void ) const noexcept {
            return bus_relative_interrupt_t{ this->m_source };
        }

        global_system_interrupt_t global_system_interrupt( void ) const noexcept {
            return global_system_interrupt_t{ this->m_global_system_interrupt };
        }

        polarity input_signal_polarity( void ) const noexcept {
            return this->m_flags.input_signal_polarity();
        }

        trigger_mode input_signal_trigger_mode( void ) const noexcept {
            return this->m_flags.input_signal_trigger_mode();
        }

    } __attribute__(( packed ));

    /* NON-MAKABLE INTERRUPT (NMI) SOURCE STRUCTURE */
    /* This structure allows a platform designer to specify which I/O (S)APIC interrupt inputs should be enabled as non-maskable. */
    template<>
    class interrupt_controller_structure<interrupt_controller_structure_base::type::NMI_SOURCE> : public interrupt_controller_structure_base {
    public:
        static constexpr interrupt_controller_structure_base::type type { interrupt_controller_structure_base::type::NMI_SOURCE };

    private:
        Detail::mps_inti_flags  m_flags;
        uint32_t                m_global_system_interrupt;

    public:
        global_system_interrupt_t global_system_interrupt( void ) const noexcept {
            return global_system_interrupt_t{ this->m_global_system_interrupt };
        }

        polarity input_signal_polarity( void ) const noexcept {
            return this->m_flags.input_signal_polarity();
        }

        trigger_mode input_signal_trigger_mode( void ) const noexcept {
            return this->m_flags.input_signal_trigger_mode();
        }

    } __attribute__(( packed ));

    enum class local_apic_interrupt_input : uint8_t {
        LINT0   = 0,
        LINT1   = 1
    };

    /* LOCAL APIC NMI STRUCTURE */
    /* This structure describes the Local APIC interrupt input (LINTn) that NMI is connected to for each of the processors in the system where such
     * a connection exists. This information is needed to enable the appropriate local APIC entry. */
    template<>
    class interrupt_controller_structure<interrupt_controller_structure_base::type::LOCAL_APIC_NMI> : public interrupt_controller_structure_base {
    public:
        static constexpr interrupt_controller_structure_base::type type { interrupt_controller_structure_base::type::LOCAL_APIC_NMI };

    private:
        uint8_t                 m_acpi_processor_uid;
        Detail::mps_inti_flags  m_flags;
        uint8_t                 m_local_apic_lint;

    public:
        /* 0xFF means all processors */
        acpi_processor_uid_t processor_id( void ) const noexcept {
            return acpi_processor_uid_t{ this->m_acpi_processor_uid };
        }

        polarity input_signal_polarity( void ) const noexcept {
            return this->m_flags.input_signal_polarity();
        }

        trigger_mode input_signal_trigger_mode( void ) const noexcept {
            return this->m_flags.input_signal_trigger_mode();
        }

        /* Local APIC interrupt input LINTn to which NMI is connected */
        local_apic_interrupt_input local_apic_lint( void ) const noexcept {
            return static_cast<local_apic_interrupt_input>( this->m_local_apic_lint );
        }

    } __attribute__(( packed ));

    /* MJULTIPROCESSOR WAKEUP STRUCTURE */
    /* NOTE: This table is supooted from ACPI v6.4 (Exact ACPI version can be found within FADT table)*/
    /* The platform firmware publishes a multiprocessor wakeup structure to let the bootstrap processor (BSP) wake up application processors (APs) with a mailbox.
     * The mailbox is a memory that the firmware reserves so that each processor can have the OS send a message to them. */
    template<>
    class interrupt_controller_structure<interrupt_controller_structure_base::type::MULTIPROCESSOR_WAKEUP> : public interrupt_controller_structure_base {
    public:
        static constexpr interrupt_controller_structure_base::type type { interrupt_controller_structure_base::type::MULTIPROCESSOR_WAKEUP };

        /* During system boot the firmware puts the application processors (APs) in a state to check the mailbox. The shared mailbox is a 4KB aligned 4K size memory
         * block allocated by the firmware in the ACPINvs memory */
        class alignas( 4_KB ) multiprocessor_wakeup_mailbox {
        public:
            enum class command : uint16_t {
                NO_OPERATION    = 0,
                WAKEUP          = 1,
            };

        private:
            /* OS Section. This section can only be written by the OS and read by the FW, except the 'm_command' field. */
            command     m_command;                          /* The command sent to the AP */
            uint32_t    m_acpi_id;                          /* The processor's local APIC ID. The AP shall check if the APIC ID field mathces its own APIC ID. The AP shall igonre the command in case of mismatch. */
            uint64_t    m_wakeup_vector;                    /* The wakeup address for application processors (APs) */
            byte        m_reserved_for_os [ 2032 ];

            /* Firmware Section */
            byte        m_reserved_for_firmware [ 2048 ];

        public:
            /* After the OS detects the processor number from MADT table, the OS may prepare wakeup routine,  and send the wakeup command.
            For each AP the mailbox can be used only once for the wakeup command. After the AP takes the action according to the command, this mailbox will no longer be checked by the AP. 
             * Other processors can continue using the mailbox for the next command. */
            void wake_up( apic_id_t apic_id, uint64_t wakeup_vector ) {
                /* Fill the wakeup address field in the mailbox, */
                this->m_wakeup_vector = wakeup_vector;
                /* Indicate which processor need to be wakeup in the APIC ID field */
                this->m_acpi_id = static_cast<uint8_t>( apic_id );
                /* Send the wakeup command */
                /* Wait until the command is set to NO_OPERATION by the FW */
                while( this->m_command != command::NO_OPERATION );
                /* Send the wakeup command */
                this->m_command = command::WAKEUP;
                /* TODO: The application processor (AP) need clear the command
                 * as the acknowledgement that the command is received. */
            }

        };

        static_assert( sizeof( multiprocessor_wakeup_mailbox ) == 4_KB );

    private:
        uint16_t    m_mailbox_version;
        uint32_t                        : 32;
        uint64_t    m_mailbox_address;          /* Physical address of the mailbox. It must be in ACPINvs. It must also be 4K bytes aligned */

    public:

    } __attribute__(( packed ));

    namespace Detail {

        template<interrupt_controller_structure_base::type ENTRY_TYPE>
        class madt_iterator {
        public:
            static constexpr interrupt_controller_structure_base::type entry_type { ENTRY_TYPE };
            using value_type = interrupt_controller_structure<ENTRY_TYPE>;
            using pointer = const value_type *;
            using reference = const value_type &;
            using difference_type = ptrdiff_t;
            using iterator_category = forward_iterator_tag;

        private:
            const interrupt_controller_structure_base * m_entry;

            const interrupt_controller_structure_base * m_end;

        public:
            /* Default constructor */
            constexpr madt_iterator( void ) : madt_iterator( nullptr ) {}

            /* nullptr constructor */
            constexpr madt_iterator( nullptr_t ) : m_entry( nullptr ) {}

            /* Explicit constuctor */
            constexpr explicit madt_iterator( const interrupt_controller_structure_base * entry, const interrupt_controller_structure_base * end ) {
                /* The pointer to the end remains unchaged so let's store it directly */
                this->m_end = end;
                /* Check whether 'entry' points to correct 'entry_type'. If not so, iterate over the entries till the first one of appropriate type is found or the end of the table is reached */
                while( ( entry < this->m_end ) && ( entry->entry_type() != entry_type ) && ( entry->entry_type() != interrupt_controller_structure_base::type::ANY ) ) {
                    entry = entry->next();
                }
                /* Entry is now pointing to either to the entry of correct type or is equal to end of table pointer */
                this->m_entry = entry;
            }

            constexpr madt_iterator( const interrupt_controller_structure_base * entry ) : madt_iterator( entry, entry ) {}

            /* Copy constructor */
            constexpr madt_iterator( const madt_iterator & other ) : m_entry( other.m_entry ) {}

            /* Move constructor */
            constexpr madt_iterator( madt_iterator && other ) : m_entry( move( other.m_entry ) ) {}

            /* Destructor */
            constexpr ~madt_iterator( void ) = default;

            /* Copy assignment operator */
            constexpr madt_iterator & operator=( const madt_iterator & other ) {
                madt_iterator{ other }.swap( *this );
                return *this;
            }

            /* Move assignment operator */
            constexpr madt_iterator & operator=( madt_iterator && other ) {
                madt_iterator{ move( other ) }.swap( *this );
                return *this;
            }

            /* Prefix increment operator */
            constexpr madt_iterator & operator++( void ) noexcept {
                const interrupt_controller_structure_base * next_entry { this->m_entry->next()};

                /* Iterate entries until the next one of appropriate entry type is found or the end of table is reached */
                while( ( next_entry < this->m_end ) && ( next_entry->entry_type() != entry_type ) && ( next_entry->entry_type() != interrupt_controller_structure_base::type::ANY ) ){
                    next_entry = next_entry->next();
                }
                /* '*this' is now pointing to the valid element of given entry_type or is equal to MADT table end() iterator */
                this->m_entry = next_entry;
                return *this;
            }

            constexpr madt_iterator operator++( int ) noexcept {
                madt_iterator temp { *this };
                this->operator++();
                return temp;
            }

            constexpr bool operator!=( const madt_iterator & other ) const noexcept {
                return this->m_entry != other.m_entry;
            }

            constexpr bool operator<( const madt_iterator & other ) const noexcept {
                return this->m_entry < other.m_entry;
            }

            /* Swap */
            constexpr void swap( madt_iterator & other ) noexcept {
                ReVolta::swap( this->m_entry, other.m_entry );
            }

            constexpr pointer operator->( void ) const noexcept {
                return static_cast<pointer>( this->m_entry );
            }

            /* FIXME: Avoid nullptr dereference */
            constexpr reference operator*( void ) const noexcept {
                return *( static_cast<pointer>( this->m_entry ) );
            }
        }; 

    }

    /* MADT table describes all the interrupt controllers in the system. It can be used
     * to enumerate all the processors currently available */
    class madt : public system_description_table {
        struct multiple_apic_flags {
            bool        m_PCAT_COMPAT   : 1;    /* A one indicates that the system also has a PC-AT compatible dual-8259 setup */
            uint32_t                    : 31;
        } __attribute__(( packed ));

        /* Verify the multiple apic flags structure is 4bytes long as defined in ACPI standard */
        static_assert( sizeof( multiple_apic_flags ) == 4 );

    public:
        constexpr static const char * signature { "APIC" };

        template<interrupt_controller_structure_base::type ENTRY_TYPE = interrupt_controller_structure_base::type::ANY> 
        using const_iterator = Detail::madt_iterator<ENTRY_TYPE>;

    private:
        uint32_t                m_local_apic_address;       /* The 32bit physical address at which each processor can access its local interrupt controller */
        multiple_apic_flags     m_flags;

        interrupt_controller_structure_base m_entry;

    public:
            /* Local APIC address */
            observer_ptr<xapic> local_apic_address( void ) const noexcept {
                return make_observer( reinterpret_cast<xapic *>( this->m_local_apic_address ) );
            }

            /* NOTE: If 'true' then all the 8259 PIC interrupts shall be masked */
            bool is_dual_8259_pic_installed( void ) const noexcept {
                return this->m_flags.m_PCAT_COMPAT;
            }

            /* Get the const iterator pointing to the first MADT table entry of given type */
            template<interrupt_controller_structure_base::type ENTRY_TYPE = interrupt_controller_structure_base::type::ANY>
            const_iterator<ENTRY_TYPE> begin( void ) const noexcept {
                return const_iterator<ENTRY_TYPE>{ addressof( this->m_entry ), this->table_end<interrupt_controller_structure_base>() };
            }

            /* Get the const iterator passed the last MADT table entry of given type */
            template<interrupt_controller_structure_base::type ENTRY_TYPE = interrupt_controller_structure_base::type::ANY>
            const_iterator<ENTRY_TYPE> end( void ) const noexcept {
                return const_iterator<ENTRY_TYPE>{ this->table_end<interrupt_controller_structure_base>() };
            }

            /* Return the number of entries of given type 
             * (i.e. 'size<interrupt_controller_structure<interrupt_controller_structure_base::type::PROCESSOR_LOCAL_APIC>>()'
             * returns the number of local APICs in the system and thus the number of logical processors) */
            template<interrupt_controller_structure_base::type ENTRY_TYPE = interrupt_controller_structure_base::type::ANY>
            constexpr size_t size( void ) const noexcept {
                return distance( begin<ENTRY_TYPE>(), end<ENTRY_TYPE>() );
            }

    } __attribute__(( packed ));

}