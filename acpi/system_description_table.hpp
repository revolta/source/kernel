#pragma once

#include <cstdint.hpp>
#include <algorithm.hpp>

namespace ReVolta::Kernel {

    /* Common SDT table descriptor */
    class system_description_table {
    protected:
        char        m_signature [ 4 ];
        uint32_t    m_length;                   /* The length of the entire table in bytes including the header, starting at offset 0 */
        uint8_t     m_revision;
        uint8_t     m_checksum;
        char        m_oem_id [ 6 ];
        char        m_oem_table_id [ 8 ];
        uint32_t    m_oem_revision;
        uint32_t    m_creator_id;
        uint32_t    m_creator_revision;

    public:
        inline bool check_signature( const char * sig ) const {
            return equal( this->m_signature, this->m_signature + 4, sig );
        }

        /* Calculates the checksum and validates its value (its equality to 0) */
        [[nodiscard]] bool is_valid( void ) const {
            size_t sum { 0 };
            const unsigned char * it { reinterpret_cast<const unsigned char *>( this ) };
            size_t count { this->m_length };
            while( count --> 0 ) {
                sum += *it++;
            }
            return ( sum & 0xFF ) == 0;
        }

        /* Returns pointer pointing to the one passed the last element */
        template<typename T>
        const T * table_end( void ) const {
            return reinterpret_cast<const T *>( reinterpret_cast<const uint8_t *>( this ) + this->m_length );
        }

    } __attribute__(( packed ));
}
