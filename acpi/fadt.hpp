#pragma once

#include "system_description_table.hpp"

#include <cstdint.hpp>

namespace ReVolta::Kernel {

    struct generic_address_structure {
        enum class address_space_type : uint8_t {
            SYSTEM_MEMORY_SPACE             = 0x00,
            SYSTEM_IO_SPACE                 = 0x01,
            PCI_CONFIGURATION_SPACE         = 0x02,
            EMBEDDED_CONTROLLER             = 0x03,
            SMBUS                           = 0x04,
            SYSTEM_CMOS                     = 0x05,
            PCI_BAR_TARGET                  = 0x06,
            IPMI                            = 0x07,
            GENERAL_PURPOSE_IO              = 0x08,
            GENERIC_SERIAL_BUS              = 0x09,
            PLATFORM_COMMUNICATION_CHANNEL  = 0x0A,
            FUNCTIONAL_FIXED_HARDWARE       = 0x7F,
        };

        enum class access_size_type : uint8_t {
            UNDEFINED       = 0,    /* Undefined due to legacy reasons */
            BYTE            = 1,    /* 8-bit access */
            WORD            = 2,    /* 16-bit access */
            DWORD           = 3,    /* 32-bit access */
            QWORD           = 4     /* 64-bit access */
        };

        address_space_type  address_space_id;       /* The address space where the data structure or register exists */
        uint8_t             register_bit_width;     /* The size in bits of the given register. When addressing a data structure, this must be 0 */
        uint8_t             register_bit_offset;    /* The bit offset of the given register at the given address. When addressing a data structure, this must be 0 */
        access_size_type    access_size;            /* Specifies access size. Unless otherwise defined bz the address space ID */
        uint64_t            address;                /* The 64-bit address of the data structure or register in the given address space */

    } __attribute__(( packed ));

    static_assert( sizeof( generic_address_structure ) == 12 );

    /* TODO: Worth to extend / add some more functionalities */
    struct version_t {
        uint8_t major;
        uint8_t minor;
        uint8_t patch;
    };

    class facs;
    class dsdt;

    enum class preferred_pm_profile : uint8_t {
        UNSPECIFIED         = 0,
        DESKTOP             = 1,
        MOBILE              = 2,
        WORKSTATION         = 3,
        ENTERPRISE_SERVER   = 4,
        SOHO_SERVER         = 5,
        APPLICANCE_PC       = 6,
        PERFORMANCE_SERVER  = 7,
        TABLET              = 8
    };

    class fadt : public system_description_table {
    public:
        constexpr static const char * signature { "FACP" };
    private:
        struct ia_pc_boot_architecture_flags {
            bool m_legacy_devices       : 1;
            bool m_8042                 : 1;
            bool m_vga_not_present      : 1;
            bool m_msi_not_supported    : 1;
            bool m_pcie_aspm_controls   : 1;
            bool m_cmos_rtc_not_present : 1;
            uint16_t : 10;
        } __attribute__(( packed ));

        static_assert( sizeof( ia_pc_boot_architecture_flags ) == 2 );

        class arm_boot_flags {
            bool m_psci_compliant   : 1;
            bool m_use_hvc          : 1;
            uint16_t : 14;
        } __attribute__(( packed ));

        /* Fixed ACPI Description Table Fixed Feature Flags */
        struct flags {
            bool m_wbinvd                               : 1;
            bool m_wbinvd_flush                         : 1;
            bool m_proc_c1                              : 1;
            bool m_p_vlv2_up                            : 1;
            bool m_pwr_button                           : 1;
            bool m_slp_button                           : 1;
            bool m_fix_rtc                              : 1;
            bool m_rtc_s4                               : 1;
            bool m_tmr_val_ext                          : 1;
            bool m_dck_cap                              : 1;
            bool m_reset_reg_sup                        : 1;
            bool m_sealed_case                          : 1;
            bool m_headless                             : 1;
            bool m_cpu_sw_slp                           : 1;
            bool m_pci_exp_wak                          : 1;
            bool m_use_platform_clock                   : 1;
            bool m_s4_rtc_sts_valid                     : 1;
            bool m_remote_power_on_capable              : 1;
            bool m_force_apic_cluster_model             : 1;
            bool m_force_apic_physical_destination_mode : 1;
            bool m_hw_reduced_acpi                      : 1;
            bool m_low_power_s0_idle_capable            : 1;
            uint16_t                                    : 10;
        } __attribute__(( packed ));

        static_assert( sizeof( flags ) == 4 );

        uint32_t                        m_firmware_ctrl;
        uint32_t                        m_dsdt;
        uint8_t : 8;
        uint8_t                         m_preferred_pm_profile;
        uint16_t                        m_sci_int;
        uint32_t                        m_smi_cmd;
        uint8_t                         m_acpi_enable;
        uint8_t                         m_acpi_disable;
        uint8_t                         m_s4bios_req;
        uint8_t                         m_pstate_cnt;
        uint32_t                        m_pm1a_evt_blk;
        uint32_t                        m_pm1b_evt_blk;
        uint32_t                        m_pm1a_cnt_blk;
        uint32_t                        m_pm1b_cnt_blk;
        uint32_t                        m_pm_tmr_blk;
        uint32_t                        m_gpe0_blk;
        uint32_t                        m_gpe1_blk;
        uint8_t                         m_pm1_evt_len;
        uint8_t                         m_pm1_cnt_len;
        uint8_t                         m_pm2_cnt_len;
        uint8_t                         m_pm_tmr_len;
        uint8_t                         m_gpe0_blk_len;
        uint8_t                         m_gpe1_blk_len;
        uint8_t                         m_gpe1_base;
        uint8_t                         m_cst_cnt;
        uint16_t                        m_p_lvl2_lat;
        uint16_t                        m_p_lvl3_lat;
        uint16_t                        m_flush_size;
        uint16_t                        m_flush_stride;
        uint8_t                         m_duty_offset;
        uint8_t                         m_duty_width;
        uint8_t                         m_day_alrm;
        uint8_t                         m_mon_alrm;
        uint8_t                         m_century;
        ia_pc_boot_architecture_flags   m_iapc_boot_arch;
        uint8_t : 8;
        flags                           m_flags;
        generic_address_structure       m_reset_reg;
        uint8_t                         m_reset_value;
        arm_boot_flags                  m_arm_boot_arch;
        uint8_t                         m_fadt_minor_version;   /* TODO: Change the variable type as it is structured bit-wise within */
        uint64_t                        m_x_firmware_ctrl;
        uint64_t                        m_x_dsdt;
        generic_address_structure       m_x_pm1a_evt_blk;
        generic_address_structure       m_x_pm1b_evt_blk;
        generic_address_structure       m_x_pm1a_cnt_blk;
        generic_address_structure       m_x_pm1b_cnt_blk;
        generic_address_structure       m_x_pm2_cnt_blk;
        generic_address_structure       m_x_pm_tmr_blk;
        generic_address_structure       m_x_gpe0_blk;
        generic_address_structure       m_x_gpe1_blk;
        generic_address_structure       m_sleep_control_reg;
        generic_address_structure       m_sleep_status_reg;
        uint64_t                        m_hypervisor_vendor_identity;

    public:
        version_t acpi_version( void ) const noexcept {
            return version_t{ .major = this->m_revision, .minor = uint8_t( this->m_fadt_minor_version & 0x0F ), .patch = 0 };
        }

        facs * firmware_ctrl( void ) const noexcept {
            return reinterpret_cast<facs *>( this->m_firmware_ctrl );
        }

        dsdt * differentiated_system_description_table( void ) const noexcept {
            return reinterpret_cast<dsdt *>( this->m_dsdt );
        }

    } __attribute__(( packed ));

}