#include "cpuid.hpp"

#include "cpuid.h"

#include <utility.hpp>

namespace ReVolta::Kernel {

    bool cpuid::check_cpuid_support( void ) noexcept {
        uint32_t value, result;
        asm volatile (
            "pushf\n"
            "popl %[result]\n"
            "movl %[result], %[value]\n"
            "xorl %[eflags_id], %[value]\n"
            "pushl %[value]\n"
            "popf\n"
            "pushf\n"
            "popl %[value]\n"
            "andl %[eflags_id], %[result]\n"
            "andl %[eflags_id], %[value]\n"
            "xorl %[value], %[result]\n"
            : [result] "=r" (result), [value] "=r" (value)
            : [eflags_id] "i" ( (1 << 21) ) 
        );
        return result;
    }

    bool cpuid::supports( const ecx_feature feature ) noexcept {
        uint32_t eax { 0U }, ebx { 0U }, ecx { 0U }, edx { 0U };
		if( cpuid::is_supported() ) {
			__cpuid( to_underlying( cpuid_leaf::FEATURE_INFORMATION ), eax, ebx, ecx, edx );
		}
        return ecx & to_underlying( feature );
    }

    bool cpuid::supports( const edx_feature feature ) noexcept {
        uint32_t eax { 0U }, ebx { 0U }, ecx { 0U }, edx { 0U };
		if( cpuid::is_supported() ) {
			__cpuid( to_underlying( cpuid_leaf::FEATURE_INFORMATION ), eax, ebx, ecx, edx );
		}
        return edx & to_underlying( feature );
    }
    
    /* EAX=1: Processor Info and Feature Bits */
    /* TODO: Maybe this shall be somehow generic - to read various CPU info. To execute the __cpuid() just once and then provide 
     * particular information out of dedicated structure using the accessor functions */
    uint8_t cpuid::local_apic_id( void ) {
        uint32_t eax { 0U }, ebx { 0U }, ecx { 0U }, edx { 0U };
        if( cpuid::is_supported() ) {
            __cpuid( to_underlying( cpuid_leaf::FEATURE_INFORMATION ), eax, ebx, ecx, edx );
        }
        return ebx >> 24;
    }

}