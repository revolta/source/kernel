#pragma once

#include "paging.hpp"
#include "memory.hpp"

namespace ReVolta::Kernel {

    struct clone_current_context_t { explicit constexpr clone_current_context_t( void ) noexcept = default; };

    inline constexpr clone_current_context_t clone_current_context;

    /* Quite good Memento example: https://www.sourcecodeexamples.net/2020/09/c-memento-pattern-example.html 
     *
     * - creation of process context is in hands of CPU as originator
     * - process just stores the context having no access to any memeber 
     * - process holds the pointer to CPU so that the call to 'cpu->restore_process_context()' is available, etc.
     */
    class process_context {
    public:
        /* Constructs empty context */
        process_context( void ) noexcept {
            /* Allocate new page directory and construct it by default constructor - shall be called for initial process only */
            /* TODO: Ensure it is being called only once in non-paging mode */
            m_page_directory = allocate_retain<page_directory>( allocator<page_directory> {} );
        }

        /* Move constructor */
        process_context( process_context && other ) noexcept {
            m_page_directory = move( other.m_page_directory );
        }

        /* Move assignment operator */
        process_context & operator=( process_context && other ) noexcept {
            process_context{ move( other ) }.swap( *this );
            return *this;
        }

        process_context( clone_current_context_t ) noexcept {
            /* Allocate new page directory frame and map it as temporary page directory */
            page::map( temporary_page_directory, 
                unique_ptr<page_directory>{ allocator_traits<allocator<page_directory>>::allocate( allocator<page_directory>{}, 1 ) }, 
                Access::READ_WRITE, Mode::SUPERVISOR
            );
            /* Construct the newly allocated page directory by copy of the current page directory - to clone current address space
             * and express the ownership by the retain_ptr. The main purpose is to correctly manage the use_count. */
            retain_ptr<page_directory> pd { construct_at( temporary_page_directory.get(), *page_directory::get() ) };
            /* All set and done within the 'pd' page directory retain pointer containing the virtual address. Convert it to physical address and store
             * it within the process context */
            m_page_directory.reset( physical_cast( pd.detach() ) );
        }

        /* Copy assignment operator */
        process_context & operator=( const process_context & other ) noexcept = delete;

        ~process_context( void ) noexcept = default;

private:
        friend class cpu;

        inline void swap( process_context & other ) noexcept {
            using ReVolta::swap;
            swap( m_page_directory, other.m_page_directory );
        }

        /* Page directory */
        /* TODO: Shall the page directory pointer be directly a tagged one, holding the tags directly than to set it in 
         * cpu::restore_process_context() ? */

        /* FIXME: The unique_ptr causes page fault once the context is being destructed - page directory is not mapped 
         * to virtual address space directly but on a fixed address 0xFFC00000. Once the regular destruction by nature
         * for unique_ptr, which holds the physical address the page fault is triggered as unmapped region */

        /* As the page directory is internally self mapped, the self-mapping is realized by retain_ptr. In order to have
         * single ownership principle the retain_ptr must be used in there as well, even though the unique ownership shall
         * be expressed here. Once fully initialized, the use_count() shall return 2 (this reference and the self-mapped one) */
        retain_ptr<page_directory> m_page_directory { nullptr };
    };

}
