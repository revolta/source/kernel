#pragma once

#include "descriptor_table.hpp"
#include "interrupt_descriptor_table.hpp"

#include "local_apic.hpp"

#include <memory_pool.hpp>

namespace ReVolta::Kernel {

    class x86 {
    public:
        /* TODO: Public definitions if any */

    private:
        /* TODO: Local data (member variables) */

    protected:
        /* Default constructor made protected in order to deny standalone construction */
        x86( void );

    public:
        [[nodiscard]] bool is_bootstrap( void ) const noexcept;
    };

}

#if false
        /* TODO: Intended to store the virtual APIC address once mapped from physical one */
        observer_ptr<local_apic> m_lapic { nullptr };

        /* FIXME: In the CPU the linear addresses are expected. So it must be mapped into virtual address space once paging is enabled.
            * Maybe the fix can be to move them to dedicated page aligned storage and share such pages between bootstrap and kernel */
        /* TODO: Make this 'constexpr' */
        descriptor_table<basic_flat_model> m_gdt;

        /* FIXME: In the CPU the linear addresses are expected. So it must be mapped into virtual address space once paging is enabled.
            * Maybe the fix can be to move them to dedicated page aligned storage and share such pages between bootstrap and kernel */
        /* TODO: Make this 'constexpr' */
        interrupt_descriptor_table m_idt { make_observer( addressof( m_gdt ) ) };
#endif
