#include "local_apic.hpp"

#include "cpuid.hpp"
#include "model_specific_registers/ia32_apic_base.hpp"

namespace ReVolta::Kernel {

    observer_ptr<local_apic> local_apic::get_physical_ptr( void )  {
        static observer_ptr<local_apic> lapic { nullptr };
        if( lapic == nullptr ) {
            /* CPUID.01h:EDX[bit 9] defines whether APIC is supported by the CPU or not */
            if( cpuid::supports( cpuid::edx_feature::APIC ) ) {
                /* Once supported, read the APIC base address from the IA32_APIC_BASE MSR and store the observer pointer to APIC */
                lapic = make_observer<local_apic>( ia32_apic_base{}.apic_base << 12 );
            }
        }
        return lapic;
    }

    void local_apic::enable( void ) {
        ia32_apic_base{}.en = true;
    }

}