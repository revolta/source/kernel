#pragma once

#include <utility.hpp>

namespace ReVolta::Kernel::APIC::Timer {

    class initial_count_register : private apic_register {
        uint32_t m_initial_count;

    public:
        initial_count_register & operator=( const uint32_t initial_count ) {
            this->m_initial_count = initial_count;
            return *this;
        }

        operator uint32_t( void ) const {
            return this->m_initial_count;
        }

    } __attribute__(( packed ));

    class current_count_register : private apic_register {
        uint32_t m_current_count;

    public:
        current_count_register & operator=( const uint32_t initial_count ) {
            this->m_current_count = initial_count;
            return *this;
        }

        operator uint32_t( void ) const {
            return this->m_current_count;
        }
    } __attribute__(( packed ));

    enum class divide_configuration_value : uint8_t {
        DIVIDE_BY_1         = 0b111,
        DIVIDE_BY_2         = 0b000,
        DIVIDE_BY_4         = 0b001,
        DIVIDE_BY_8         = 0b010,
        DIVIDE_BY_16        = 0b011,
        DIVIDE_BY_32        = 0b100,
        DIVIDE_BY_64        = 0b101,
        DIVIDE_BY_128       = 0b110
    };

    class divide_configuration_register : private apic_register {
        uint8_t m_bit0      : 1;
        uint8_t m_bit1      : 1;
        uint8_t             : 1;
        uint8_t m_bit3      : 1;
        uint32_t            : 28;

    public:
        void divide_value( const divide_configuration_value & value ) {
            uint8_t val { to_underlying( value ) };
            this->m_bit0 = val & 0x01;
            this->m_bit1 = ( val >> 1 ) & 0x01;
            this->m_bit3 = ( val >> 3 ) & 0x01;
        }

        divide_configuration_value divide_value( void ) const {
            return static_cast<divide_configuration_value>( ( this->m_bit3 << 3 ) & ( this->m_bit1 << 1 ) & ( this->m_bit0 ) );
        }

    } __attribute__(( packed ));

}