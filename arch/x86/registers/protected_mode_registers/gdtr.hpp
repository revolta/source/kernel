#pragma once

#include "segmentation.hpp"

#include "cstdint.hpp"
#include "cpu_register.hpp"

namespace ReVolta::Kernel {

    class gdtr : public cpu_register<gdtr, uint48_t> {
    public:
        [[no_unique_address]] cpu_register::reference<uint16_t, 0, 16>                              limit;  /* [ 0  .. 15 ] 16bit Table Limit - ( ( size of GDT ) - 1 ) */
        [[no_unique_address]] cpu_register::reference<descriptor_table<basic_flat_model> *, 16, 32> base;   /* [ 16 .. 47 ] 32bit Descriptor Table Linear Base Address */

    private:
        friend class cpu_register<gdtr, uint48_t>;
        /* When the GDTR register is stored in memory using SGDT instruction a 48-bit "pseudo-descriptor" is stored in memory.
            * To avoid alignment check faults in user mode (RING3) the pseudo descriptor shall be located at an odd word address
            * means - address MOD 4 equals to 2 */
        /* TODO: the pseudo-descriptor is de facto the 'gdtr_register_bits' structure. Shall this structure be returned instead 
            * of the raw_type to ensure the alignment is ensured? */
        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "sgdtl %[v]\n" : [v] "=m" ( raw ) );
            return raw;
        }

        inline static void store( raw_type raw ) noexcept {
            asm volatile ( "lgdtl %[v]\n" :: [v] "m" ( raw ) );
        }
    };

}