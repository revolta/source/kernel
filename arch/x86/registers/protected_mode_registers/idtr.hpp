#pragma once

#include "interrupts.hpp"

#include "cstdint.hpp"
#include "cpu_register.hpp"

namespace ReVolta::Kernel {

    class idtr : public cpu_register<idtr, uint48_t> {
    public:
        [[no_unique_address]] cpu_register::reference<uint16_t, 0, 16>                      limit;  /* [ 0  .. 15 ] 16bit Table Limit - ( ( size of IDT ) - 1 ) */
        [[no_unique_address]] cpu_register::reference<interrupt_descriptor_table *, 16, 32> base;   /* [ 16 .. 47 ] 32bit Descriptor Table Linear Base Address */

    private:
        friend class cpu_register<idtr, uint48_t>;

        /* When the IDTR register is stored in memory using SGDT instruction a 48-bit "pseudo-descriptor" is stored in memory.
         * To avoid alignment check faults in user mode (RING3) the pseudo descriptor shall be located at an odd word address
         * means - address MOD 4 equals to 2 */
        inline raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "sidtl %[v]\n" : [v] "=m" ( raw ) );
            return raw;
        }
        inline void store( raw_type raw ) noexcept {
            asm volatile ( "lidtl %[v]\n" :: [v] "m" ( raw ) );
        }
    };
    
}