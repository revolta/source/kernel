#pragma once

#include "model_specific_register.hpp"

namespace ReVolta::Kernel {

    struct ia32_apic_base : public model_specific_register<0x1B> {
        
        static constexpr size_t MAX_PHYS_ADDR { 36 };

        [[no_unique_address]] const cpu_register::reference<bool, 8, 1>                         bsp;
        [[no_unique_address]] cpu_register::reference<bool, 10, 1>                              extd;
        [[no_unique_address]] cpu_register::reference<bool, 11, 1>                              en;
        [[no_unique_address]] cpu_register::reference<uintptr_t, 12, ( MAX_PHYS_ADDR - 12 )>    apic_base;
    };

}