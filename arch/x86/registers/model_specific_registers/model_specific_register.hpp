#pragma once

#include "cpu_register.hpp"

#include <cstdint.hpp>

namespace ReVolta::Kernel {

    template<uint32_t MSR>
    class model_specific_register : public cpu_register<model_specific_register<MSR>, uint64_t> {
    public:
        static constexpr uint32_t msr_address { MSR };

        using raw_type = typename cpu_register<model_specific_register<MSR>, uint64_t>::raw_type;

    private:
        friend class cpu_register<model_specific_register<MSR>, uint64_t>;

        inline static raw_type load( void ) noexcept {
            raw_type msr_value;
            asm volatile ( "rdmsr" : "=A" ( msr_value ) : "c" ( msr_address ) );
            return msr_value;
        }

        inline static void store( raw_type raw ) noexcept {
            uint32_t low = raw & 0xFFFFFFFF;
            uint32_t high = raw >> 32;
            asm volatile ( "wrmsr" : : "c"( msr_address ), "a"( low ), "d"( high ) );
        }
    };

}