#pragma once

#include "cstdint.hpp"
#include "cpu_register.hpp"

namespace ReVolta::Kernel {

    class eflags : public cpu_register<eflags, uint32_t> {
        [[no_unique_address]] cpu_register::reference<bool, 0, 1>       cf;	    /* [ 0 ]        Carry flag */
        [[no_unique_address]] cpu_register::reference<bool, 2, 1>       pf;	    /* [ 2 ]        Parity flag */
        [[no_unique_address]] cpu_register::reference<bool, 4, 1>       af;	    /* [ 4 ]        Auxiliary flag */
        [[no_unique_address]] cpu_register::reference<bool, 6, 1>       zf;	    /* [ 6 ]        Zero flag */
        [[no_unique_address]] cpu_register::reference<bool, 7, 1>       sf;	    /* [ 7 ]        Sign flag */
        [[no_unique_address]] cpu_register::reference<bool, 8, 1>       tf;	    /* [ 8 ]        Trap flag */
        [[no_unique_address]] cpu_register::reference<bool, 9, 1>       irqf;	/* [ 9 ]        Interrupts enable flag */
        [[no_unique_address]] cpu_register::reference<bool, 10, 1>      df;	    /* [ 10 ]       Direction flag */
        [[no_unique_address]] cpu_register::reference<bool, 11, 1>      of;	    /* [ 11 ]       Overflow flag */
        [[no_unique_address]] cpu_register::reference<uint8_t, 12, 2>   iopl;	/* [ 12 .. 13 ] i/o Privilege level */
        [[no_unique_address]] cpu_register::reference<bool, 14, 1>      nt;	    /* [ 14 ]       Nested task flag */
        [[no_unique_address]] cpu_register::reference<bool, 16, 1>      rf;	    /* [ 16 ]       Resume flag */
        [[no_unique_address]] cpu_register::reference<bool, 17, 1>      vm;	    /* [ 17 ]       Virtual 8086 mode flag */
        [[no_unique_address]] cpu_register::reference<bool, 18, 1>      ac;	    /* [ 18 ]       Alignment check flag */
        [[no_unique_address]] cpu_register::reference<bool, 19, 1>      vif;	/* [ 19 ]       Virtual interrupt flag */
        [[no_unique_address]] cpu_register::reference<bool, 20, 1>      vip;	/* [ 20 ]       Virtual interrupt pending */
        [[no_unique_address]] cpu_register::reference<bool, 21, 1>      id;	    /* [ 21 ]       ID flag */
    
    private:
        friend class cpu_register<eflags, uint32_t>;

        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile (
                "pushf\n"
                "popl %[v]\n" : [v] "=r" ( raw )
            );
            return raw;
        }

        inline static void store( raw_type raw ) noexcept {
            asm volatile (
                "pushl %[v]\n"
                "popf\n" :: [v] "r" ( raw )
            );
        }
    };

}