#pragma once

#include "cstdint.hpp"
#include "cpu_register.hpp"

namespace ReVolta::Kernel {

    class cr4 : public cpu_register<cr4, uint32_t> {
    public:
        [[no_unique_address]] cpu_register::reference<bool, 0, 1>       vme;	        /* bit0 - Virtual 8086 Mode Extensions */
        [[no_unique_address]] cpu_register::reference<bool, 1, 1>       pvi;	        /* bit1 - Protected Mode Virtual Interrupts */
        [[no_unique_address]] cpu_register::reference<bool, 2, 1>       tsd;	        /* bit2 - Time Stamp Disable */
        [[no_unique_address]] cpu_register::reference<bool, 3, 1>       de;	            /* bit3 - Debugging Extensions */
        [[no_unique_address]] cpu_register::reference<bool, 4, 1>       pse;	        /* bit4 - Page Size Extension */
        [[no_unique_address]] cpu_register::reference<bool, 5, 1>       pae;	        /* bit5 - Physical Address Extension */
        [[no_unique_address]] cpu_register::reference<bool, 6, 1>       mce;	        /* bit6 - Machine Check Exception */
        [[no_unique_address]] cpu_register::reference<bool, 7, 1>       pge;	        /* bit7 - Page Global Enable */
        [[no_unique_address]] cpu_register::reference<bool, 8, 1>       pce;	        /* bit8 - Performance Monitoring Counter Enable */
        [[no_unique_address]] cpu_register::reference<bool, 9, 1>       osfxsr;	        /* bit9 - OS Support for FXSAVE and FXSTOR Instructions */
        [[no_unique_address]] cpu_register::reference<bool, 10, 1>      osxmmexcpt;     /* bit10 - OS Support for Unmasked SIMD Floating Point Exceptions */
        [[no_unique_address]] cpu_register::reference<bool, 11, 1>      umip;	        /* bit11 - User Mode Instruction Prevention */
        [[no_unique_address]] cpu_register::reference<bool, 13, 1>      vmxe;	        /* bit13 - Virtual Machine Extensions Enable */
        [[no_unique_address]] cpu_register::reference<bool, 14, 1>      smxe;	        /* bit14 - Safer Mode Extensions Enable */
        [[no_unique_address]] cpu_register::reference<bool, 17, 1>      pcide;	        /* bit17 - PCID Enable */
        [[no_unique_address]] cpu_register::reference<bool, 18, 1>      osxsave;        /* bit18 - XSAVE and Processor Extended States Enable */
        [[no_unique_address]] cpu_register::reference<bool, 20, 1>      smep;	        /* bit20 - Supervisor Mode Executions Protection Enable */
        [[no_unique_address]] cpu_register::reference<bool, 21, 1>      smap;	        /* bit21 - Supervisor Mode Access Protection Enable */
        [[no_unique_address]] cpu_register::reference<bool, 22, 1>      pke	;	        /* bit22 - Protection Keys for User-mode Pages Enable */
        [[no_unique_address]] cpu_register::reference<bool, 23, 1>      cet	;	        /* bit23 - Control-flow-enforcement Enable */
        [[no_unique_address]] cpu_register::reference<bool, 24, 1>      pks	;	        /* bit24 - Protection Keys for Supervisor-mode Pages Enable */

    private:
        friend class cpu_register<cr4, uint32_t>;

        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "movl %%cr4, %0\n" : "=r" ( raw ) );
            return raw;
        }

        inline static void store( raw_type raw ) noexcept {
            asm volatile ( "movl %[v], %%cr4\n" :: [v] "r" ( raw ) );
        }
    };

}