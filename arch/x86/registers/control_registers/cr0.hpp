#pragma once

#include "cstdint.hpp"
#include "cpu_register.hpp"

namespace ReVolta::Kernel {

    class cr0 : public cpu_register<cr0, uint32_t> {
    public:
        [[no_unique_address]] cpu_register::reference<bool, 0, 1>   pe;     /* bit0 - Protected Mode Enable */
        [[no_unique_address]] cpu_register::reference<bool, 1, 1>   mp;     /* bit1 - Monitor co-processor */
        [[no_unique_address]] cpu_register::reference<bool, 2, 1>   em;     /* bit2 - Emulation */
        [[no_unique_address]] cpu_register::reference<bool, 3, 1>   ts;     /* bit3 - Task Switched */
        [[no_unique_address]] cpu_register::reference<bool, 4, 1>   et;     /* bit4 - Extension Type */
        [[no_unique_address]] cpu_register::reference<bool, 5, 1>   ne;     /* bit5 - Numeric Error */
        [[no_unique_address]] cpu_register::reference<bool, 16, 1>  wp;     /* bit16 - Write Protect */
        [[no_unique_address]] cpu_register::reference<bool, 18, 1>  am;     /* bit18 - Alignment Mask */
        [[no_unique_address]] cpu_register::reference<bool, 29, 1>  nw;     /* bit29 - Not-write Through */
        [[no_unique_address]] cpu_register::reference<bool, 30, 1>  cd;     /* bit30 - Cache Disable */
        [[no_unique_address]] cpu_register::reference<bool, 31, 1>  pg;     /* bit30 - Paging */

    private:
        friend class cpu_register<cr0, uint32_t>;

        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "movl %%cr0, %0\n" : "=r" ( raw ) );
            return raw;
        }

        inline static void store( raw_type raw ) noexcept {
            asm volatile ( "movl %[v], %%cr0\n" :: [v] "r" ( raw ) );
        }
    };

}