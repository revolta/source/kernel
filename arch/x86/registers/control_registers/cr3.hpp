#pragma once

#include "cstdint.hpp"
#include "cpu_register.hpp"
#include "paging.hpp"

namespace ReVolta::Kernel {

    class cr3 : public cpu_register<cr3, uint32_t> {
    public:
        [[no_unique_address]] cpu_register::reference<tagged<observer_ptr<page_directory>, page_directory_pointer_tags>> pd;

        /* Read the content of CR3 and write it back in there - effectively it flushes the whole TLB */
        inline void reload( void ) noexcept {
            asm volatile ( 
                "movl %eax, %cr3\n" 
                "movl %cr3, %eax"
            );
        }

    private:
        friend class cpu_register<cr3, uint32_t>;

        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "movl %%cr3, %0\n" : "=r" ( raw ) );
            return raw;
        }

        inline static void store( raw_type raw ) noexcept {
            asm volatile ( "movl %[v], %%cr3\n" :: [v] "r" ( raw ) );
        }
    };

}