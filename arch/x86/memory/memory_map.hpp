#pragma once

#include "memory.hpp"

namespace ReVolta::Kernel {

    class page;
    class page_directory;
    class page_table;
    class sysconf;

    constexpr inline fixed_ptr<page> higher_half { 0xC0000000 };

    constexpr inline fixed_ptr<page> head_page { 0xFF800000 };

    constexpr inline fixed_ptr<page> next_head_page { 0xFF801000 };

    constexpr inline fixed_ptr<page_directory> temporary_page_directory { 0xFF802000 };

    constexpr inline fixed_ptr<page_table> page_table_mapping { 0xFFC00000 };

    constexpr inline fixed_ptr<page_table> temporary_page_table { 0xFF803000 };

    constexpr inline fixed_ptr<page> process_share { 0xFF400000 };

    constexpr inline fixed_ptr<sysconf> sysconf_mapping { 0xFF400000 };

    constexpr inline fixed_ptr<page_directory> page_directory_mapping { 0xFFFFF000 };

}