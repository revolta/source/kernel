#pragma once

#include "segment_descriptor.hpp"
#include "segment_selector.hpp"

#include <array.hpp>
#include <memory.hpp>

namespace ReVolta::Kernel {

	/* TODO: Might contain the table size for corresponding model */
    struct basic_flat_model {};
	struct protected_flat_model {};

	extern task_state_segment tss;

	template<typename SEGMENTATION_MODEL> class descriptor_table;

	/* NOTE: The base address of descriptor table should be aligned to an eight byte boundary (sizeof(segment_descriptor))
	 * to yield the best processor performance. The size of segment_descriptor is asserted in its implementation */
	template<>
	struct alignas( sizeof( segment_descriptor ) ) descriptor_table<basic_flat_model> : public array<segment_descriptor, 4> {		
		constexpr descriptor_table( void ) noexcept {
			/* FIXME: Shall be replaced by segment_selector? */
			uint16_t selector { 0 };
			/* The first segment descriptor is not used by the CPU. Once null_segment_descriptor is loaded into data segment register (DS, ES, FS or GS)
			 * but it always generates #GP when an attempt is made to access memory using the descriptor */
			this->at( selector++ ) = null_segment_descriptor();
			this->at( selector++ ) = code_segment_descriptor( nullptr, ( 4_GB - 1 ), read_enable::EXECUTE_ONLY, privilege_level::RING_0 );
			this->at( selector++ ) = data_segment_descriptor( nullptr, ( 4_GB - 1 ), write_enable::READ_WRITE, privilege_level::RING_0 );
			/* TODO: Shall reference as many TSSs as number of CPUs - one for each */
			this->at( selector++ ) = task_state_segment_descriptor( &tss );
		}

		inline segment_selector get_code_segment_selector( privilege_level level ) noexcept {
			uint16_t selected_index { 0 };
			for( uint16_t index { 0 }; const segment_descriptor & descriptor : *this ) {
				if( ( descriptor.is_code_segment_descriptor() ) && ( descriptor.get_privilege_level() == level ) ) { 
					selected_index = index;
				} else {
					index++;
				}
			}
			return { level, descriptor_table_type::GDT, selected_index };
		}
	};

}