/*
 * tss.hpp
 *
 *  Created on: 27. 7. 2020
 *      Author: martin
 */

#pragma once

#include "cstdint.hpp"

namespace ReVolta::Kernel {

	struct task_state_segment {
		uint32_t link;			/* Previous task state segment link */
		uint32_t esp0;			/* Privilege 0 stack pointer */
		uint32_t ss0;			/* Privilege 0 stack segment selector */
		uint32_t esp1;			/* Privilege 1 stack pointer */
		uint32_t ss1;			/* Privilege 1 stack segment selector */
		uint32_t esp2;			/* Privilege 2 stack pointer */
		uint32_t ss2;			/* Privilege 2 stack segment selector */
		uint32_t cr3;			/* Page directory base address */
		uint32_t eip;			/* Instruction pointer */
		uint32_t eflags;		/* EFLAGS register state */
		uint32_t eax;			/* EAX General Purpose Register */
		uint32_t ecx;			/* ECX General Purpose Register */
		uint32_t edx;			/* EDX General Purpose Register */
		uint32_t ebx;			/* EBX General Purpose Register */
		uint32_t esp;			/* ESP General Purpose Register */
		uint32_t ebp;			/* EBP General Purpose Register */
		uint32_t esi;			/* ESI General Purpose Register */
		uint32_t edi;			/* EDI General Purpose Register */
		uint32_t es;			/* ES Segment selector */
		uint32_t cs;			/* CS Segment selector */
		uint32_t ss;			/* SS Segment selector */
		uint32_t ds;			/* DS Segment selector */
		uint32_t fs;			/* FS Segment selector */
		uint32_t gs;			/* GS Segment selector */
		uint32_t ldtr;			/* LDT segment selector */
		uint16_t trap;			/* Debug trap */
		uint16_t iopb_offset;	/* I/O Map base */
	} __attribute__((packed));

}
