#pragma once

#include "page.hpp"
#include "array.hpp"

namespace ReVolta::Kernel {

	template<typename TAGGED_PTR>
	class page_table_entry_tags {
		friend class page_table;

	private:
		union properties {
			constexpr properties( void ) noexcept : raw( 0 ) {}

			struct {
				uint8_t p 			: 1 { false };									/* Bit 0 - Present - must be 1 to map a 4-KByte page */
				uint8_t r_w			: 1 { to_underlying( Access::READ_WRITE ) };	/* Bit 1 [R/W] - Read/Write = writable? [true/false] */
				uint8_t u_s			: 1 { to_underlying( Mode::USER ) };			/* Bit 2 [U/S] - User/Supervisor = User accessible? [true/false]*/
				uint8_t pwt 		: 1 { false };									/* Bit 3 - Page-level write through */
				uint8_t pcd 		: 1 { false };									/* Bit 4 - Page-level cache disable */
				uint8_t a 			: 1 { 0 };										/* Bit 5 - Accessed */
				uint8_t d 			: 1 { 0 };										/* Bit 6 - Dirty - indicates whether SW has written to the 4-KByte page referenced */
				uint8_t pat 		: 1 { 0 };										/* Bit 7 - Page attribute table */
				uint8_t g			: 1 { 0 };										/* Bit 8 - Determines whether the translation is global */
				uint8_t use_count	: 3 { 0 };										/* Bits 9..11 - Ignored by paging, used as use counter for retain_ptr */
			} bits;
			uint16_t raw : 12;
		}__attribute__((packed));

	public:
		using tags_type = properties;

		constexpr static size_t required_tags_bit_width { 12 };

		/* Default constructor defines tags defaults */
		/* FIXME: Shall set an empty page table - in minimum set the present flag to false */
		constexpr page_table_entry_tags( void ) noexcept {}

		/* The destructor erases all the tags except the use counter which is handled by dedicated 'retain_ptr_traits' */
		~page_table_entry_tags( void ) noexcept {
			get_properties().raw = 0;
		}

		inline page_table_entry_tags & present( bool present ) noexcept {
			get_properties().bits.p = ( present ) ? 1 : 0;
			return *this;
		}

		inline page_table_entry_tags & access( const Access access ) noexcept {
			get_properties().bits.r_w = to_underlying( access );
			return *this;
		}

		inline page_table_entry_tags & mode( Mode mode ) noexcept {
			get_properties().bits.u_s = to_underlying( mode );
			return *this;
		}

		inline page_table_entry_tags & write_through( bool value ) noexcept {
			get_properties().bits.pwt = ( value ) ? 1 : 0;
			return *this;
		}

		inline page_table_entry_tags & cache_disabled( bool disabled ) noexcept {
			get_properties().bits.pcd = ( disabled ) ? 1 : 0;
			return *this;
		}

		inline bool is_present( void ) const noexcept {
			return get_properties().bits.p;
		}

		inline bool is_accessed( void ) const noexcept {
			return get_properties().bits.a;
		}

		inline bool is_dirty( void ) const noexcept {
			return get_properties().bits.d;
		}

		/* TODO: PAT */

		inline page_table_entry_tags & global( bool value ) noexcept {
			get_properties().bits.g = ( value ) ? 1 : 0;
			return *this;
		}

		inline uint8_t get_use_count( void ) noexcept {
			return get_properties().bits.use_count;
		}

		inline void set_use_count( uint8_t value ) noexcept {
			get_properties().bits.use_count = value;
		}

	private:
		inline properties & get_properties( void ) noexcept {
			return static_cast<TAGGED_PTR &>( *this ).template get_tags();
		}

		inline const properties & get_properties( void ) const noexcept {
			return static_cast<const TAGGED_PTR &>( *this ).template get_tags();
		}
	};

	/* Page table consists of tagged frame pointers */
	using page_table_entry = tagged<unique_ptr<frame>, page_table_entry_tags>;

	/* Check the page table entry size */
	static_assert( sizeof( page_table_entry ) == 4 );

	/* Calculate the page table size - the amount of page_table_entry */
	constexpr static const size_t page_table_size { page_size / sizeof( page_table_entry ) };

	class alignas( page_size ) page_table : private array<page_table_entry, page_table_size> {
		/* Page is used as the interface to manage the virtual address space so shall be the only who might modify the content of page table */
		friend class page;
		/* During the conversion to physical frame pointer, an access to the frame mapping within must be granted */
		friend observer_ptr<frame> frame_cast( const observer_ptr<page> & ) noexcept;

	public:
		static inline const observer_ptr<page_table> get( const page_directory_index index ) noexcept {
			return make_observer<page_table>( page_table_mapping + static_cast<typename page_directory_index::type>( index ) );
		}

		constexpr page_table( void ) noexcept {}

		/* To copy the page table would mean to copy all the underlying unique_ptrs which is not intended.
		 * Thus the copy constructor must be deleted */
		page_table( const page_table & other ) noexcept = delete;

		/* FIXME: Page table destructor shall release all the frames owned */
		~page_table( void ) noexcept = default;
		
		/* TODO: Shall be private/protected and accessible only to retain_traits */
		inline constexpr size_t max_use_count( void ) const noexcept {
			typename page_table_entry_tags<void>::properties properties;

			/* Write "all ones" in there to get the maximum value */
			/* FIXME: warning: conversion from 'uint8_t' {aka 'unsigned char'} to 'unsigned char:3' changes value from '255' to '7' [-Woverflow] */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
			properties.bits.use_count = ~0;
#pragma GCC diagnostic pop

			return properties.bits.use_count;
		}
 
		/* TODO: Shall be private/protected and accessible only to retain_traits */
		inline void set_use_count( const size_t & value ) noexcept {
			this->at( 0 ).set_use_count( value );
		}

		/* TODO: Shall be private/protected and accessible only to retain_traits */
		inline size_t get_use_count( void ) noexcept {
			return this->at( 0 ).get_use_count();
		}

		/* Check whether the frame at given index is present - aka mapped or not */
		inline bool is_present( page_table_index index ) const noexcept {
			return this->get_entry( index ).is_present();
		}

	private:
		inline page_table_entry & get_entry( page_table_index index ) noexcept {
			return this->at( static_cast<typename page_table_index::type>( index ) );
		}

		inline const page_table_entry & get_entry( page_table_index index ) const noexcept {
			return this->at( static_cast<typename page_table_index::type>( index ) );
		}

		inline observer_ptr<frame> get_frame( const page_table_index index ) const noexcept {
			return make_observer( this->get_entry( index ) );
		}

		inline void add( page_table_index index, page_table_entry && pte ) noexcept {
			if( static_cast<typename page_table_index::type>( index ) == 0 ) {
				/* In the free bits of page_table_entry there are metadata stored. So save them prior overwriting once the
				 * affected page_table_entry is written */
				size_t preserve_use_count { this->get_use_count() };
				this->get_entry( index ) = move( pte );
				/* Restore preserved metadata */
				this->set_use_count( preserve_use_count );
			} else {
				this->get_entry( index ) = move( pte );
			}
		}

		inline void remove( page_table_index index ) noexcept {
			/* Destroy frame unique_ptr to deallocate the frame but keep the page_table metadata within - reseat the unique_ptr 
			 * to hold nullptr and the corresponding tags, present=false, paga_table use count preserved */
			if( static_cast<typename page_table_index::type>( index ) == 0 ) {
				size_t preserve_use_count { this->get_use_count() };
				/* Call the frame deleter to deallocate it */
				this->get_entry( index ).reset( nullptr );
				/* Restore preserved metadata */
				this->set_use_count( preserve_use_count );
			} else {
				/* Call the frame deleter to deallocate it */
				this->get_entry( index ).reset( nullptr );
			}
		}
	};

	/* Check page table size which must exactly match the frame / page size */
	static_assert( (sizeof( page_table ) == page_size ), "Invalid Page Table size" );
}

namespace ReVolta {

	template<>
	struct default_allocator<Kernel::page_table> {
		using type = Kernel::frame_allocator<Kernel::page_table>;
	};

	/* Because of page directory self mapping all the page tables are mapped in a special way which requires special handling
	 * of page table retain pointers. The pointer is copied in page directory scope but then the referenced page table is not
	 * manageable from the same scope */
	template<>
	class retain_traits<Kernel::page_table> {
	public:
		using pointer = Kernel::page_table *;
		using allocator_type = allocator<Kernel::page_table>;

		static void increment( pointer entry ) noexcept {
			using namespace Kernel;
			if( entry != nullptr ) {
				/* Paging is not enabled (yet) */
				if( is_paging_enabled() == false ) {
					if( entry->get_use_count() < entry->max_use_count() ) {
						entry->set_use_count( entry->get_use_count() + 1 );
					}
				} else {
					/* FIXME: Map only if not mapped */			
					/* Map page table which reference count is to be incremented to current address space */
					page::map( temporary_page_table, unique_ptr<page_table>{ entry }, Access::READ_WRITE, Mode::SUPERVISOR );						
					/* Increment the use count if applicable */
					if( temporary_page_table->get_use_count() < temporary_page_table->max_use_count() ) {
						temporary_page_table->set_use_count( temporary_page_table->get_use_count() + 1 );
					}
					/* Remove temporary page table mapping. Release the pointer to avoid the destruction of pointee - the page table */
					page::unmap( temporary_page_table ).release();
				}
			}
		}

		static void decrement( pointer entry ) noexcept {
			using namespace Kernel;
			if( entry != nullptr ) {
				if( is_paging_enabled() == false ) {
					if( entry->get_use_count() >= 1 ) {
						entry->set_use_count( entry->get_use_count() - 1 );
					}
					if( entry->get_use_count() == 0 ) {
						allocator_type allocator;
						/* Destroy the page table which use count reached zero and thus is no loger referenced anywhere */
						allocator_traits<allocator_type>::destroy( allocator, entry );
						/* Deallocate the resource */
						allocator_traits<allocator_type>::deallocate( allocator, entry, 1 );
					}
				} else {
					/* Paging enabled here */
					/* FIXME: Map only if not mapped */		
					/* Map page table which reference count is to be incremented to current address space */
					page::map( temporary_page_table, unique_ptr<page_table>{ entry }, Access::READ_WRITE, Mode::SUPERVISOR );
					/* Decrement use count */
					if( temporary_page_table->get_use_count() >= 1 ) {
						temporary_page_table->set_use_count( temporary_page_table->get_use_count() - 1 );
					}
					/* If the use count reaches zero, destroy the page table */
					if( temporary_page_table->get_use_count() == 0 ) {
						allocator_type allocator;
						/* Destroy the page table which use count reached zero and thus is no loger referenced anywhere.
						 * The destruction must happen in virtual address space so let's use the temporary mapping in there */
						allocator_traits<allocator_type>::destroy( allocator, temporary_page_table.get() );
						/* Deallocate page table instance */
						allocator_traits<allocator_type>::deallocate( allocator, entry, 1 );
					}
					/* Remove temporary page table mapping. Release the pointer to avoid the destruction of pointee - the page table */
					page::unmap( temporary_page_table ).release();
				}
			}
		}

		static size_t use_count( pointer entry ) noexcept {
			using namespace Kernel;
			size_t use_count { 0 };
			if( is_paging_enabled() == false ) {
				use_count = entry->get_use_count();
			} else {
				/* Map page table which reference count is to be incremented to current address space */
				page::map( temporary_page_table, unique_ptr<page_table>{ entry }, Access::READ_WRITE, Mode::SUPERVISOR );						
				use_count = temporary_page_table->get_use_count();
				/* Remove temporary page table mapping. Release the pointer to avoid the destruction of pointee - the page table */
				page::unmap( temporary_page_table ).release();
			}
			return use_count;
		}
	};

}