#include "frame_allocator.hpp"
#include "page.hpp"
#include "config.hpp"
#include "mutex.hpp"

namespace ReVolta::Kernel {

    [[nodiscard]] raw_frame_allocator::pointer raw_frame_allocator::allocate( [[maybe_unused]] size_type n, const void * hint ) const noexcept {
        /* The pointer to the frame being allocated */
        pointer allocated_frame { nullptr };
        /* Once 'hint' is valid entry and is properly aligned, set 'allocated_frame' as the given 'hint' */
        if( ( hint != invalid_ptr ) && ( is_aligned<page_size>( hint ) ) ) {
            allocated_frame = pointer { const_cast<frame *>( reinterpret_cast<const frame *>( hint ) ) };
        } else {
            /* Lock head page for the entire block. Blocks there if the lock is acquired in other thread */
            const lock_guard<mutex> head_page_lock { sysconf::get()->get_config<Feature::MEMORY>().head_frame_mutex() };
            if( is_paging_enabled() ) {
                /* If HEAD is available in virtual address space - is mapped */
                if( head_page->is_mapped() == true ) {
                    /* If current HEAD frame's NEXT is nullptr - the HEAD is leaf... */
                    if( page::as_frame( head_page )->is_leaf() ) {
                        frame * next_frame { physical_cast<frame>( head_page ) + 1 };
                        /* Once the calculated 'next_frame' fits the current partition - means is available for further allocation, allocate it
                         * and map into virtual address space as 'next_head_page' */
                        if( sysconf::get()->get_config<Feature::MEMORY>().fits_partition( next_frame ) ) {
                            page::map( next_head_page, unique_ptr<frame, forward_deleter<frame>>{ next_frame }, Access::READ_WRITE, Mode::SUPERVISOR );
                            /* Construct the frame mapped previously */
                            construct_at( page::as_frame( next_head_page ).get(), nullptr );
                            /* The mapping of NEXT is no longer needed */
                            page::as_frame( head_page )->m_next_frame = page::unmap( next_head_page );
                        }
                    }
                    /* Remap the former 'head_page" to the head page's next frame and return the original frame being mapped as 'allocated_frame' */
                    allocated_frame = page::remap( head_page, move( page::as_frame( head_page )->m_next_frame ) ).release();
                }
            } else {
                unique_ptr<frame, forward_deleter<frame>> & head_frame { sysconf::get()->get_config<Feature::MEMORY>().head_frame() };
                if( head_frame != nullptr ) {
                    /* First step is to ensure the NEXT frame is either already set (HEAD is not LEAF) or is newly allocated */
                    /* If new HEAD is a LEAF */
                    if( head_frame->is_leaf() ) {
                        frame * next_frame { head_frame.get() + 1 };
                        /* Once the calculated 'next_frame' fits the current partition - means is available for further allocation,
                         * construct another frame to be the LEAF. As the LEAF is the last frame (with highest address), the next
                         * LEAF is simply the next one */
                        if( sysconf::get()->get_config<Feature::MEMORY>().fits_partition( next_frame ) ) {
                            head_frame->m_next_frame.reset( construct_at( next_frame, nullptr ) );
                        }
                    }
                    unique_ptr<frame, forward_deleter<frame>> next_head_frame { move( head_frame->m_next_frame ) };
                    /* The current HEAD is the frame being allocated */
                    allocated_frame = head_frame.release();
                    /* The new HEAD is the already constructed next Frame */
                    head_frame = move( next_head_frame );
                }
            }
            /* NOTE: head page lock released here */
        }
        return allocated_frame;
    }

    void raw_frame_allocator::deallocate( pointer ptr, [[maybe_unused]] size_type n ) const noexcept {
        /* Do not deallocate any frame below 1MB physical address - it is reserved memory, including 'nullptr' */
        /* TODO: replace the 1MB frame by a fixed_ptr<frame> */
        if( ptr >= reinterpret_cast<pointer>( 1_MB ) ) {
            if( is_paging_enabled() == true ) {
                /* Map the 'ptr' as new HEAD page and store the original one for further uses as NEXT */
                unique_ptr<frame, forward_deleter<frame>> next_frame = page::remap( head_page, unique_ptr<frame, forward_deleter<frame>>{ ptr } );
                /* Construct the new HEAD @'ptr' sinking the 'next_frame' in there to link the frames */                
                construct_at( page::as_frame( head_page ).get(), move( next_frame ) );
                /* NOTE: Keep the head_frame mapped for next use */
            } else {
                unique_ptr<frame, forward_deleter<frame>> & head_frame { sysconf::get()->get_config<Feature::MEMORY>().head_frame() };
                /* In place of the frame being deallocated, construct new Frame which links to the current HEAD.
			 	 * It results of the frame being deallocated to prepend to the 'chain of frames' */ 
                /* Sink current 'head_frame' into the frame constructor making it owned by the frame being constructed @'ptr' location,
                 * create unique_ptr out of that ('ptr') and assign it to head_frame (which is made 'nullptr' before (thanks to move( head_frame )) )
                 * so no deleter gets called and effectively newly constructed unique_ptr @ ptr is prepended to the former HEAD */
                head_frame = unique_ptr<frame, forward_deleter<frame>>{ construct_at( ptr, move( head_frame ) ) };
            }
        }
    }

}