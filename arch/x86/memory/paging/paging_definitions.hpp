#pragma once

#include "config.hpp"
#include "cstddef.hpp"

namespace ReVolta::Kernel {

    /* TODO: Confirm values */
	enum class Access {
		READ_ONLY,
		READ_WRITE
	};

	enum class Mode {
		USER,
		SUPERVISOR
	};

}