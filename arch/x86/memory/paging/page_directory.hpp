#pragma once

#include "page_table.hpp"
#include "initializer_list.hpp"
#include "utility.hpp"

namespace ReVolta::Kernel {

	template<typename TAGGED_PTR>
	class page_directory_entry_tags {
		friend class page_directory;

	private:
		union properties {
			constexpr properties( void ) noexcept : raw( 0 ) {}

			struct {
				uint8_t p 			: 1 { false };									/* [ 0 ] Present - Set to 1 once the PDE references a page table, 0 otherwise */
				uint8_t r_w			: 1 { to_underlying( Access::READ_WRITE ) };	/* [ 1 ] Read/Write = writable? [true/false] */
				uint8_t u_s			: 1 { to_underlying( Mode::USER ) };			/* [ 2 ] User/Supervisor = User accessible [true/false] */
				uint8_t pwt			: 1 { false };									/* [ 3 ] Page-level write through */
				uint8_t pcd			: 1 { false };									/* [ 4 ] Page-level cache disable */
				uint8_t a 			: 1 { 0 };										/* [ 5 ] Accessed - Indicates whether this entry has been used for linear address translation */
				uint8_t 			: 1;											/* [ 6 ] equal to 0 */
				uint8_t ps			: 1 { 0 };										/* [ 7 ] Ignored in 32-bit Page Directory Entry */
				uint8_t				: 1;											/* [ 8 ] Reserved - to force use_count, which follows be mapped to the same bits as in page_table to have that convertible */
				uint8_t use_count 	: 3 { 0 };										/* [ 9.. 11 ] Ignored by paging, used as counter for retain_ptr */
			} bits;
			uint16_t raw : 12;
		}__attribute__(( packed ));

	public:
		using tags_type = properties;

		constexpr static size_t required_tags_bit_width { 12 };

		constexpr page_directory_entry_tags( void ) noexcept {}

		~page_directory_entry_tags( void ) noexcept {
			get_properties().raw = 0;
		}

		page_directory_entry_tags & present( bool present ) noexcept {
			get_properties().bits.p = ( present ) ? 1 : 0;
			return *this;
		}

		page_directory_entry_tags & access( Access access ) noexcept {
			get_properties().bits.r_w = to_underlying( access );
			return *this;
		}

		page_directory_entry_tags & mode( Mode mode ) noexcept {
			get_properties().bits.u_s = to_underlying( mode );
			return *this;
		}

		page_directory_entry_tags & write_through( bool value ) noexcept {
			get_properties().bits.pwt = ( value ) ? 1 : 0;
			return *this;
		}

		page_directory_entry_tags & cache_disabled( bool disabled ) noexcept {
			get_properties().bits.pcd = ( disabled ) ? 1 : 0;
			return *this;
		}

		inline bool is_present( void ) const noexcept {
			return get_properties().bits.p;
		}

		inline bool is_accessed( void ) const noexcept {
			return get_properties().bits.a;
		}

		inline uint8_t get_use_count( void ) noexcept {
			return get_properties().bits.use_count;
		}

		inline void set_use_count( uint8_t value ) noexcept {
			get_properties().bits.use_count = value;
		}

	private:
		inline properties & get_properties( void ) noexcept {
			return static_cast<TAGGED_PTR &>( *this ).template get_tags();
		}

		inline const properties & get_properties( void ) const noexcept {
			return static_cast<const TAGGED_PTR &>( *this ).template get_tags();
		}
	};

	using page_directory_entry = tagged<retain_ptr<page_table>, page_directory_entry_tags>;

	/* Check the size of page directory entry */
	static_assert( sizeof( page_directory_entry ) == 4 );

	/* Calculate the page direcotry size - how many elements are stored within. */
	constexpr static const size_t page_directory_size { page_size / sizeof( page_directory_entry ) };

	class alignas( page_size ) page_directory : private array<page_directory_entry, page_directory_size> {
		/* Page is designed to be the interface to virtual memory management */
		friend class page;

	public:
		/* Once the paging is enabled, the page directory is mapped to itself at fixed virtual address. This is the accessor 
		 * function returning this address */
		static constexpr inline fixed_ptr<page_directory> get( void ) noexcept {
			/* Page directory is mapped to the last page_directory_entry. The size of page directory is fixed to 1024, so the last 
			 * element is 1023 which makes up the address by storing those bits in uppermost 10bits of page directory pointer */
			return page_directory_mapping;
		}

		/* Default constructor constructs empty page directory and maps itself to defined location in virtual address space 
		 * so that the paging strucures are accessible even once the paging is enabled */
		page_directory( void ) noexcept {
			/* Map the first 1MiB of physical memory as reserved area */
			for( size_t frame_index { 0 }; frame_index < ( 1_MB / page_size ); frame_index++ ) {
				page::map( 
					make_observer( this ),
					make_observer<page>( higher_half + frame_index ),
					/* NOTE: 'allocate_unique_hint() cannot be used here as internally it calls the constructor (frame c-tor) which is not intended in this case */		
					unique_ptr<frame>( allocator_traits<allocator<frame>>::allocate( allocator<frame> {}, 1, reinterpret_cast<frame *>( frame_index * page_size ) ) ), 
					Access::READ_WRITE, 
					Mode::SUPERVISOR
				);
			}
			/* Check the validity of the self pointer - effectively constructs the sysconf instance within a frame if not yet done previously */
			if( sysconf::get() != nullptr ) {
                /* Identify the paging structures indices */
				/* FIXME: In principle, it works, to identify the right page_table using the head page mapping but might be defined better way or? */
                constexpr paging<fixed_ptr<page>> indices { head_page };
				emplace( indices.get_page_directory_index(), Access::READ_WRITE, Mode::SUPERVISOR );
			}
			/* Page directory self mapping. It does not increment the use count to break down internal dependency
			 * (retaint pointer to itself prevents easy destruction) */
			this->back() = page_directory_entry { physical_cast<page_table>( this ), false };
			/* Setup page directory self mapping properties */
			this->back().present( true ).access( Access::READ_WRITE ).mode( Mode::SUPERVISOR );
		}

		page_directory( const page_directory & other ) noexcept {
			/* Copy all the pointers to page tables EXCEPT THE LAST 1024th ENTRY which is reserved for page directory
			 * self mapping which is unique for each page directory instance */
			for( size_t pde_index = 0; pde_index < ( page_directory_size - 1); pde_index++ ) {
				if( other.at( pde_index ).is_present() ) {
					/* Internally, as retain_ptr operator= is used, the reference count of all page tables is incremented.
					 * This operation is quite expensive as many mappings and unmappings need to be performed */
					this->at( pde_index ) = other.at( pde_index );
				}
			}
			/* Page directory self mapping. It does not increment the use count to break down internal dependency
			 * (retaint pointer to itself prevents easy destruction) */
			this->back() = page_directory_entry { physical_cast<page_table>( this ), false };
			/* Setup page directory self mapping properties */
			this->back().present( true ).access( Access::READ_WRITE ).mode( Mode::SUPERVISOR );
		}

		/* During page_directory construction, add given page directory entries within */
		page_directory( initializer_list<pair<page_directory_index, page_directory_entry>> init ) noexcept : page_directory() {
			this->add( init );
		}

		~page_directory( void ) noexcept = default;

		/* TODO: Shall be private/protected and accessible only to retain_traits */
		inline constexpr size_t max_use_count( void ) const noexcept {
			typename page_directory_entry_tags<void>::properties properties;

			/* Write "all ones" in there to get the maximum value */
			/* FIXME: warning: conversion from 'uint8_t' {aka 'unsigned char'} to 'unsigned char:3' changes value from '255' to '7' [-Woverflow] */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
			properties.bits.use_count = ~0;
#pragma GCC diagnostic pop

			return properties.bits.use_count;
		}
 
		/* TODO: Shall be private/protected and accessible only to retain_traits */
		inline void set_use_count( const size_t & value ) noexcept {
			this->at( 0 ).set_use_count( value );
		}

		/* TODO: Shall be private/protected and accessible only to retain_traits */
		inline size_t get_use_count( void ) noexcept {
			return this->at( 0 ).get_use_count();
		}

		inline bool is_present( page_directory_index index ) const noexcept {
			return this->get_entry( index ).is_present();
		}

		/* Emplace new page table if it is not already existing */
		page_directory_entry & emplace( page_directory_index index, const Access writable, const Mode user_accessible ) noexcept {
			if( !is_present( index ) ) {
				this->get_entry( index ) = allocate_tagged<retain_ptr, page_table, page_directory_entry_tags>( allocator<page_table> {} );
				this->get_entry( index ).present( true ).access( writable ).mode( user_accessible );
			}
			return this->get_entry( index );
		}

	private:
		inline page_directory_entry & get_entry( page_directory_index index ) noexcept {
			return this->at( static_cast<typename page_directory_index::type>( index ) );
		}

		inline const page_directory_entry & get_entry( page_directory_index index ) const noexcept {
			return this->at( static_cast<typename page_directory_index::type>( index ) );
		}

		void add( initializer_list<pair<page_directory_index, page_directory_entry>> init ) noexcept {
			/* FIXME: Shall check the presence of corresponding page tables within */
			for( pair<page_directory_index, page_directory_entry> element : init ) {
				/* FIXME: What if the element of initializer list remaps the self-mapped page directory (the last PDE) */
				this->get_entry( element.first ) = element.second;
			}
		}
	};
	
	static_assert( sizeof( page_directory ) == page_size );

	template<typename TAGGED_PTR>
	class page_directory_pointer_tags {
	private:
		/* Properties as defined in CPU CR3 control register */
		union properties {
			struct {
				uint8_t 	: 3;
				uint8_t pwt	: 1 { 0 };	/* [ 3 ] Page-level Write-Through */
				uint8_t pcd	: 1 { 0 };	/* [ 4 ] Page-level Cache Disable */
			} bits;
			uint8_t raw : 5;
		}__attribute__((packed));

	public:
		using tags_type = properties;

		constexpr static size_t required_tags_bit_width { 5 };

		constexpr page_directory_pointer_tags( void ) noexcept {}

		~page_directory_pointer_tags( void ) noexcept {
			get_properties().raw = 0;
		}

		page_directory_pointer_tags & page_level_write_through( bool enabled ) noexcept {
			get_properties().bits.pwt = ( enabled ) ? 1 : 0;
			return *this;
		}

		page_directory_pointer_tags & page_level_cache_disabled( bool disabled ) noexcept {
			get_properties().bits.pcd = ( disabled ) ? 1 : 0;
			return *this;
		}

	private:
		inline properties & get_properties( void ) noexcept {
			return static_cast<TAGGED_PTR &>( *this ).template get_tags();
		}

		inline const properties & get_properties( void ) const noexcept {
			return static_cast<const TAGGED_PTR &>( *this ).template get_tags();
		}
	};

}

namespace ReVolta {

	template<>
	struct default_allocator<Kernel::page_directory> {
		using type = Kernel::frame_allocator<Kernel::page_directory>;
	};

	/* The deletion of page directory requires a special treatment as is not directly mapped to virtual address space.
	 * That is why there is parial specialization of the deleter for page directory using any allocator */
	template<template<typename> class ALLOCATOR>
	class deleter<ALLOCATOR<Kernel::page_directory>> {
	public:
		using pointer = typename ALLOCATOR<Kernel::page_directory>::pointer;
		using value_type = typename ALLOCATOR<Kernel::page_directory>::value_type;
		using allocator_type = ALLOCATOR<Kernel::page_directory>;

		/* Default deleter constructor */
		constexpr deleter( void ) noexcept = default;

		void operator()( pointer ptr ) const noexcept {
			if( ptr != nullptr ) {
				allocator_type allocator;
				/* Destruct the instance being deleted */

				/* FIXME: This is going to work only if the page directory being destructed is the one currently made active - is mapped to virtual 
				 * address space 
				 * --> the solution might be to map whatever PD we are about to destroy to current address space as dedicated page directory entry,
				 * let is destroyed and then unmap it as well */

				/* Destroy the page directory using its linear address where is mapped */
				allocator_traits<allocator_type>::destroy( allocator, Kernel::page_directory::get().get() );
				/* Deallocate resource */
				allocator_traits<allocator_type>::deallocate( allocator, ptr, 1 );
			}
		}
	};

}