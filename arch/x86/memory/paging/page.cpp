#include "page_directory.hpp"
#include "config.hpp"
#include "control_registers/cr3.hpp"

namespace ReVolta::Kernel {

    bool page::is_mapped( void ) const noexcept {
        bool mapped { false };
        paging<observer_ptr<page>> indices { make_observer( const_cast<page *>( this ) ) };
        /* Check whether the page table itself is present... */
        if( ( page_directory::get()->is_present( indices.get_page_directory_index() ) )
            /* ...and within that present page the related frame is present ... */
            && ( page_table::get( indices.get_page_directory_index() )->is_present( indices.get_page_table_index() ) ) ) {
            /* ...indicate the frame is present and thus the mapping is set */     
            mapped = true;
        }
        /* Return the result */
        return mapped;
    }

    observer_ptr<page> page::map( observer_ptr<page_directory> page_directory, const observer_ptr<page> mapping, unique_ptr<frame> source, const Access writable, const Mode user_accessible ) noexcept {
        /* The case of mapping to NULL virtual address is forbidden - to detect the null pointer dereference using proper page fault detection */
        if( mapping != nullptr ) {
            /* Identify the paging structures indices */
            const paging<observer_ptr<page>> indices { mapping };
            page_table_entry pte { move( source ) };
            pte.present( true ).access( writable ).mode( user_accessible );
            page_directory->emplace( indices.get_page_directory_index(), writable, user_accessible )->add( indices.get_page_table_index(), move( pte ) );
        }
        return mapping;
    }

    /* This overload of page::map() is really tricky part. In normal page::map the function itself sinks the source unique_ptr. But in case of sysconf, once the sysconf singleton pointer is sunken
     * inside the function itself, it is not available to the frame_allocator any more which makes it unable to allocate new page table to hold the sysconf mapping. Therefore this special
     * overload is implemented to not to sink sysconf self pointer until the page_table is created */
    observer_ptr<page> page::map( observer_ptr<page_directory> page_directory, const observer_ptr<sysconf> mapping, const Access writable, const Mode user_accessible ) noexcept {
        /* The case of mapping to NULL virtual address is forbidden - to detect the null pointer dereference using proper page fault detection */
        if( mapping != nullptr ) {
            /* Identify the paging structures indices */
            const paging<observer_ptr<page>> indices { make_observer<page>( mapping ) };
            page_directory->emplace( indices.get_page_directory_index(), writable, user_accessible );
            /* Now, page directory and page table are all in place and present, so add the mapping */
            /* NOTE: Move the pointer from 'sysconf::m_self' to 'pte' while no deleter is moved, just converted pointer */
            page_table_entry pte { reinterpret_cast<frame *>( sysconf::m_self.release() ) };
            pte.present( true ).access( writable ).mode( user_accessible );
            page_directory->get_entry( indices.get_page_directory_index() )->add( indices.get_page_table_index(), move( pte ) );
        }
        return make_observer<page>( mapping );
    }

    observer_ptr<page> page::map( const observer_ptr<page> mapping, unique_ptr<frame> source, const Access writable, const Mode user_accessible ) noexcept {
        /* The case of mapping to NULL virtual address is forbidden - to detect the null pointer dereference using proper page fault detection */
        if( ( mapping != nullptr ) && ( is_paging_enabled() ) ) {
            /* Identify the paging structures indices */
            const paging<observer_ptr<page>> indices { mapping };
            page_directory::get()->emplace( indices.get_page_directory_index(), writable, user_accessible );
            /* Now, page directory and page table are all in place and present, so add the mapping */
            page_table_entry pte { move( source ) };
            pte.present( true ).access( writable ).mode( user_accessible );
            page_table::get( indices.get_page_directory_index() )->add( indices.get_page_table_index(), move( pte ) );
        }
        return mapping;
    }

    unique_ptr<frame> page::remap( const observer_ptr<page> mapping, unique_ptr<frame> source, const Access writable, const Mode user_accessible ) noexcept {
        if( is_paging_enabled() == true ) {
            /* Identify the paging structures indices */
            const paging<observer_ptr<page>> indices { mapping };
            /* Check whether the page identified by 'mapping' is already mapped */
            if( mapping->is_mapped() == true ) {
                /* If remapping to nullptr, it effectively means to unmap the page while returning the underlying frame, not to destroy it, at least not in remap() scope */
                if( source != nullptr ) {
                    /* Create page table entry - the tagged frame pointer to be exchanged with the one in page table */
                    page_table_entry pte { move( source ) };
                    pte.present( true ).access( writable ).mode( user_accessible );
                    /* Swap page table entries */
                    swap( page_table::get( indices.get_page_directory_index() )->get_entry( indices.get_page_table_index() ), pte );
                    /* The change in frame mapping has been perfomed, flush the TLB */
                    page::flush_tlb( mapping );
                    /* Now the 'pte' contains the tagged frame unique pointer to be returned */
                    return static_cast<unique_ptr<frame>>( move( pte ) );
                } else {
                    /* The change in frame mapping has been perfomed, flush the TLB */
                    page::flush_tlb( mapping );
                    /* Effectively, this replaces the orginally stored mapping by nullptr - means to unmap the virtual address */
                    return static_cast<unique_ptr<frame>>( move( page_table::get( indices.get_page_directory_index() )->get_entry( indices.get_page_table_index() ) ) );
                }
            } else {
                /* Once the page is not mapped, it behaves identically to page::map() */
                page::map( mapping, move( source ), writable, user_accessible );
                return { nullptr };
            }
        } else {
            /* Paging is disabled, no remapping possible */
            return { nullptr };
        }
    }

    unique_ptr<frame, forward_deleter<frame>> page::unmap( const observer_ptr<page> entry ) noexcept {
        unique_ptr<frame, forward_deleter<frame>>  unmapped_frame { nullptr };
        const paging<observer_ptr<page>> indices { entry };
        if( page_directory::get()->is_present( indices.get_page_directory_index() ) == true ) {
            unmapped_frame.reset( page_table::get( indices.get_page_directory_index() )->get_entry( indices.get_page_table_index() ).release() );
            page::flush_tlb( entry );
        }
        return move( unmapped_frame );
    }

    /* TODO: Once multiprocessor support is added (SMP), the entry must be invalidated on all the processors */
    void page::flush_tlb( const observer_ptr<page> entry ) noexcept {
        asm volatile ( "invlpg (%0)" :: "r" ( entry.get() ) : "memory" );
    }

    /* TODO: Once multiprocessor support is added (SMP), the entry must be invalidated on all the processors */
    void page::flush_tlb( void ) noexcept {
        cr3{}.reload();
    }

    void * page::get_physical_address( const void * linear_address ) noexcept {
        void * physical_address { nullptr };
        /* frame_cast works only if paging is enabled */
        if( is_paging_enabled() ) {
            /* FIXME: This multiple conversions are pretty complicated, try to invent better way to do so */
            const paging<observer_ptr<int>> indices { reinterpret_cast<int *>( const_cast<void *>( linear_address ) ) };
            /* First of all check whether the required page table which holds the frame mapping is present at all. This must be done in page directory itself (holds the pointers to page tables)  */
            if( page_directory::get()->is_present( indices.get_page_directory_index() ) ) {
                /* Get the page table checked above and check whether the frame mapping is present */
                if( page_table::get( indices.get_page_directory_index() )->is_present( indices.get_page_table_index() ) ) {
                    /* Page table as well as the frame mapping is present so calculate the physical address from there */
                    physical_address = ( page_table::get( indices.get_page_directory_index() )->get_frame( indices.get_page_table_index() ) ).get();
                }
            }
        } else {
            physical_address = const_cast<void *>( linear_address );
        }
        return physical_address;
    }

}