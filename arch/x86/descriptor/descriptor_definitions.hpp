#pragma once

#include "cstdint.hpp"

namespace ReVolta::Kernel {

	enum class privilege_level : uint8_t {
		RING_0 = 0,		/* Kernel protection level (most privileged) */
		RING_1 = 1,		/* Device drivers */
		RING_2 = 2,		/* Device drivers */
		RING_3 = 3		/* Userspace applications protection level (least privileged) */
	};

	enum class descriptor_table_type : uint8_t {
		GDT = 0,
		LDT = 1
	};

	/* See: Chapter 3.5 - Table 3-2 System-segment and Gate-descriptor types */
	/* TODO: Change the name */
	enum class system_segment_type : uint8_t {
		/* RESERVED 			= 0 */
		TSS_16BIT_AVAILABLE		= 1,
		LDT						= 2,
		TSS_16BIT_BUSY			= 3,
		CALL_GATE_16BIT			= 4,
		TASK_GATE				= 5,
		INTERRUPT_GATE_16BIT	= 6,
		TRAP_GATE_16BIT			= 7,
		/* RESERVED 			= 8 */
		TSS_32BIT_AVAILABLE		= 9,
		/* RESERVED 			= 10 */
		TSS_32BIT_BUSY			= 11,
		CALL_GATE_32BIT 		= 12,
		/* RESERVED 			= 13 */
		INTERRUPT_GATE_32BIT	= 14,
		TRAP_GATE_32BIT			= 15
	};

}