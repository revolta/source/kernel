#pragma once

#include "cstdint.hpp"
#include "type_traits.hpp"
#include "descriptor_definitions.hpp"
#include "segment_selector.hpp"

namespace ReVolta::Kernel {

    class gate_descriptor {
    public:
        constexpr gate_descriptor( void ) noexcept = default;

    protected:
        /* NOTE/PRINCIPLE EXPLANATION:
         * The key property of the gate descriptor is the offset. The original way to design it is to match the segmentation scheme.
         * It is intended to work with the selector together. The selector defines the segment being used, as defined in GDT in protected
         * mode and the offset (made of offset_low and offset_high) defines the offset within that segment.
         * While flat segmentation model is used which defines the code segment to be the full 32bit address space (0x00000000 - 0xFFFFFFFF)
         * which makes the segmentation effectively bypassed, the offset within this segment is equal to the pointer, the address within
         * the whole address space. So the offset is the address/pointer.
         */
        template<typename P>
        constexpr gate_descriptor( P ptr, segment_selector selector, system_segment_type type, bool present, privilege_level level ) noexcept 
            : offset_low( reinterpret_cast<uintptr_t>( ptr ) & 0x0000FFFF ),
              sel( selector ),
              type( to_underlying( type ) ),
              dpl( to_underlying( level ) ),
              p( (present) ? 1 : 0 ),
              offset_high( ( reinterpret_cast<uintptr_t>( ptr ) & 0xFFFF0000 ) >> 16 )
        {}

    private:
        uint16_t offset_low         : 16;       /* [ 0  .. 15 ] Offset to procedure entry point (lower 16bits). See the note aside of gate_descriptor constructor */
        uint16_t sel                : 16;       /* [ 16 .. 31 ] Segment selector for destination code segment in GDT or LDT */
        uint8_t  unused             : 8 { 0 };  /* [ 32 .. 39 ] Reserved, shall be set to '0' */
        uint8_t  type               : 5;        /* [ 40 .. 44 ] Descriptor type */
        uint8_t  dpl                : 2;        /* [ 45 .. 46 ] Descriptor privilege level */
        uint8_t  p                  : 1;        /* [ 47 ]       Segment present flag */
        uint16_t offset_high        : 16;       /* [ 48 .. 63 ] Offset to procedure entry point (higher 16bits) */
    } __attribute__(( packed ));

    static_assert( sizeof( gate_descriptor ) == 8 );

    struct task_gate_descriptor : public gate_descriptor {
        constexpr task_gate_descriptor( segment_selector tss_segment_selector, bool present = true, privilege_level level = privilege_level::RING_0 ) noexcept
            : gate_descriptor( nullptr, tss_segment_selector, system_segment_type::TASK_GATE, present, level )
        {}
    };

    template<typename INTERRUPT_HANDLER>
    struct interrupt_gate_16bit_descriptor : public gate_descriptor {
        constexpr interrupt_gate_16bit_descriptor( INTERRUPT_HANDLER * handler, segment_selector selector, bool present = true, privilege_level level = privilege_level::RING_0 ) noexcept
            : gate_descriptor( handler, selector, system_segment_type::INTERRUPT_GATE_16BIT, present, level )
        {}
    };

    template<typename INTERRUPT_HANDLER>
    struct interrupt_gate_32bit_descriptor : public gate_descriptor {
        constexpr interrupt_gate_32bit_descriptor( INTERRUPT_HANDLER * handler, segment_selector selector, bool present = true, privilege_level level = privilege_level::RING_0 ) noexcept
            : gate_descriptor( handler, selector, system_segment_type::INTERRUPT_GATE_32BIT, present, level )
        {}
    };

    template<typename TRAP_HANDLER>
    struct trap_gate_16bit_descriptor : public gate_descriptor {
        constexpr trap_gate_16bit_descriptor( TRAP_HANDLER * handler, segment_selector selector, bool present = true, privilege_level level = privilege_level::RING_0 ) noexcept
            : gate_descriptor( handler, selector, system_segment_type::TRAP_GATE_16BIT, present, level )
        {}
    };

    template<typename TRAP_HANDLER>
    struct trap_gate_32bit_descriptor : public gate_descriptor {
        constexpr trap_gate_32bit_descriptor( TRAP_HANDLER * handler, segment_selector selector, bool present = true, privilege_level level = privilege_level::RING_0 ) noexcept
            : gate_descriptor( handler, selector, system_segment_type::TRAP_GATE_32BIT, present, level )
        {}
    };

}