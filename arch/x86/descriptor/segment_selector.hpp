#pragma once

#include "descriptor_definitions.hpp"
#include "utility.hpp"
#include "cstdint.hpp"

namespace ReVolta::Kernel {

    /* TODO: What is the difference between segment_selector and descriptor_selector? Is that the same? */

    /* Segment selector is a 16-bit identifier for a segment. It does not point directly to the segment,
     * but instead points to the segment descriptor that defines the segment.
     *
     * Segment selector is a reference to a descriptor you can load into a segment register; the selector is an offset of a descriptor table entry.
     * These entries are 8 bytes long. Therefore, bits 3 and up only declare the descriptor table entry offset, while bit 2 specifies if this selector 
     * is a GDT or LDT selector (LDT - bit set, GDT - bit cleared), and bits 0 - 1 declare the ring level that needs to correspond to the descriptor
     * table entry's DPL field. If it doesn't, a General Protection Fault occurs; if it does correspond then the CPL level of the selector used is changed
     * accordingly.
     * 
     * See Intel 64 and IA-32 Architectures Software Developers Manual - Vol 3, Chapter 3.4.2 */
    struct segment_selector {
        using raw_type = uint16_t;

        constexpr segment_selector( privilege_level level, descriptor_table_type indicator, raw_type idx ) noexcept 
            : rpl( to_underlying( level ) ), ti( to_underlying( indicator ) ), index( idx ) {}

        constexpr operator raw_type( void ) noexcept {
            return ( (index << 3) | ( ti << 2 ) | rpl );
        }

        constexpr operator raw_type( void ) const noexcept {
            return ( (index << 3) | ( ti << 2 ) | rpl );
        }

        const uint8_t     rpl     : 2;    /* [ 0 .. 1 ]  Requested privilege level */
        const uint8_t     ti      : 1;    /* [ 2 ]       Table indicator - GDT = 0, IDT = 1 */
        const uint16_t    index   : 13;   /* [ 3 .. 15 ] Selects one of 8192 (2^13) descriptors in GDT or LDT */
    } __attribute__((packed));

    static_assert( sizeof( segment_selector ) == 2 );

}