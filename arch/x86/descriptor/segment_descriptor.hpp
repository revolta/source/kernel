#pragma once

#include "descriptor_definitions.hpp"

#include <cstdint.hpp>
#include <cstddef.hpp>
#include <type_traits.hpp>
#include <utility.hpp>

namespace ReVolta::Kernel {

	enum class default_operation_size : uint8_t {
		/* The meaning differs in which segment descriptor type this is being used:
		 * EXECUTABLE CODE SEGMENT: 16-bit addresses and 16-bit or 8-bit operands are assumed
		 * STACK SEGMENT: 16-bit stack pointer is used, which is stored in the 16-bit SP register.
		 *                If the stack segment is set up to be an expand-down data segment it also
		 *                specifies the upper bound of the stack segment.
		 * EXPAND DOWN DATA SEGMENT: The upper bound of the segment is 0xFFFF (64KiB)
		 */
		SEGMENT_16BIT,

		/* The meaning differs in which segment descriptor type this is being used:
		 * EXECUTABLE CODE SEGMENT: 32-bit addresses and 32-bit or 8-bit operands are assumed
		 * STACK SEGMENT: 32-bit stack pointer is used, which is stored in the 32-bit ESP register
		 * EXPAND DOWN DATA SEGMENT: The upper bound of the segment is 0xFFFFFFFF (4GiB)
		 */
		SEGMENT_32BIT
	};

	/* Determines the scaling of the segment limit field
	 * As the limit property is represented by 20bits value it may range from 0 to 1Mi entries (2^20 = 1Mi)
	 */
	enum class granularity : uint8_t {	
		UNIT_BYTE, 		/* Segment size can range from 1Byte to 1MByte in byte increments */
		UNIT_4KB		/* Segment size can range from 4KBytes to 4GBytes in 4-KByte increments */
	};

	/* The accessed bit indicates whether the segment has been accessed since the last time the operating-system or
	 * executive cleared the bit. The processor sets this bit whenever it loads a segment selector for the segment into a
	 * segment register, assuming that the type of memory that contains the segment descriptor supports processor
	 * writes. The bit remains set until explicitly cleared. This bit can be used both for virtual memory management and
	 * for debugging. */
	enum class accessed : uint8_t {
		NOT_ACCESSED,
		ACCESSED
	};

	enum class read_enable : uint8_t {
		EXECUTE_ONLY,
		READ_EXECUTE
	};

	enum class write_enable : uint8_t {
		READ_ONLY,
		READ_WRITE
	};

	/* Code segments can be either conforming or nonconforming. A transfer of execution into a more-privileged
	 * conforming segment allows execution to continue at the current privilege level. A transfer into a nonconforming
	 * segment at a different privilege level results in a general-protection exception (#GP), unless a call gate or
	 * task gate is used */
	enum class conformity : uint8_t {
		NONCONFORMING,
		CONFORMING
	};

	enum class expansion_direction : uint8_t {
		NOT_EXPANDED,	/* TODO: Shall be rather EXPAND_UP? */
		EXPAND_DOWN
	};

	class segment_descriptor {
	public:
		segment_descriptor( void ) = default;

		inline void * get_base_address( void ) const noexcept {
			return reinterpret_cast<void *>( ( ( m_base_address_high << 24 ) & 0xFF000000 ) | ( m_base_address_low & 0x00FFFFFF ) );
		}

		inline size_t get_limit( void ) const noexcept {
			return static_cast<size_t>( ( m_segment_limit_low & 0x0000FFFF ) | ( ( m_segment_limit_high << 16 ) & 0x000F0000 ) );
		}

		inline bool is_null_segment_descriptor( void ) const noexcept {
			return ( ( m_descriptor_type == 0 ) && ( m_type == 0 ) );
		}

		inline bool is_code_segment_descriptor( void ) const noexcept {
			return ( ( static_cast<descriptor_type>( m_descriptor_type ) == descriptor_type::CODE_DATA ) && ( ( m_type & 0x08 ) > 0 ) );
		}

		inline bool is_data_segment_descriptor( void ) const noexcept {
			return ( ( static_cast<descriptor_type>( m_descriptor_type ) == descriptor_type::CODE_DATA ) && ( ( m_type & 0x08 ) == 0 ) );
		}

		inline bool is_system_segment_descriptor( void ) const noexcept {
			return ( ( static_cast<descriptor_type>( m_descriptor_type ) == descriptor_type::SYSTEM ) && ( m_type != 0 ) );
		}

		inline privilege_level get_privilege_level( void ) const noexcept {
			return static_cast<privilege_level>( m_descriptor_priviledge_level );
		}

	protected:
		enum class descriptor_type : uint8_t {
			SYSTEM = 0,
			CODE_DATA = 1
		};

		constexpr segment_descriptor( void * ptr, size_t limit, uint8_t type, descriptor_type descriptor_type, privilege_level level, bool present, default_operation_size op_size, granularity granularity ) noexcept
			: m_segment_limit_low( limit & 0x0000FFFF ),
			  m_base_address_low( reinterpret_cast<uintptr_t>( ptr ) & 0x00FFFFFF ),
			  m_type( type & 0x0F ),
			  m_descriptor_type( to_underlying( descriptor_type ) ),
			  m_descriptor_priviledge_level( to_underlying( level ) ),
			  m_segment_present( (present) ? 1 : 0 ),
			  m_segment_limit_high( ( limit & 0x000F0000 ) >> 16 ),
			  m_available_for_system_sw( 0 ),
			  m_is_64bit_code_segment( 0 ), /* TODO: NOTE: Applies only in IA-32e mode (PAE) which is not being supported now. Shall be set to 0 in non-PAE mode */
			  m_default_operation_size( to_underlying( op_size ) ),
			  m_granularity( to_underlying( granularity ) ),
			  m_base_address_high( ( reinterpret_cast<uintptr_t>( ptr ) & 0xFF000000 ) >> 24 )
		{}

	private:
		uint16_t m_segment_limit_low : 16;
		uint32_t m_base_address_low : 24;		/* Aggregates LOW and MID base address fields */
		uint8_t  m_type : 4;
		uint8_t  m_descriptor_type : 1;
		uint8_t  m_descriptor_priviledge_level : 2;
		uint8_t	 m_segment_present : 1;
		uint8_t  m_segment_limit_high : 4;
		uint8_t  m_available_for_system_sw : 1;
		uint8_t  m_is_64bit_code_segment : 1;
		uint8_t  m_default_operation_size : 1;
		uint8_t  m_granularity : 1;
		uint8_t  m_base_address_high : 8;
	} __attribute__((packed));

	static_assert( sizeof( segment_descriptor ) == 8 );

	namespace Detail {

		struct code_segment_type {
			accessed	m_accessed : 1;			/* Bit8  - A */
			read_enable m_read_enable : 1;		/* Bit9  - R */
			conformity	m_conforming : 1;		/* Bit10 - C */
			uint8_t		m_code_data : 1 { 1 };	/* Bit11 */
		};

		struct data_segment_type {
			accessed 	 m_accessed : 1;		/* Bit8  - A */
			write_enable m_write_enable : 1;	/* Bit9  - W */
			expansion_direction m_expand : 1;	/* Bit10 - E */
			uint8_t		 m_code_data : 1 { 0 };	/* Bit11 */
		};

	}

	class null_segment_descriptor : public segment_descriptor {
	public:
		null_segment_descriptor( void ) noexcept
			: segment_descriptor( nullptr, 0, 0, descriptor_type::SYSTEM, privilege_level::RING_0, false, default_operation_size::SEGMENT_16BIT, granularity::UNIT_BYTE )
		{}
	};

	/* Code-Segment Descriptor
	 * See: Intel 64 and IA-32 Architectures Software Developers Manual - Volume 3 - System Programming Guide, Chapter 5 - Protection */
	struct code_segment_descriptor : public segment_descriptor {
		constexpr code_segment_descriptor( void * address, size_t limit, read_enable readable, privilege_level level = privilege_level::RING_0, conformity conformity = conformity::CONFORMING, granularity granularity = granularity::UNIT_4KB, default_operation_size op_size = default_operation_size::SEGMENT_32BIT ) noexcept
			: segment_descriptor( address, limit, [&]{ Detail::code_segment_type type { accessed::NOT_ACCESSED, readable, conformity }; return *(reinterpret_cast<uint8_t *>( &type )); }(), descriptor_type::CODE_DATA, level, true, op_size, granularity )
		{}
	};

	/* Data-Segment Descriptor
	 * See: Intel 64 and IA-32 Architectures Software Developers Manual - Volume 3 - System Programming Guide, Chapter 5 - Protection */
	struct data_segment_descriptor : public segment_descriptor {
		constexpr data_segment_descriptor( void * address, size_t limit, write_enable writable, privilege_level level = privilege_level::RING_0, expansion_direction expand = expansion_direction::EXPAND_DOWN, granularity granularity = granularity::UNIT_4KB, default_operation_size op_size = default_operation_size::SEGMENT_32BIT ) noexcept
			: segment_descriptor( address, limit, [&]{ Detail::data_segment_type type { accessed::NOT_ACCESSED, writable, expand }; return *(reinterpret_cast<uint8_t *>( &type )); }(), descriptor_type::CODE_DATA, level, true, op_size, granularity )
		{}
	};

	/* System-Segment Descriptor
	 * See: Intel 64 and IA-32 Architectures Software Developers Manual - Volume 3 - System Programming Guide, Chapter 5 - Protection */
	class system_segment_descriptor : public segment_descriptor {
	protected:
		constexpr system_segment_descriptor( void * address, size_t limit, system_segment_type type, privilege_level level = privilege_level::RING_0, granularity granularity = granularity::UNIT_4KB, default_operation_size op_size = default_operation_size::SEGMENT_32BIT ) noexcept
			: segment_descriptor( address, limit, to_underlying( type ), descriptor_type::SYSTEM, level, true, op_size, granularity )
		{}
	};

	class task_state_segment;

	/* FIXME:
	 * - what shall be the privilege level for TSS descriptor?
	 * - what shall be the limit settings for TSS descriptor?
	 */
	struct task_state_segment_descriptor : public system_segment_descriptor {
		constexpr task_state_segment_descriptor( task_state_segment * tss ) noexcept
			: system_segment_descriptor( tss, 0, system_segment_type::TSS_32BIT_AVAILABLE, privilege_level::RING_0 )
		{}
	};

	/* TODO: Define more specializations of system_segment_descriptor where needed
	 * - LDT?
	 * - IDT - Interrupt descriptor table
	 * - ...
	 */

#if false
	/* Segment Descriptor pretty printer */
	template<typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, segment_descriptor & descriptor ) noexcept {
		if( descriptor.is_null_segment_descriptor() ) ostream << "NULL";
		if( descriptor.is_code_segment_descriptor() ) ostream << "CODE";
		if( descriptor.is_data_segment_descriptor() ) ostream << "DATA";
		if( descriptor.is_system_segment_descriptor() ) ostream << "SYSTEM";

		return ostream;
	}
#endif

}
