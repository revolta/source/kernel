namespace ReVolta::Kernel {
    /* See:
     * https://gcc.gnu.org/onlinedocs/gcc/x86-Function-Attributes.html#x86-Function-Attributes
     *
     * Intel manual: 6.12.1 Exception- or Interrupt-Handler Procedures, 6-12 Vol.3A 
     * 
     * NO PRIVILEGE LEVEL CHANGE STACK FRAME:
     * - EFLAGS
     * - CS
     * - EIP
     */ 

#if false
    /* TODO: define struct interrupt_frame as described in the processor’s manual */
    struct interrupt_frame;
 
    /* Sample INTERRUPT handler */
    /* NOTE: If the interrupt attribute is used, the iret instruction will be used instead of ret on x86 */
    __attribute__((interrupt)) void interrupt_handler( struct interrupt_frame* frame ) {
        ...
    }

    __attribute__ ((interrupt)) void exception_handler( struct interrupt_frame *frame, uword_t error_code ) {
        ...
    }

#endif

}