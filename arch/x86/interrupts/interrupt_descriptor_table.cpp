
#include "interrupt_descriptor_table.hpp"
#include "interrupt_frame.hpp"
#include "error_code.hpp"
#include "segmentation.hpp"

namespace ReVolta::Kernel {

    void divide_by_zero( interrupt_frame * frame );

    void debug( interrupt_frame * frame );

    void non_maskable_interrupt( interrupt_frame * frame );

    void breakpoint( interrupt_frame * frame );

    void overflow( interrupt_frame * frame );

    void bound_range_exceeded( interrupt_frame * frame );

    void invalid_opcode( interrupt_frame * frame );

    void device_not_available( interrupt_frame * frame );

    void double_fault( interrupt_frame * frame, raw_error_code error_code );

    void invalid_tss( interrupt_frame * frame, raw_error_code error_code );

    void segment_not_present( interrupt_frame * frame, raw_error_code error_code );

    void stack_segment_fault( interrupt_frame * frame, raw_error_code error_code );

    void general_protection_fault( interrupt_frame * frame, raw_error_code error_code );

    void page_fault( interrupt_frame * frame, raw_error_code error_code );
    
    void x87_floating_point_exception( interrupt_frame * frame );

    void alignment_check( interrupt_frame * frame, raw_error_code error_code );

    void machine_check( interrupt_frame * frame );

    void simd_flaoting_point_exception( interrupt_frame * frame );

    void virtualization_exception( interrupt_frame * frame );

    void security_exception( interrupt_frame * frame, raw_error_code error_code );

    /* TODO: This function is far from being easy to read and elegant - try to find another approach */
    interrupt_descriptor_table::interrupt_descriptor_table( observer_ptr<descriptor_table<basic_flat_model>> gdt ) noexcept {
        /* 0 [FAULT] #DE - Divide by zero exception */
        this->add( interrupt_vector::DIVIDE_BY_ZERO, interrupt_gate_32bit_descriptor{ addressof( divide_by_zero ), gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 1 [FAULT/TRAP] #DB - Debug exception */
        this->add( interrupt_vector::DEBUG, interrupt_gate_32bit_descriptor{ addressof( debug ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 2 [INTERRUPT] Non-maskable interrupt */
        this->add( interrupt_vector::NON_MASKABLE_INTERRUPT, interrupt_gate_32bit_descriptor{ addressof( non_maskable_interrupt ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 3 [TRAP] #BP - Breakpoint */
        this->add( interrupt_vector::BREAKPOINT, interrupt_gate_32bit_descriptor{ addressof( breakpoint ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 4 [TRAP] #OF - Overflow */
        this->add( interrupt_vector::OVERFLOW, interrupt_gate_32bit_descriptor{ addressof( overflow ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 5 [FAULT] #BR - Bound range exceeded exception */
        this->add( interrupt_vector::BOUND_RANGE_EXCEEDED, interrupt_gate_32bit_descriptor{ addressof( bound_range_exceeded ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 6 [FAULT] #UD - Invalid opcode exception */
        this->add( interrupt_vector::INVALID_OPCODE, interrupt_gate_32bit_descriptor{ addressof( invalid_opcode ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 7 [FAULT] #NM - Device not available */
        this->add( interrupt_vector::DEVICE_NOT_AVALABLE, interrupt_gate_32bit_descriptor{ addressof( device_not_available ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 8 [ABORT] #DF - Double fault */
        this->add( interrupt_vector::DOUBLE_FAULT, interrupt_gate_32bit_descriptor{ addressof( bound_range_exceeded ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 10 [FAULT] #TS - Invalid TSS */
        this->add( interrupt_vector::INVALID_TSS, interrupt_gate_32bit_descriptor{ addressof( invalid_tss ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 11 [FAULT] #NP - Segment not present */
        this->add( interrupt_vector::SEGMENT_NOT_PRESENT, interrupt_gate_32bit_descriptor{ addressof( bound_range_exceeded ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 12 [FAULT] #SS - Stack segment fault */
        this->add( interrupt_vector::STACK_SEGMENT_FAULT, interrupt_gate_32bit_descriptor{ addressof( stack_segment_fault ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 13 [FAULT] #GP - General protection fault */
        this->add( interrupt_vector::GENERAL_PROTECTION_FAULT, interrupt_gate_32bit_descriptor{ addressof( general_protection_fault ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 14 [FAULT] #PF - Page Fault */
        this->add( interrupt_vector::PAGE_FAULT, interrupt_gate_32bit_descriptor{ addressof( page_fault ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 15 - Reserved */
        /* 16 [FAULT] #MF - x87 floating point exception */
        this->add( interrupt_vector::X87_FLOATING_POINT_EXCEPTION, interrupt_gate_32bit_descriptor{ addressof( x87_floating_point_exception ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 17 [FAULT] #AC - Alignment check */
        this->add( interrupt_vector::ALIGNMENT_CHECK, interrupt_gate_32bit_descriptor{ addressof( alignment_check ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 18 [ABORT] #MC - Machine check */
        this->add( interrupt_vector::MACHINE_CHECK, interrupt_gate_32bit_descriptor{ addressof( machine_check ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 19 [FAULT] #XM/#XF - SIMD floating point exception */
        this->add( interrupt_vector::SIMD_FLOATING_POINT_EXCEPTION, interrupt_gate_32bit_descriptor{ addressof( simd_flaoting_point_exception ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 20 [FAULT] #VE - Virtualization exception */
        this->add( interrupt_vector::VIRTUALIZATION_EXCEPTION, interrupt_gate_32bit_descriptor{ addressof( virtualization_exception ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 21 - 29 - Reserved */
        /* 30 [-] #SX - Security exception */
        this->add( interrupt_vector::SECURITY_EXCEPTION, interrupt_gate_32bit_descriptor{ addressof( security_exception ),  gdt->get_code_segment_selector( privilege_level::RING_0 ), true, privilege_level::RING_0 } );
        /* 31 - Reserved */
    }

}