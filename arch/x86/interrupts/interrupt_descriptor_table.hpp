#pragma once

#include "interrupt_vector.hpp"
#include "array.hpp"
#include "gate_descriptor.hpp"
#include "memory.hpp"

namespace ReVolta::Kernel {

    static constexpr size_t idt_size { 256 };

    /* See: https://wiki.osdev.org/IDT_problems#Problems_with_IDTs */

    struct basic_flat_model;
    template<typename> struct descriptor_table; 

    class alignas( sizeof( gate_descriptor ) ) interrupt_descriptor_table : public array<gate_descriptor, idt_size> {
    public:
        /* FIXME: Check the descriptor type - trap for trap, iterrupt for interrupt etc. */
        /* FIXME: 'constexpr' is implicitly 'inline' thus the function body must be defined in the header */
        /*constexpr*/ interrupt_descriptor_table( observer_ptr<descriptor_table<basic_flat_model>> gdt ) noexcept;

    private:
        template<typename T>
        constexpr inline void add( interrupt_vector iv, const T & descriptor ) noexcept {
            this->at( to_underlying( iv ) ) = static_cast<const gate_descriptor &>( descriptor );
        }
    };

}