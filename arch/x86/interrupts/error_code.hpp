#pragma once

#include "interrupt_vector.hpp"
#include "cstddef.hpp"

namespace ReVolta::Kernel {

    using raw_error_code = size_t;

    template<interrupt_vector E>
    struct error_code_type {
        error_code_type( size_t v ) noexcept : value( v ) {}
        size_t value;
    };

    template<>
    struct error_code_type<interrupt_vector::PAGE_FAULT> {
        constexpr error_code_type( raw_error_code raw ) noexcept : raw( raw ) {}

        inline bool is_page_protection_violation( void ) noexcept { return ( bits.p == 1 ); }

        inline bool is_non_present_page( void ) noexcept { return ( bits.p == 0 ); }

        inline bool is_write_access( void ) noexcept { return ( bits.w == 1 ); }

        inline bool is_read_access( void ) noexcept { return ( bits.w == 0 ); }

        inline bool is_userspace( void ) noexcept { return ( bits.u == 1 ); }

        inline bool is_reserved_write( void ) noexcept { return ( bits.r == 1 ); }

        inline bool is_instruction_fetch( void ) noexcept { return ( bits.i == 1 ); }

        inline bool is_protection_key_violation( void ) noexcept { return ( bits.pk == 1 ); }

        inline bool is_shadow_stack_access( void ) noexcept { return ( bits.ss == 1 ); }

        inline bool is_software_guard_extension( void ) noexcept { return ( bits.sgx == 1 ); }

    private:
        union {
            struct {
                uint32_t p   : 1;    /* [0]  Present - SET: A page protection violation. When unset, it was caused by non-present page */
                uint32_t w   : 1;    /* [1]  Write - SET: A write access. When unset, it was caused by read access */
                uint32_t u   : 1;    /* [2]  User - SET: the page fault was caused while CPL=3. Does not necessarily mean the page fault was a privilege violation */
                uint32_t r   : 1;    /* [3]  Reserved Write - SET: one or more page directory entries contains reserved bits set to 1 */
                uint32_t i   : 1;    /* [4]  Instruction Fetch - SET: instruction fetch. Only applies when No-Execute bit supported and enabled */
                uint32_t pk  : 1;    /* [5]  Protection Key -SET: Protection key violation  */
                uint32_t ss  : 1;    /* [6]  Shadow Stack - SET: Page fault caused by shadow stack access */
                uint32_t     : 8;
                uint32_t sgx : 1;    /* [15] Software Guard Extensions - SET: SGX violation. Unrelated to ordinary paging */
                uint32_t     : 16;
            } bits;
            raw_error_code raw;
        };

    } __attribute__(( packed ));

    /* Check the size of the error code type */
    static_assert( sizeof( error_code_type<interrupt_vector::PAGE_FAULT> ) == sizeof( raw_error_code ) );

}