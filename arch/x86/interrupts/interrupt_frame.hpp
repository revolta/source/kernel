#pragma once

#include "registers.hpp"
#include "segment_selector.hpp"

namespace ReVolta::Kernel {

    struct interrupt_frame {
        /* EIP */
        void * eip;
        /* Code segment selector (CS) */
        alignas( 4 ) segment_selector cs;
        /* EFLAGS register */
        uint32_t eflags;

    }__attribute__((packed));

    static_assert( sizeof( interrupt_frame ) == 12 );

}