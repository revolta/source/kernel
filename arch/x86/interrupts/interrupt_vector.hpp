#pragma once

#include "cstdint.hpp"

namespace ReVolta::Kernel {

    enum class interrupt_vector : uint8_t {
        DIVIDE_BY_ZERO                  = 0,
        DEBUG                           = 1,
        NON_MASKABLE_INTERRUPT          = 2,
        BREAKPOINT                      = 3,
        OVERFLOW                        = 4,
        BOUND_RANGE_EXCEEDED            = 5,
        INVALID_OPCODE                  = 6,
        DEVICE_NOT_AVALABLE             = 7,
        DOUBLE_FAULT                    = 8,
        COPROCESSOR_SEGMENT_OVERRUN     = 9,
        INVALID_TSS                     = 10,
        SEGMENT_NOT_PRESENT             = 11,
        STACK_SEGMENT_FAULT             = 12,
        GENERAL_PROTECTION_FAULT        = 13,
        PAGE_FAULT                      = 14,
        X87_FLOATING_POINT_EXCEPTION    = 16,
        ALIGNMENT_CHECK                 = 17,
        MACHINE_CHECK                   = 18,
        SIMD_FLOATING_POINT_EXCEPTION   = 19,
        VIRTUALIZATION_EXCEPTION        = 20,
        SECURITY_EXCEPTION              = 30,
        /* TRIPLE_FAULT */
    };

}