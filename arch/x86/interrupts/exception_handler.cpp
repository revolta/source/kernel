
/* See: https://wiki.osdev.org/Exceptions */

#include "interrupt_frame.hpp"
#include "error_code.hpp"

namespace ReVolta::Kernel {

    void direct_output( const char * output ) noexcept {
        struct character {
            char m_char : 8;
            uint8_t m_foreground : 4 = { 15 };  /* White */
            uint8_t m_background : 4 = { 4 };   /* Red */
        };

        character * const framebuffer { reinterpret_cast<character *>( ( cr0{}.pg == ENABLED ) ? 0xC00B8000 : 0xB8000 ) };

        size_t i { 0 };
        while( output[i] ) {
            framebuffer[ i ] = {.m_char = output[ i++ ], .m_foreground = 15, .m_background = 4 };
        }
    }

    /* The Divide-by-zero Error occurs when dividing any number by 0 using the DIV or IDIV instruction,
     * or when the division result is too large to be represented in the destination. */
    __attribute__((interrupt)) void divide_by_zero( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Division by zero #DE [FAULT] " );
    }

    __attribute__((interrupt)) void debug( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Debug #DB [FAULT/TRAP] " );
    }

    __attribute__((interrupt)) void non_maskable_interrupt( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Non maskable interrupt [INTERRUPT] " );
    }

    __attribute__((interrupt)) void breakpoint( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Breakpoint #BP [TRAP] " );
    }

    __attribute__((interrupt)) void overflow( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Overflow #OF [TRAP] " );
    }

    __attribute__((interrupt)) void bound_range_exceeded( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Bound range exceeded [FAULT] " );
    }

    __attribute__((interrupt)) void invalid_opcode( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Invalid opcode #UD [FAULT] " );
    }

    __attribute__((interrupt)) void device_not_available( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Device not available #NM [FAULT] " );
    }

    __attribute__((interrupt)) void double_fault( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " Double fault #DF [ABORT] " );
    }

    __attribute__((interrupt)) void invalid_tss( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " Invalid TSS #TS [FAULT] " );
    }

    __attribute__((interrupt)) void segment_not_present( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " Segment not present #NP [FAULT] " );
    }

    __attribute__((interrupt)) void stack_segment_fault( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " Stack segment fault #SS [FAULT] " );
    }

    __attribute__((interrupt)) void general_protection_fault( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " General protection fault #GP [FAULT] " );
    }

    inline void * get_page_fault_address( void ) noexcept {
        return reinterpret_cast<void *>( static_cast<uintptr_t>( cr2{}.pfla ) );
    }

    /* See: https://www.iaik.tugraz.at/teaching/materials/os/tutorials/pagefaults/ */
    /* TODO: When a page fault occurs, the hardware cannot do anything else with the instruction that caused the page fault and thus it must transfer control to an operating system routine (this is the page fault handler).
     * The page fault handler must then decide how to handle the page fault. It can do one of two things:
     * - It can decide the virtual address is just simply not valid.  In this case, Windows will report this error back by indicating an exception has occurred (typically STATUS_ACCESS_VIOLATION)
     * - It can decide the virtual address is valid. In this case, Windows will find an available physical page, place the correct data in that page, update the virtual-to-physical page translation mechanism 
     *   and then tell the hardware to retry the operation.  When the hardware retries the operation it will find the page translation and continue operations as if nothing had actually happened.
     * Once you have a virtual-to-physical page translation there's always the temptation to add features to it.  So for all the Windows platforms each page can support specific types of access:
     * - User mode access.  This access indicates if code running in user mode (that is the CPU operating mode with the least privilege, CPL 3  on the x86) can access the page.
     *   Code in kernel mode (that is the CPU operating mode with the most privilege, CPL 0 on the x86) can always access the page.
     * - Write access. This access indicates if code accessing the page is allowed to modify the contents of the virtual page.  This applies to all running code. */
    __attribute__((interrupt)) void page_fault( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code ec ) {
        error_code_type<interrupt_vector::PAGE_FAULT> error_code { ec };
        /* Attempt to dereference nullptr */
        if( ( get_page_fault_address() == nullptr ) /*&& ( error_code.is_non_present_page() == true ) */) {
            direct_output( " nullptr dereference - #PF [FAULT] -> SIGSEGV " );
            /* TODO: Send SIGSEGV signal to the running process */
        } else {
            direct_output( " Page Fault #PF [FAULT] " );
        }
    }
    
    __attribute__((interrupt)) void x87_floating_point_exception( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " x87 floating point exception #MF [FAULT] " );
    }

    __attribute__((interrupt)) void alignment_check( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " Alignment check #AC [FAULT] " );
    }

    __attribute__((interrupt)) void machine_check( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Machine check #MC [FAULT] " );
    }

    __attribute__((interrupt)) void simd_flaoting_point_exception( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " SIMD floating point exception #XM/#XF [FAULT] " );
    }

    __attribute__((interrupt)) void virtualization_exception( [[maybe_unused]] interrupt_frame * frame ) {
        direct_output( " Virtualization exception #VE [FAULT] " );
    }

    __attribute__((interrupt)) void security_exception( [[maybe_unused]] interrupt_frame * frame, [[maybe_unused]] raw_error_code error_code ) {
        direct_output( " Security exception #SX [-] " );
    }

}