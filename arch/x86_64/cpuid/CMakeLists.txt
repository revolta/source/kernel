set( TARGET_ARCH_CPUID "cpuid-x86_64" )

add_library( ${TARGET_ARCH_CPUID} STATIC )

target_include_directories( ${TARGET_ARCH_CPUID}
PUBLIC
	$<$<COMPILE_LANGUAGE:C,CXX>:${CMAKE_CURRENT_LIST_DIR}>
)

target_sources( ${TARGET_ARCH_CPUID}
PUBLIC
    cpuid.hpp
PRIVATE
    cpuid.cpp
)

target_link_libraries( ${TARGET_ARCH_CPUID}
PRIVATE
	kernel-build-config
)