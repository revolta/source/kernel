#include "cpuid.hpp"

#include "cpuid.h"

#include <utility.hpp>

namespace ReVolta::Kernel {

#if false
    bool cpuid::check_cpuid_support( void ) noexcept {
        uint32_t value, result;
        asm volatile (
            "pushf\n"
            "popq %[result]\n"
            "movq %[result], %[value]\n"
            "xorq %[eflags_id], %[value]\n"
            "pushq %[value]\n"
            "popf\n"
            "pushf\n"
            "popq %[value]\n"
            "andq %[eflags_id], %[result]\n"
            "andq %[eflags_id], %[value]\n"
            "xorq %[value], %[result]\n"
            : [result] "=r" (result), [value] "=r" (value)
            : [eflags_id] "i" ( (1 << 21) ) 
        );
        return result;
    }
#endif

    bool cpuid::supports( const ecx_feature feature ) noexcept {
        uint32_t eax { 0U }, ebx { 0U }, ecx { 0U }, edx { 0U };
#if false        
		if( cpuid::is_supported() ) {
#endif
			__cpuid( to_underlying( cpuid_leaf::FEATURE_INFORMATION ), eax, ebx, ecx, edx );
#if false
		}
#endif
        return ecx & to_underlying( feature );
    }

    bool cpuid::supports( const edx_feature feature ) noexcept {
        uint32_t eax { 0U }, ebx { 0U }, ecx { 0U }, edx { 0U };
#if false        
		if( cpuid::is_supported() ) {
#endif
			__cpuid( to_underlying( cpuid_leaf::FEATURE_INFORMATION ), eax, ebx, ecx, edx );
#if false
		}
#endif
        return edx & to_underlying( feature );
    }
    
    /* EAX=1: Processor Info and Feature Bits */
    /* TODO: Maybe this shall be somehow generic - to read various CPU info. To execute the __cpuid() just once and then provide 
     * particular information out of dedicated structure using the accessor functions */
    uint8_t cpuid::local_apic_id( void ) {
        uint32_t eax { 0U }, ebx { 0U }, ecx { 0U }, edx { 0U };
#if false        
		if( cpuid::is_supported() ) {
#endif
            __cpuid( to_underlying( cpuid_leaf::FEATURE_INFORMATION ), eax, ebx, ecx, edx );
#if false
		}
#endif
        return ebx >> 24;
    }

}