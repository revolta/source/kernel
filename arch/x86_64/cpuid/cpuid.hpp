#pragma once

#include <cstdint.hpp>

namespace ReVolta::Kernel {

    class cpuid {
    public:
        /* See AP-485 Intel Processor Identification and the CPUID instruction, chapter 5.1.2.4 - Table 5-5 and Table 5-6 */

        /* Feature flags reported in ECX register - Table 5-5 */
        enum class ecx_feature : uint32_t {
            SSE3	        = ( 1U << 0 ), 		/* Streaming SIMD Extension 3 */
            PCLMULDQ        = ( 1U << 1 ),		/* PCLMULDQ instruction supported */
            DTES64          = ( 1U << 2 ),		/* 64-bit Debug Store */
            MONITOR         = ( 1U << 3 ), 		/* MONITOR/MWAIT instruction */
            DS_CPL 	        = ( 1U << 4 ), 		/* CPL-qualified Debug Store */
            VMX 	        = ( 1U << 5 ), 		/* Virtual Machine Extensions */
            SMX 	        = ( 1U << 6 ), 		/* Safer Mode Extensions */
            EIST 	        = ( 1U << 7 ), 		/* Enhanced Intel SpeedStep Technology */
            TM2 	        = ( 1U << 8 ), 		/* Thermal Monitor 2 */
            SSSE3 	        = ( 1U << 9 ), 		/* Supplemental Streaming SIMD Extensions 3 */
            CNXT_ID         = ( 1U << 10 ), 	/* L1 Context ID */
            FMA 	        = ( 1U << 12 ), 	/* Fused Multiply Add */
            CX16 	        = ( 1U << 13 ), 	/* CMPXCHG16B */
            XTPR 	        = ( 1U << 14 ), 	/* xTPR Update Control */
            PDCM 	        = ( 1U << 15 ), 	/* Perfmon and Debug Capability */
            PCID 	        = ( 1U << 17 ), 	/* Process Context Identifiers */
            DCA 	        = ( 1U << 18 ), 	/* Direct Cache Access */
            SSE4_1 	        = ( 1U << 19 ), 	/* Streaming SIMD Extensions 4.1 */
            SSE4_2 	        = ( 1U << 20 ), 	/* Streaming SIMD Extensions 4.2 */
            X2APIC 	        = ( 1U << 21 ), 	/* Extended xAPIC Support */
            MOVBE 	        = ( 1U << 22 ), 	/* MOVBE Instruction */
            POPCNT 	        = ( 1U << 23 ), 	/* POPCNT Instruction */
            TSC_DEADLINE    = ( 1U << 24),      /* Time Stamp Counter Deadline */
            AES 	        = ( 1U << 25 ), 	/* AES Instruction Extensions */
            XSAVE 	        = ( 1U << 26 ), 	/* XSAVE/XSTOR States */
            OSXSAVE	        = ( 1U << 27 ), 	/* OS-Enabled Extended State Management */
            AVX 	        = ( 1U << 28 ), 	/* Advanced Vector Extensions */
            F16C 	        = ( 1U << 29 ), 	/* 16-bit Floating Point Conversion Instructions */
            RDRAND 	        = ( 1U << 30 ), 	/* RDRAND Instruction Supported */
            /* Unused - always returns 0 */
        };

        /* Feature flags reported in EDX register - Table 5-6 */
        enum class edx_feature : uint32_t {
            FPU   	        = ( 1U << 0 ), 		/* Floating-Point Unit on-chip */
            VME   	        = ( 1U << 1 ), 		/* Virtual Mode Extension */
            DE    	        = ( 1U << 2 ), 		/* Debugging Extension */
            PSE   	        = ( 1U << 3 ), 		/* Page Size Extension */
            TSC   	        = ( 1U << 4 ), 		/* Time Stamp Counter */
            MSR   	        = ( 1U << 5 ), 		/* Model Specific Registers */
            PAE   	        = ( 1U << 6 ), 		/* Physical Address Extension */
            MCE   	        = ( 1U << 7 ), 		/* Machine Check Exception */
            CX8   	        = ( 1U << 8 ), 		/* CMPXCHG8 Instruction */
            APIC  	        = ( 1U << 9 ), 		/* On-chip APIC hardware */
            SEP		        = ( 1U << 11 ), 	/* Fast System Call SYSENTER/SYSEXIT */
            MTRR  	        = ( 1U << 12 ), 	/* Memory type Range Registers */
            PGE   	        = ( 1U << 13 ), 	/* Page Global Enable */
            MCA   	        = ( 1U << 14 ), 	/* Machine Check Architecture */
            CMOV  	        = ( 1U << 15 ), 	/* Conditional MOVe Instruction */
            PAT   	        = ( 1U << 16 ), 	/* Page Attribute Table */
            PSE36 	        = ( 1U << 17 ), 	/* 36bit Page Size Extension */
            PSN   	        = ( 1U << 18 ), 	/* Processor Serial Number */
            CLFSH 	        = ( 1U << 19 ), 	/* CFLUSH Instruction */
            DS    	        = ( 1U << 21 ), 	/* Debug Store */
            ACPI  	        = ( 1U << 22 ), 	/* Thermal Monitor & Software Controlled Clock */
            MMX   	        = ( 1U << 23 ), 	/* MultiMedia eXtension */
            FXSR  	        = ( 1U << 24 ), 	/* Fast Floating Point Save & Restore */
            SSE   	        = ( 1U << 25 ), 	/* Streaming SIMD Extension 1 */
            SSE2  	        = ( 1U << 26 ), 	/* Streaming SIMD Extension 2 */
            SS    	        = ( 1U << 27 ), 	/* Self Snoop */
            HTT   	        = ( 1U << 28 ), 	/* Hyper Threading Technology */
            TM    	        = ( 1U << 29 ), 	/* Thermal Monitor */
            PBE   	        = ( 1U << 31 ) 		/* Pending Break Enabled */
        };

        enum class cpuid_leaf : uint32_t {
            LARGEST_STANDARD_FUNCTION	= 0x00000000,
            FEATURE_INFORMATION 		= 0x00000001,	    /* Chapter 5.1.2 */

            LARGEST_EXTENDED_FUNCTION	= 0x80000000,
            EXTENDED_FEATURE_BITS		= 0x80000001,
            PROCESSOR_BRAND_STRING_1	= 0x80000002,
            PROCESSOR_BRAND_STRING_2	= 0x80000003,
            PROCESSOR_BRAND_STRING_3	= 0x80000004,
#if false
            CACHE_DESCRIPTORS                 = 0x02,       /* Chapter 5.1.3 */
            PROCESSOR_SERIAL_NUMBER           = 0x03,       /* Chapter 5.1.4 */
            DETERMINISTIC_CACHE_PARAMETERS    = 0x04,       /* Chapter 5.1.5 */
            MONITOR_MWAIT_PARAMETERS          = 0x05,       /* Chapter 5.1.6 */
#endif
        };

    private:
#if false    
        static bool check_cpuid_support( void );
#endif

    public:
#if false    
        inline static bool is_supported( void ) noexcept {
            static const bool cpuid_supported { check_cpuid_support() };
			return cpuid_supported;
        }
#endif

        static bool supports( const ecx_feature feature ) noexcept;

        static bool supports( const edx_feature feature ) noexcept;

        static uint8_t local_apic_id( void );
    };  

}