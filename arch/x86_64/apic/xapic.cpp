#include "xapic.hpp"

#include "xapic_registers.hpp"

#include "cpuid.hpp"
#include "model_specific_registers/ia32_apic_base.hpp"

#include <chrono.hpp>


namespace ReVolta::Kernel {

    observer_ptr<xapic_registers> xapic_registers::get( void ) noexcept {
        static observer_ptr<xapic_registers> lapic { nullptr };
        if( lapic == nullptr ) {
            /* CPUID.01h:EDX[bit 9] defines whether APIC is supported by the CPU or not */
            if( cpuid::supports( cpuid::edx_feature::APIC ) ) {
                /* Once supported, read the APIC base address from the IA32_APIC_BASE MSR and store the observer pointer to APIC */
                lapic = make_observer<xapic_registers>( ia32_apic_base{}.apic_base << 12 );
            }
        }
        return lapic;
    }

    xapic::xapic( void ) noexcept {
        /* TODO: Prior enabling the xapic, specify the spurious interrupt, Task Priority register and SW enable the APIC */
    }

    void xapic::enable( void ) {
        ia32_apic_base{}.en = true;
    }

    void xapic::send( const interprocessor_interrupt & ipi ) noexcept {
        xapic_registers::get()->m_interrupt_command.send( ipi );
    }

}