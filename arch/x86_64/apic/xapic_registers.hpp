#pragma once

#include "apic_register.hpp"
#include "xapic_lvt.hpp"
#include "xapic_timer.hpp"

namespace ReVolta::Kernel {

    class local_apic_id_register : private apic_register {
        uint32_t            : 24;
        uint8_t m_apic_id   : 8;

    public:
        /* Read APIC ID */
        uint8_t apic_id( void ) const {
            return this->m_apic_id;
        }

        /* Write APIC ID */
        void apic_id( const uint8_t & value ) {
            this->m_apic_id = value;
        }

    } __attribute__(( packed ));

    class local_apic_version_register : private apic_register {
        uint8_t m_version                   : 8;
        uint8_t                             : 8;
        uint8_t m_max_lvt_entry             : 8;
        uint8_t m_suppress_eoi_broadcasts   : 1;
        uint8_t                             : 7;      

    public:
        local_apic_version_value version( void ) const {
            return ( this->m_version < 0x10 ) ? local_apic_version_value::DISCRETE_APIC_82489DX : local_apic_version_value::INTEGRATED_APIC;
        }

        /* Returns the number of LVT entries */
        uint8_t max_lvt_entry( void ) const {
            /* NOTE: In within the register, the max LVT value is the number of entries minus 1 */
            return this->m_max_lvt_entry + 1;
        }

        bool is_supported_eoi_broadcast_suppression( void ) const {
            return ( this->m_suppress_eoi_broadcasts == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    class task_priority_register : private apic_register {
        uint8_t m_task_priority_subclass        : 4;
        uint8_t m_task_priority_class           : 4;
        uint32_t                                : 24;

    public:
        /* TODO: Accessor functions */

    } __attribute__(( packed ));

    class processor_priority_register : private apic_register {
        uint8_t m_processor_priority_subclass   : 4;
        uint8_t m_processor_priority_class      : 4;
        uint32_t                                : 24;

    public:
        /* TODO: Accessor functions */

    } __attribute__(( packed ));

    class arbitration_priority_register : private apic_register {
        uint8_t m_arbitration_priority_subclass : 4;
        uint8_t m_arbitration_priority_class    : 4;
        uint32_t                                : 24;

    public:
        /* TODO: Accessor functions */

    } __attribute__(( packed ));

    /* Signaling interrupt servicing completion - End Of Interrupt  */
    class eoi_register : private apic_register {
        uint32_t m_value;

    public:
        /* For all interrupts except those delivered with the NMI, SMI, INIT, ExtINT, the start-up or INIT-Deassert delivery mode
        * the interrupt handler must includea write to EOI register at the end of the handler routine before the IRET instruction.
        * This action indicates that the servicing of the current interrupt is complete and APIC can issue the next interrupt from ISR */
        void end_of_interrupt( const uint32_t value = 1 ) {
            this->m_value = value;
        }

    } __attribute__(( packed ));

    class remote_read_register : private apic_register {
    private:
        /* TODO */

    public:
        /* TODO: Accessor functions */

    } __attribute__(( packed ));

    class logical_destination_register : private apic_register {
        uint32_t                    : 24;
        uint8_t m_logical_apic_id   : 8;

    public:
        /* Read logical APIC ID */
        uint8_t logical_apic_id( void ) const {
            return this->m_logical_apic_id;
        }

        /* Write logical APIC ID */
        void logical_apic_id( const uint8_t & value ) {
            this->m_logical_apic_id = value;
        }

    } __attribute__(( packed ));

    class destination_format_register : private apic_register {
        uint32_t                    : 28;
        uint8_t m_model             : 4;

    public:
        destination_format_value model( void ) const {
            return static_cast<destination_format_value>( this->m_model );
        }

        void model( const destination_format_value & value ) {
            this->m_model = to_underlying( value );
        }

    } __attribute__(( packed ));

    class spurious_interrupt_vector_register : private apic_register {
        uint8_t m_vector                    : 8;
        uint8_t m_apic_enabled              : 1;
        uint8_t m_focus_processor_checking  : 1;
        uint8_t                             : 2;
        uint8_t m_eoi_broadcast_suppression : 1;
        uint32_t                            : 19;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }

        /* For Pentium 4 and Intel Xeon processors the bits 0 through 7 of this field are SW programmable.
        * For P6 family and Pentium processors the bits 4 through 7 are SW programmable. 
        * Bits 0 through 3 are hardwired to logical ones. Writing to those has no effect. */
        void vector( const uint8_t & value ) {
            this->m_vector = value;
        }

        /* Allows SW to temporarily enable the local APIC */
        void enable_apic( void ) {
            this->m_apic_enabled = 1;
        }

        /* Allows SW to temporarily disable the local APIC */
        void disable_apic( void ) {
            this->m_apic_enabled = 0;
        }

        bool is_apic_enabled( void ) {
            return ( this->m_apic_enabled == 1 ) ? true : false;
        }

        /* NOTE: Not supported in Pentium 4 and Intel Xeon processors */
        void enable_focus_processor_checking( void ) {
            /* NOTE: To enable the checking the bit shall be cleared */
            this->m_focus_processor_checking = 0;
        }

        /* NOTE: Not supported in Pentium 4 and Intel Xeon processors */
        void disable_focus_processor_checking( void ) {
            /* NOTE: To disable the checking the bit shall be set */
            this->m_focus_processor_checking = 1;
        }

        /* NOTE: Not supported in Pentium 4 and Intel Xeon processors */
        bool is_focus_processor_checking_enabled( void ) {
            return ( this->m_focus_processor_checking == 0 ) ? true : false;
        }

        /* Not supported on all processors. Check 'local_apic_version_register::is_supported_eoi_broadcast_suppression()' */
        void enable_eoi_broadcast_suppression( void ) {
            this->m_eoi_broadcast_suppression = 1;
        }

        /* Not supported on all processors. Check 'local_apic_version_register::is_supported_eoi_broadcast_suppression()' */
        void disable_eoi_broadcast_suppression( void ) {
            this->m_eoi_broadcast_suppression = 0;
        }

        /* Not supported on all processors. Check 'local_apic_version_register::is_supported_eoi_broadcast_suppression()' */
        bool is_eoi_broadcast_suppression_enabled( void ) {
            return ( this->m_eoi_broadcast_suppression == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    namespace Detail {

        /* APIC specific 256bit wide register accessible using 128bit aligned 32bit loads and stores */
        class apic_register_256bit {
            /* 128bit aligned 32bit register containing the relevant bits */
            class bit_bank : private apic_register {
                /* Just a hard coded constant defining how many bits are there in one byte */
                static constexpr size_t bits_per_byte { 8 };

            public:
                /* Internal storage type */
                using value_type = uint32_t;
                /* How many bits are there wihin one bank */
                static constexpr size_t bits_per_bank { bits_per_byte * sizeof( value_type ) };

            private:
                constexpr static size_t which_bit( size_t position ) noexcept {
                    return position % bit_bank::bits_per_bank;
                }

                /* The bits itself */
                value_type m_bits;

            public:
                constexpr bit_bank( void ) : m_bits( 0 ) {}

                constexpr bit_bank( value_type value ) : m_bits( value ) {}

                constexpr operator value_type( void ) const {
                    return this->m_bits;
                }

                /* Create bit mask setting selected maks bit to one leaving all others to zeros */
                constexpr static bit_bank mask_bit( size_t position ) noexcept {
                    return static_cast<bit_bank>( 1 ) << which_bit( position );
                }

                constexpr static size_t which_bank( size_t position ) noexcept {
                    return position / bit_bank::bits_per_bank;
                }

            } __attribute__(( packed ));
        
            constexpr static size_t bitwidth { 256 };

            constexpr static size_t n_banks { bitwidth / bit_bank::bits_per_bank };

            bit_bank m_bank [ n_banks ];

            const bit_bank & get_bank( size_t position ) const {
                return this->m_bank[ bit_bank::which_bank( position ) ];
            }

        public:
            /* Read only access to 'position' selected bit. Only bits 16 through 255 are evaluated as the ones in range 0 through 15 are reserved. */
            bool operator[]( size_t position ) const {
                if( ( position > 15 ) && ( position < bitwidth ) ) {
                    return ( ( this->get_bank( position ) & bit_bank::mask_bit( position ) ) != static_cast<bit_bank>( 0 ) );
                }
                return false;
            }
        } __attribute__(( packed ));

    }

    class in_service_register : public Detail::apic_register_256bit {} __attribute__(( packed ));

    class trigger_mode_register  : public Detail::apic_register_256bit {} __attribute__(( packed ));

    /* The interrupt request (IRR) register contains the active interrupt requests that have been accepted but not yet
    * dispatched to the processor for servicing. When the local APIC accepts an interrupt, it sets teh bit here in the IRR
    * that corresponds the vector of the accepted interrupt.
    * When the processor core is ready to handle the next interrupt, the local APIC clears the highest priority IRR bit that is set
    * and sets the corresponding ISR bit. */
    class interrupt_request_register : public Detail::apic_register_256bit {} __attribute__(( packed ));

    class error_status_register : private apic_register {
        uint8_t m_send_checksum_error       : 1;
        uint8_t m_receive_checksum_error    : 1;
        uint8_t m_send_accept_error         : 1;
        uint8_t m_receive_accept_error      : 1;
        uint8_t m_redirectable_ipi          : 1;
        uint8_t m_send_illegal_vector       : 1;
        uint8_t m_received_illegal_vector   : 1;
        uint8_t m_illegal_register_address  : 1;
        uint32_t                            : 24;

    public:
        /* TODO: Accessor functions */

    } __attribute__(( packed ));

    /* The interrupt command register (ICR) is a 64-bit register that allows the SW running on the processor to specify
     * and send interprocessor interrupts (IPIs) to other processors in the system. */
    class interrupt_command_register : private apic_register {

        alignas( 16 ) Detail::interrupt_command_register_low    m_icr_low;
        alignas( 16 ) Detail::interrupt_command_register_high   m_icr_high;

    public:
        void send( const interprocessor_interrupt & ipi ) noexcept {
            this->m_icr_high = ipi.high();
            /* By writing to low, the IPI is sent out to the bus */
            this->m_icr_low = ipi.low();
        }

        uint8_t vector( void ) const {
            return this->m_icr_low.m_bits.m_vector;
        }

        /* TODO: Delivery mode */

        destination_mode_value destination_mode( void ) const {
            return static_cast<destination_mode_value>( this->m_icr_low.m_bits.m_destination_mode );
        }

        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_icr_low.m_bits.m_delivery_status );
        }

        level_value level( void ) const {
            return static_cast<level_value>( this->m_icr_low.m_bits.m_level );
        }

        trigger_mode_value trigger_mode( void ) const {
            return static_cast<trigger_mode_value>( this->m_icr_low.m_bits.m_trigger_mode );
        }

        destination_shorthand_value destination_shorthand( void ) const {
            return static_cast<destination_shorthand_value>( this->m_icr_low.m_bits.m_destination_shorthand );
        }

        uint8_t destination_field( void ) const {
            return this->m_icr_high.m_bits.m_destination_field;
        }

    } __attribute__(( packed ));

    static_assert( sizeof( interrupt_command_register ) == 32 );

    /* The structure being mapped in processor physical address space at given location */
    /* SINGLETON design pattern applied as only a single set of registers is allowed */
    struct alignas( 4_KB ) xapic_registers {
        apic_register                                       reserved_01 [ 2 ];                          /* 0xFEE00000 - 0xFEE0001F is reserved */
        local_apic_id_register                              m_apic_id;              
        local_apic_version_register                         m_apic_version;             
        apic_register                                       reserved_02 [ 4 ];                          /* 0xFEE00040 - 0xFEE0007F is reserved */
        task_priority_register                              m_task_priority;                            /* TPR */
        arbitration_priority_register                       m_arbitration_priority;                     /* APR */
        processor_priority_register                         m_processor_priority;                       /* PPR */
        eoi_register                                        m_eoi;                                      /* EOI */
        remote_read_register                                m_remote_read;                              /* RRD */
        logical_destination_register                        m_logical_destination;              
        destination_format_register                         m_destination_format;               
        spurious_interrupt_vector_register                  m_spurious_interrupt_vector;                
        in_service_register                                 m_in_service;                               /* ISR */
        trigger_mode_register                               m_trigger_mode;                             /* TMR */
        interrupt_request_register                          m_interrupt_request;                        /* IRR */
        error_status_register                               m_error_status;             
        apic_register                                       reserved_03 [ 6 ];                          /* 0xFEE00290 - 0xFEE002EF is reserved */
        lvt_corrected_machine_check_interrupt_register      m_lvt_corrected_machine_check_interrupt;    /* CMCI */
        interrupt_command_register                          m_interrupt_command;                        /* ICR */
        lvt_timer_register                                  m_lvt_timer;
        lvt_thermal_sensor_register                         m_lvt_thermal_sensor;
        lvt_performance_monitoring_counters_register        m_performance_monitoring_counters;
        lvt_lint0_register                                  m_lvt_lint0;
        lvt_lint1_register                                  m_lvt_lint1;
        lvt_error_register                                  m_lvt_error;
        initial_count_register                              m_initial_count;
        current_count_register                              m_current_count;
        apic_register                                       reserved_04 [ 4 ];
        divide_configuration_register                       m_divide_configuration;
        apic_register                                       reserved_05;

    private:
        /* Make default constructor private in order to deny xapic_registers construction */
        xapic_registers( void ) noexcept {}

    public:
        /* Non copyable */
        xapic_registers( const xapic_registers & ) = delete;

        /* Non copy assignable */
        xapic_registers & operator=( const xapic_registers & ) = delete;

        xapic_registers( xapic_registers && ) = delete;

        /* Non moveable */
        xapic_registers & operator=( xapic_registers && ) = delete;

        static observer_ptr<xapic_registers> get( void ) noexcept;

    } __attribute__(( packed ));

}