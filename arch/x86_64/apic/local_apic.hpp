#pragma once

#include "interprocessor_interrupt.hpp"
#include "xapic.hpp"

namespace ReVolta::Kernel {

    namespace Interface {

        template<typename T>
        concept LocalAPIC = requires ( T lapic, interprocessor_interrupt ipi ) {
            /* TODO: Define the return type as well */
            lapic.enable();

            lapic.send( ipi );

        };

        template<typename T>
        concept LocalAPICTimer = requires ( T timer ) {
            timer.stop();
        };

    }

    template<Interface::LocalAPIC LAPIC>
    class local_apic : public LAPIC {};

    template<Interface::LocalAPICTimer LAPIC_TIMER>
    class local_apic_timer : public LAPIC_TIMER {};

}