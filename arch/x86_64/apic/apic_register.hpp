#pragma once

#include <cstdint.hpp>
#include <utility.hpp>
#include <memory.hpp>

namespace ReVolta::Kernel {

    enum class local_apic_version_value : uint8_t {
        DISCRETE_APIC_82489DX,
        INTEGRATED_APIC
    };

    enum class delivery_status_value : uint8_t {
        IDLE            = 0,    /* There is currently no activity for this interrupt source or the previous interrupt from this source was delivered to the processor core and accepted */
        SEND_PENDING    = 1     /* Indicates that an interrupt from this source has been delivered to the processor core but has noe yet been accepted */
    };

    enum class timer_mode_value : uint8_t {
        ONE_SHOT        = 0,    /* Program count down value is an intial-count register */
        PERIODIC        = 1,    /* Program interval value in an initial-count register */
        TSC_DEADLINE    = 2     /* Program target value in IA32_TSC_DEADLINE MSR */
    };

    enum class interrupt_input_pin_polarity_value : uint8_t {
        ACTIVE_HIGH     = 0,    /*  */
        ACTIVE_LOW      = 1     /*  */
    };

    enum class trigger_mode_value : uint8_t {
        EDGE            = 0,
        LEVEL           = 1
    };

    enum class destination_mode_value : uint8_t {
        PHYSICAL        = 0,
        LOGICAL         = 1
    };

    enum class level_value : uint8_t {
        DE_ASSERT       = 0,
        ASSERT          = 1
    };

    enum class destination_shorthand_value : uint8_t {
        NO_SHORTHAND        = 0,
        SELF                = 1,
        ALL_INCLUDING_SELF  = 2,
        ALL_EXCLUDING_SELF  = 3
    };

    enum class destination_format_value : uint8_t {
        CLUSTER_MODEL       = 0b0000,
        FLAT_MODEL          = 0b1111
    };

    namespace Detail {

        union interrupt_command_register_low {
            struct {
                uint8_t     m_vector                    : 8;
                uint8_t     m_delivery_mode             : 3;
                uint8_t     m_destination_mode          : 1;
                uint8_t     m_delivery_status           : 1;
                uint8_t                                 : 1;
                uint8_t     m_level                     : 1;
                uint8_t     m_trigger_mode              : 1;
                uint8_t                                 : 2;
                uint8_t     m_destination_shorthand     : 2;
                uint16_t                                : 12;
            }           m_bits;
            uint32_t    m_raw;
        } __attribute__(( packed ));

        union interrupt_command_register_high {
            struct {
                uint32_t                                : 24;
                uint8_t     m_destination_field         : 8;
            }           m_bits;
            uint32_t    m_raw;
        } __attribute__(( packed ));

    }

    /* All the APIC registers are aligned to 128bit boundary */
    class alignas( 16 ) apic_register {};

}