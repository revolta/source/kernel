#include "xapic.hpp"

#include "xapic_registers.hpp"

namespace ReVolta::Kernel {

    /* TODO: Start in mode... */
    void xapic_timer::start( uint32_t initial_count ) noexcept {
        xapic_registers::get()->m_initial_count = initial_count;
    }

    void xapic_timer::stop( void ) noexcept {
        /* A write of '0' into initial count register effectively stops the timer
         * in both one-shot and periodic mode */
        xapic_registers::get()->m_initial_count = 0;
    }

}