#pragma once

#include "interprocessor_interrupt.hpp"

namespace ReVolta::Kernel {
    
    /* Local APIC in xAPIC mode public interface */
    class xapic {
    public:
        xapic( void ) noexcept;

        void enable( void ) noexcept;

        /* Send an interprocessor interrupt (IPI) */
        void send( const interprocessor_interrupt & ipi ) noexcept;

    };

    /* TODO: See https://wiki.osdev.org/APIC_timer */

    /* Local APIC Timer in xAPIC mode public interface */
    class xapic_timer {
    public:
        /* TODO: Set the local APIC timer's divide configuration register */
        /* TODO: Configure the local APIC timer's interrupt vector and unmask the timer's IRQ */
        //xapic_timer( mode )

        /* TODO: Get rid of too abstract 'initial_count', use some sort of time and/or frequency instead.
         * See https://en.cppreference.com/w/cpp/chrono/duration :
         * 
         * template<class Rep, Period> class duration; in <chrono.hpp> once available */
        void start( uint32_t initial_count ) noexcept;

        void stop( void ) noexcept;

    };

    class xapic_local_vector_table {
    public:

    };

}