#pragma once

#include "apic_register.hpp"
#include "acpi.hpp"

#include <named_type.hpp>

namespace ReVolta::Kernel {

    inline constexpr apic_id_t apic_id_broadcast { 0xFF };

    namespace Detail {
        /* TODO: Potential relations might be expressed by inheritance for those IPI types */

        struct fixed_ipi_t { 
            constexpr explicit fixed_ipi_t( void ) = default; 
            constexpr static uint8_t value { 0b000 };
        };

        struct lowest_priority_ipi_t { 
            constexpr explicit lowest_priority_ipi_t( void ) = default;
            constexpr static uint8_t value { 0b001 };
        };

        struct SMI_ipi_t { 
            constexpr explicit SMI_ipi_t( void ) = default;
            constexpr static uint8_t value { 0b010 };
        };

        struct NMI_ipi_t {
            constexpr explicit NMI_ipi_t( void ) = default;
            constexpr static uint8_t value { 0b100 };
        };

        struct INIT_ipi_t {
            constexpr explicit INIT_ipi_t( void ) = default;
            constexpr static uint8_t value { 0b101 };
        };

        struct INIT_level_deassert_ipi_t {
            constexpr explicit INIT_level_deassert_ipi_t( void ) = default;
            constexpr static uint8_t value { 0b101 };
        };

        struct startup_ipi_t { 
            constexpr explicit startup_ipi_t( void ) = default;
            constexpr static uint8_t value { 0b110 };
        };

    }
    
    /* Fixed interrupt delivers the interrupt specified in the vector field to the target processor or processors */
    inline constexpr Detail::fixed_ipi_t fixed_ipi;

    /* Same as fixed interrupt, except that the interrupt is delivered to the processor executing at the lowest priority
     * among the set of processors specified in the destination field. 
     * NOTE: The ability for a processor to send a lowest priority IPI is model specific ans should be avoided by OS */
    inline constexpr Detail::lowest_priority_ipi_t lowest_priority_ipi;

    /* Delivers and SMI interrupt to the target processor or processors. The vector field must be programmed to 0x00 for future compatibility */
    inline constexpr Detail::SMI_ipi_t SMI_ipi;

    /* Delivers an NMI interrupt to the target processor or processors. the vector information is ignored */
    inline constexpr Detail::NMI_ipi_t NMI_ipi;

    /* Delivers an INIT request to the target processor or processors which causes them to perform an INIT.
     * The vector field must be programmed to 0x00 for future compatibility */
    inline constexpr Detail::INIT_ipi_t INIT_ipi;

    /* Sends a synchronization message to all the local APICs in the system to set their arbitration IDs to the values of their APIC IDs.
     * This IPI is sent to all processors, regardless of the value in the destination field or destination shorthand. */
    inline constexpr Detail::INIT_level_deassert_ipi_t INIT_level_deassert;

    /* Sends a special "start-up" IPI, called SIPI to the target processor or processors.
     * The vector typically points to a start up routine that is a part of BIOS bootstrap code. IPI sent with this delivery
     * mode are not automatically retried fi the source APIC is unable to deliver it.
     * NOTE: It is up to the SW to determine if the SIPI was not successfully delivered and to reissue the SIPI if neccessary. */
    inline constexpr Detail::startup_ipi_t startup_ipi;

    /* Message Destination Address (MDA) is an 8-bit number which is about to be entered into the ICR's destination field. Upon receiving an IPI message that was sent using
     * logical destination mode, a local APIC compares the MDA in the message with the values it its LDR (Logical APIC ID 8-bit data) and DFR (Model - Flat/Cluster) value)
     * to determine if it shoudl accept and handle the IPI or not. */
    using message_destination_address = named_type<uint8_t, struct message_destination_address_tag>;

    class interprocessor_interrupt {
        Detail::interrupt_command_register_low  m_low;
        Detail::interrupt_command_register_high m_high;

        template<typename T>
        constexpr interprocessor_interrupt( T, bus_relative_interrupt_t vector, destination_shorthand_value shorthand, destination_mode_value destination_mode ) {
            this->m_low.m_bits.m_delivery_mode = T::value;
            this->m_low.m_bits.m_vector = static_cast<uint8_t>( vector );
            this->m_low.m_bits.m_destination_shorthand = to_underlying( shorthand );
            this->m_low.m_bits.m_destination_mode = to_underlying( destination_mode );
            this->m_low.m_bits.m_level = to_underlying( level_value::ASSERT );
            this->m_low.m_bits.m_trigger_mode = to_underlying( trigger_mode_value::EDGE );
        }

    public:
        /* Applicable to FIXED, Start-Up IPIs delivery modes.
         * Shorthand defined, destination mode ignored (Physical/Logical). Destination field ignored because of defined shorthand */
        template<typename T>
            requires ( is_same_v<T, Detail::fixed_ipi_t> || is_same_v<T, Detail::startup_ipi_t> )
        constexpr interprocessor_interrupt( T type, bus_relative_interrupt_t vector, destination_shorthand_value shorthand )
              /* Once the shorthand is used, physical vs. logical  destination mode is ignored */
            : interprocessor_interrupt( type, vector, shorthand, destination_mode_value::PHYSICAL )
        {}

        /* Applicable to ALL delivery modes.
         * Physical mode, no shorthand */
        template<typename T>
        constexpr interprocessor_interrupt( T type, bus_relative_interrupt_t vector, apic_id_t apic_id )
            : interprocessor_interrupt( type, vector, destination_shorthand_value::NO_SHORTHAND, destination_mode_value::PHYSICAL )
        { 
            this->m_high.m_bits.m_destination_field = static_cast<uint8_t>( apic_id );
        }

        /* Aplicable to ALL delivery modes.
         * Logical mode, no shorthand */
        template<typename T>
        constexpr interprocessor_interrupt( T type, bus_relative_interrupt_t vector, message_destination_address mda )
            : interprocessor_interrupt( type, vector, destination_shorthand_value::NO_SHORTHAND, destination_mode_value::LOGICAL )
        {
            this->m_high.m_bits.m_destination_field = static_cast<uint8_t>( mda );
        }

        /* Applicable to SMI, NMI, INIT delivery modes.
         * In SMI, NMI and INIT IPI types, the vector field is either ignored or shall be set to 0x00 */
        template<typename T, typename... ARGUMENTS>
            requires ( is_same_v<T, Detail::SMI_ipi_t> || is_same_v<T, Detail::NMI_ipi_t> || is_same_v<T, Detail::INIT_ipi_t> )
        constexpr interprocessor_interrupt( T type, ARGUMENTS &&... arguments ) 
            : interprocessor_interrupt( type, bus_relative_interrupt_t{ 0x00 }, forward<ARGUMENTS>( arguments )... )
        {}

        /* Applicable to INIT level de-assert IPI type */
        constexpr interprocessor_interrupt( Detail::INIT_level_deassert_ipi_t type, bus_relative_interrupt_t vector, destination_mode_value destination_mode )
            : interprocessor_interrupt( type, vector, destination_shorthand_value::ALL_INCLUDING_SELF, destination_mode )
        {
            this->m_low.m_bits.m_level = to_underlying( level_value::DE_ASSERT );
            this->m_low.m_bits.m_trigger_mode = to_underlying( trigger_mode_value::LEVEL );
            this->m_high.m_bits.m_destination_field = 0x00;
        }

        /* Applicable to Lowest Priority delivery mode.
         * The ability for a processor to send a lowest priority IPI is model specific ans should be avoided by OS, thus such IPI constructor is deleted. */
        template<typename... ARGUMENTS>
        constexpr interprocessor_interrupt( Detail::lowest_priority_ipi_t, ARGUMENTS... ) = delete;

        const Detail::interrupt_command_register_low & low( void ) const noexcept {
            return this->m_low;
        }

        Detail::interrupt_command_register_low & low( void ) noexcept {
            return this->m_low;
        }

        const Detail::interrupt_command_register_high & high( void ) const noexcept {
            return this->m_high;
        }

        Detail::interrupt_command_register_high & high( void ) noexcept {
            return this->m_high;
        }

    };

}