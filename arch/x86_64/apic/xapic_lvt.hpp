#pragma once

#include "apic_register.hpp"

namespace ReVolta::Kernel {

    enum class lvt_delivery_mode_value : uint8_t {
        FIXED           = 0b000,    /* Delivers the interrupt specified in the vector field */
        SMI             = 0b010,    /* Delivers an SMI interrupt to the processor core through it's local SMI signal path */
        NMI             = 0b100,    /* Delivers an NMI interrupt to the processor. The vector information is ignored */
        ExtINT          = 0b111,    /* Causes the processor to respond to the interrupt as if the interrupt originated in an externallz connected controller (8259A compatible) */
        INIT            = 0b101     /* Delivers an INIT request to the processor core which causes the processor to perform INIT. */
    };

    /* Specifies interrupt delivery when the APIC timer signals an interrupt. See "APIC Timer" */
    class lvt_timer_register : private apic_register {
        uint8_t m_vector            : 8;
        uint8_t                     : 4;
        uint8_t m_delivery_status   : 1;
        uint8_t                     : 3;
        uint8_t m_mask              : 1;
        uint8_t m_timer_mode        : 2;
        uint16_t                    : 13;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }
        
        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

        timer_mode_value timer_mode( void ) const {
            return static_cast<timer_mode_value>( this->m_timer_mode );
        }

    } __attribute__(( packed ));

    /* CMCI register specifies interrupt delivery when an overflow condition of corrected machine check error count
     * reaching a threshold value occured in a machine chek bank supporting CMCI. See "CMCI Local APIC Interface" */
    class lvt_corrected_machine_check_interrupt_register : private apic_register {
        uint8_t m_vector            : 8;
        uint8_t m_delivery_mode     : 2;
        uint8_t                     : 1;
        uint8_t m_delivery_status   : 1;
        uint8_t                     : 3;
        uint8_t m_mask              : 1;
        uint16_t                    : 16;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }

        lvt_delivery_mode_value delivery_mode( void ) const {
            return static_cast<lvt_delivery_mode_value>( this->m_delivery_mode );
        }

        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    /* Specifies interrupt delivery when the thermal sensor generates an interrupt. See "Thermal Monitor". This LVT entry is implementation specific */
    class lvt_thermal_sensor_register : private apic_register {
        uint8_t m_vector            : 8;
        uint8_t m_delivery_mode     : 2;
        uint8_t                     : 1;
        uint8_t m_delivery_status   : 1;
        uint8_t                     : 3;
        uint8_t m_mask              : 1;
        uint16_t                    : 16;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }

        lvt_delivery_mode_value delivery_mode( void ) const {
            return static_cast<lvt_delivery_mode_value>( this->m_delivery_mode );
        }

        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    /* Specifies interrupt delivery when a performance counter generates an interrupt on overflow. See "Generating interrupt on Overflow" */
    class lvt_performance_monitoring_counters_register : private apic_register {
        uint8_t m_vector            : 8;
        uint8_t m_delivery_mode     : 2;
        uint8_t                     : 1;
        uint8_t m_delivery_status   : 1;
        uint8_t                     : 3;
        uint8_t m_mask              : 1;
        uint16_t                    : 16;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }

        lvt_delivery_mode_value delivery_mode( void ) const {
            return static_cast<lvt_delivery_mode_value>( this->m_delivery_mode );
        }

        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    /* Specifies interrupt delivery when an interrupt is signaled a the LINT0 pin */
    class lvt_lint0_register : private apic_register {
        uint8_t m_vector                        : 8;
        uint8_t m_delivery_mode                 : 2;
        uint8_t                                 : 1;
        uint8_t m_delivery_status               : 1;
        uint8_t m_interrupt_input_pin_polarity  : 1;
        uint8_t m_remote_irr                    : 1;
        uint8_t m_trigger_mode                  : 1;
        uint8_t m_mask                          : 1;
        uint16_t                                : 16;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }

        lvt_delivery_mode_value delivery_mode( void ) const {
            return static_cast<lvt_delivery_mode_value>( this->m_delivery_mode );
        }

        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        interrupt_input_pin_polarity_value interrupt_input_pin_polarity( void ) const {
            return static_cast<interrupt_input_pin_polarity_value>( this->m_interrupt_input_pin_polarity );
        }

        /* TODO: IRR Flag */

        trigger_mode_value trigger_mode( void ) const {
            return static_cast<trigger_mode_value>( this->m_trigger_mode );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    /* Specifies interrupt delivery when an interrupt is signaled a the LINT1 pin */
    class lvt_lint1_register : private apic_register {
        uint8_t m_vector                        : 8;
        uint8_t m_delivery_mode                 : 2;
        uint8_t                                 : 1;
        uint8_t m_delivery_status               : 1;
        uint8_t m_interrupt_input_pin_polarity  : 1;
        uint8_t m_remote_irr                    : 1;
        uint8_t m_trigger_mode                  : 1;
        uint8_t m_mask                          : 1;
        uint16_t                                : 16;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }

        lvt_delivery_mode_value delivery_mode( void ) const {
            return static_cast<lvt_delivery_mode_value>( this->m_delivery_mode );
        }

        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        interrupt_input_pin_polarity_value interrupt_input_pin_polarity( void ) const {
            return static_cast<interrupt_input_pin_polarity_value>( this->m_interrupt_input_pin_polarity );
        }

        /* TODO: IRR Flag */

        trigger_mode_value trigger_mode( void ) const {
            return static_cast<trigger_mode_value>( this->m_trigger_mode );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

    /* Specifies interrupt delivery when the APIC detects an internal error. See "Error Handling" */
    class lvt_error_register : private apic_register {
    private:
        uint8_t m_vector            : 8;
        uint8_t                     : 3;
        uint8_t m_delivery_status   : 1;
        uint8_t                     : 3;
        uint8_t m_mask              : 1;
        uint16_t                    : 16;

    public:
        uint8_t vector( void ) const {
            return this->m_vector;
        }
        
        delivery_status_value delivery_status( void ) const {
            return static_cast<delivery_status_value>( this->m_delivery_status );
        }

        bool is_masked( void ) const {
            return ( this->m_mask == 1 ) ? true : false;
        }

    } __attribute__(( packed ));

}