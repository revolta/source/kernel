#pragma once

#include "frame.hpp"

namespace ReVolta::Kernel {

	class raw_frame_allocator {
	public:
		using value_type = frame;
		using pointer = value_type *;
		using const_pointer = const value_type *;
		using size_type = size_t;
		using difference_type = ptrdiff_t;
		using propagate_on_container_move_assignment = true_type;
		using is_always_equal = true_type;

		[[nodiscard]] pointer allocate( [[maybe_unused]] size_type n, const void * hint ) const noexcept;

		void deallocate( pointer ptr, [[maybe_unused]] size_type n ) const noexcept;
	};

	/* TODO: Add the support for memory map - so that the allocator knows where the frames might be allocated,
	 * where not (protected area) etc. */ 

	template<typename T>
	class frame_allocator : public raw_frame_allocator {
	public:
		using value_type = T;
		using pointer = value_type *;
		using difference_type = raw_frame_allocator::difference_type;
		using propagate_on_container_move_assignment = raw_frame_allocator::propagate_on_container_move_assignment;
		using is_always_equal = raw_frame_allocator::is_always_equal;

		frame_allocator( void ) noexcept = default;

		[[nodiscard]] inline pointer allocate( size_type n, const void * hint = invalid_ptr ) const noexcept {
			return reinterpret_cast<pointer>( raw_frame_allocator::allocate( n, hint ) );
		}

		inline void deallocate( pointer ptr, size_type n ) const noexcept {
			raw_frame_allocator::deallocate( reinterpret_cast<frame *>( ptr ), n );
		}
	};

}