#pragma once

#include "control_registers/cr0.hpp"

namespace ReVolta::Kernel {

    inline bool is_paging_enabled( void ) noexcept {
        return ( cr0{}.pg == ENABLED );
    }

}