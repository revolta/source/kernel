#pragma once

#include "frame_allocator.hpp"

namespace ReVolta::Kernel {

    class page_directory;

    class alignas( page_size ) page {
    public:
        /* Page cannot be constructed */
        page( void ) noexcept = delete;

        bool is_mapped( void ) const noexcept;

        template<typename T>
        static bool is_mapped( observer_ptr<T> ptr ) noexcept {
            return make_observer<page>( ptr )->is_mapped();
        }

        static observer_ptr<page> map( observer_ptr<page_directory> page_directory, const observer_ptr<page> mapping, unique_ptr<frame> source, const Access writable, const Mode user_accessible ) noexcept;

        static observer_ptr<page> map( observer_ptr<page_directory> page_directory, const observer_ptr<sysconf> mapping, const Access writable, const Mode user_accessible ) noexcept;

        static observer_ptr<page> map( const observer_ptr<page> mapping, unique_ptr<frame> source, const Access writable, const Mode user_accessible ) noexcept;

        static inline observer_ptr<page> map( const observer_ptr<page> mapping, unique_ptr<frame, forward_deleter<frame>> source, const Access writable, const Mode user_accessible ) noexcept {
            return page::map( mapping, unique_ptr<frame>{ source.release() }, writable, user_accessible );
        }

        template<typename MAPPING, typename SOURCE>
        static inline observer_ptr<page> map( observer_ptr<page_directory> page_directory, const observer_ptr<MAPPING> mapping, unique_ptr<SOURCE> source, const Access writable, const Mode user_accessible ) noexcept {
            return page::map( page_directory, make_observer<page>( mapping ), unique_ptr<frame>{ reinterpret_cast<frame *>( source.release() ) }, writable, user_accessible );
        }

        template<typename MAPPING, typename SOURCE>
        static inline observer_ptr<page> map( const observer_ptr<MAPPING> mapping, unique_ptr<SOURCE> source, const Access writable, const Mode user_accessible ) noexcept {
            return page::map( make_observer<page>( mapping ), unique_ptr<frame>{ reinterpret_cast<frame *>( source.release() ) } , writable, user_accessible );
        }

        template<typename MAPPING, typename SOURCE>
        static inline observer_ptr<page> map( fixed_ptr<MAPPING> mapping, unique_ptr<SOURCE> source, const Access writable, const Mode user_accessible ) noexcept {
            return page::map( static_cast<observer_ptr<MAPPING>>( mapping ), unique_ptr<frame>{ reinterpret_cast<frame *>( source.release() ) }, writable, user_accessible );
        }

        static unique_ptr<frame> remap( const observer_ptr<page> mapping, unique_ptr<frame> source, const Access writable = Access::READ_WRITE, const Mode user_accessible = Mode::SUPERVISOR ) noexcept;

        /* This remap() overload is mainly used in frame_allocator where special type of frame unique pointers are used (as frame allocator is not yet complete) */
        static unique_ptr<frame, forward_deleter<frame>> remap( const observer_ptr<page> mapping, unique_ptr<frame, forward_deleter<frame>> source, const Access writable = Access::READ_WRITE, const Mode user_accessible = Mode::SUPERVISOR ) noexcept {
            return unique_ptr<frame, forward_deleter<frame>>{ remap( mapping, unique_ptr<frame>{ source.release() }, writable, user_accessible ).release() };
        }

        static unique_ptr<frame, forward_deleter<frame>> unmap( const observer_ptr<page> entry ) noexcept;

        template<typename T>
        static inline unique_ptr<frame, forward_deleter<frame>> unmap( const observer_ptr<T> entry ) noexcept {
            return unmap( observer_ptr<page>( reinterpret_cast<page *>( entry.get() ) ) );
        }

        template<typename T>
        static inline unique_ptr<frame, forward_deleter<frame>> unmap( const fixed_ptr<T> entry ) noexcept {
            return unmap( static_cast<observer_ptr<T>>( entry ) );
        }

        /* Convert the page to frame while no address translation is performed. */
        static observer_ptr<frame> as_frame( const observer_ptr<page> & page ) noexcept {
            return make_observer<frame>( reinterpret_cast<frame *>( page.get() ) );
        }
    
    private:
        /* Transition lookaside buffer (TLB) flushing removes the entry from TLB for defined entry - makes the updates of paging structures
         * applicable to the CPU. Mostly this routine shall be used in unmapping and remapping */
        static void flush_tlb( const observer_ptr<page> entry ) noexcept;

        /* Flush all current context's TLB entries */
        static void flush_tlb( void ) noexcept;

        template<typename T, typename U>
        friend T * physical_cast( const U * ptr ) noexcept;

        template<typename T>
        friend T * physical_cast( const T * ptr ) noexcept;

        static void * get_physical_address( const void * linear_address ) noexcept;
    };

    template<typename T>
    inline T * physical_cast( const T * ptr ) noexcept {
        return static_cast<T *>( page::get_physical_address( ptr ) );
    }

    template<typename T, typename U>
    inline T * physical_cast( const U * ptr ) noexcept {
        return reinterpret_cast<T *>( page::get_physical_address( ptr ) );
    }

    template<typename T, typename U>
    inline observer_ptr<T> physical_cast( const observer_ptr<U> ptr ) noexcept {
        return make_observer( physical_cast<T>( ptr.get() ) );
    }

    static_assert( sizeof( page ) == page_size );

}