#pragma once

#include "config.hpp"

namespace ReVolta::Kernel {

    /* TODO: Confirm values */
	enum class Access {
		READ_ONLY,
		READ_WRITE
	};

	enum class Mode {
		USER,
		SUPERVISOR
	};

}