#pragma once

#include "paging_definitions.hpp"
#include "memory.hpp"

#include <cstdint.hpp>
#include <type_traits.hpp>

namespace ReVolta::Kernel {

	class alignas( page_size ) frame {
		/* frame_allocator shall access the Frame metadata stored within */
		friend class raw_frame_allocator;
		/* head_frame shall construct the initial Frame */
		friend class head_frame;

	public:
		frame( void ) noexcept : m_next_frame( nullptr ) {}

		frame( unique_ptr<frame, forward_deleter<frame>> next ) noexcept : m_next_frame( move( next ) ) {}

		~frame( void ) noexcept {}

		bool is_leaf( void ) const noexcept {
			return ( m_next_frame == nullptr );
		}

		unique_ptr<frame, forward_deleter<frame>> get_next( void ) noexcept {
			return move( m_next_frame );
		}

		unique_ptr<frame, forward_deleter<frame>> m_next_frame { nullptr };
	};

	static_assert( sizeof( frame ) == page_size );

	template<typename> class frame_allocator;

}

namespace ReVolta {

	template<>
	struct default_allocator<Kernel::frame> {
		using type = Kernel::frame_allocator<Kernel::frame>;
	};

}