#pragma once

#include "cstdint.hpp"
#include "cpu_register.hpp"

namespace ReVolta::Kernel {

    class cr2 : public cpu_register<cr2, uint32_t> {
    public:
        [[no_unique_address]] cpu_register::reference<uintptr_t>   pfla;   /* Page fault linear address */

    private:
        friend class cpu_register<cr2, uint32_t>;

        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "movl %%cr2, %0\n" : "=r" ( raw ) );
            return raw;
        }
    };

}