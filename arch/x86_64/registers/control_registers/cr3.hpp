#pragma once

#include "cpu_register.hpp"
#include "paging.hpp"

#include <cstdint.hpp>

namespace ReVolta::Kernel {

    class cr3 : public cpu_register<cr3, uint64_t> {
    public:
#if false
        [[no_unique_address]] cpu_register::reference<tagged<observer_ptr<page_directory>, page_directory_pointer_tags>> pd;
#else
        [[no_unique_address]] cpu_register::reference<void *> pd;
#endif

        /* Read the content of CR3 and write it back in there - effectively it flushes the whole TLB */
        inline void reload( void ) noexcept {
            asm volatile ( 
                "mov %rax, %cr3\n" 
                "mov %cr3, %rax"
            );
        }

    private:
        friend class cpu_register<cr3, uint64_t>;

        inline static raw_type load( void ) noexcept {
            raw_type raw;
            asm volatile ( "mov %%cr3, %0\n" : "=r" ( raw ) );
            return raw;
        }

        inline static void store( raw_type raw ) noexcept {
            asm volatile ( "mov %[v], %%cr3\n" :: [v] "r" ( raw ) );
        }
    };

}