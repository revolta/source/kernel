#pragma once
#include "control_registers/cr0.hpp"
#include "control_registers/cr2.hpp"
#include "control_registers/cr3.hpp"
#include "control_registers/cr4.hpp"

#include "rflags/rflags.hpp"

/* FIXME */
#if false
#include "protected_mode_registers/gdtr.hpp"
#include "protected_mode_registers/idtr.hpp"
#endif

#include "model_specific_registers/ia32_apic_base.hpp"
