#pragma once

#include <cstddef.hpp>
#include <type_traits.hpp>

namespace ReVolta::Kernel {

    template<typename REGISTER, typename RAW, size_t ALIGNMENT = alignof( RAW )>
        requires is_alignment_v<ALIGNMENT>
    class alignas( ALIGNMENT ) cpu_register {
    public:
        using raw_type = RAW;

    private:
        using register_type = REGISTER;

        static constexpr size_t bits_per_byte { 8 };

        static constexpr size_t bitwidth { sizeof( raw_type ) * bits_per_byte };

    public:
        template<typename T, size_t POSITION = size_t{ 0 }, size_t SIZE = bitwidth>
        class reference {
        public:
            using value_type = T;

        private:
            constexpr static raw_type bit_mask( size_t position, size_t size ) noexcept {
                return ( ( ~static_cast<raw_type>( 0 ) ) >> ( ( sizeof( raw_type ) * bits_per_byte ) - size ) << position );
            }

            inline constexpr static size_t position( void ) noexcept {
                return POSITION;
            }

            inline constexpr static size_t size( void ) noexcept {
                return SIZE;
            }

        public:
            /* To store the value to the register */
            constexpr reference & operator=( value_type value ) noexcept {
                raw_type reg_content { register_type::load() };
                reg_content ^= ( -value ^ reg_content ) & bit_mask( reference::position(), reference::size() );
                register_type::store( reg_content );
                return *this;
            }

#if false
            constexpr reference & operator=( const reference & other ) noexcept {
                /* TODO: load value from register, modify by given value and store it back */
                return *this;
            }
#endif

            /* To read the value from the register */
            constexpr operator value_type( void ) const {
                return static_cast<value_type>( ( register_type::load() & bit_mask( reference::position(), reference::size() ) ) >> position() );
            }

            constexpr bool operator~( void ) const noexcept {
                return static_cast<bool>( *this ) ? false : true;
            }

#if false
            constexpr reference & flip( void ) noexcept {
                /* TODO */
                return *this;
            }
#endif
        };

        constexpr cpu_register( void ) noexcept = default;

        cpu_register( const raw_type & raw ) noexcept {
            register_type::store( raw );
        }

        ~cpu_register( void ) noexcept {}

        cpu_register & operator=( const cpu_register & other ) noexcept {};

        cpu_register & operator=( const raw_type & raw ) noexcept {
            register_type::store( raw );
        }

        operator raw_type( void ) const noexcept {
            return register_type::load();
        }

    };

}