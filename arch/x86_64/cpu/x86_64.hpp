#pragma once

#if false
#include "descriptor_table.hpp"
#include "interrupt_descriptor_table.hpp"
#endif

#include "local_apic.hpp"

#include <memory_pool.hpp>

namespace ReVolta::Kernel {

    /* TODO: CPU shall use the interface to particular LAPIC implementation (xAPIC, x2APIC) */

    class x86 {
    public:
        /* TODO: Public definitions if any */

    private:
        /* Local APIC */
        [[no_unique_address]] local_apic<xapic>             m_local_apic;
        /* Local APIC Timer */
        [[no_unique_address]] local_apic_timer<xapic_timer> m_local_apic_timer;

    protected:
        /* Default constructor made protected in order to deny standalone construction */
        x86( void );

    public:
        [[nodiscard]] bool is_bootstrap( void ) const noexcept;
    };

}

#if false
        /* FIXME: In the CPU the linear addresses are expected. So it must be mapped into virtual address space once paging is enabled.
            * Maybe the fix can be to move them to dedicated page aligned storage and share such pages between bootstrap and kernel */
        /* TODO: Make this 'constexpr' */
        descriptor_table<basic_flat_model> m_gdt;

        /* FIXME: In the CPU the linear addresses are expected. So it must be mapped into virtual address space once paging is enabled.
            * Maybe the fix can be to move them to dedicated page aligned storage and share such pages between bootstrap and kernel */
        /* TODO: Make this 'constexpr' */
        interrupt_descriptor_table m_idt { make_observer( addressof( m_gdt ) ) };
#endif
