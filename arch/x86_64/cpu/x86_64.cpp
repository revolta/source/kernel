#include "x86_64.hpp"

#include "registers.hpp"
#include "cpuid.hpp"

#include "local_apic.hpp"

#include <cstring.hpp>


namespace ReVolta::Kernel {

#if false
    extern "C" const void * ap_trampoline;
#endif

    x86::x86( void ) {
        if( this->is_bootstrap() ) {
            /* TODO: See: https://wiki.osdev.org/Symmetric_Multiprocessing#BSP_Initialization_Code */
            /* FIXME: Seems not to work, returns 0 */
            uint8_t local_apic_id = cpuid::local_apic_id();

            /* Copy the AP trampoline code to a fixed address in low conventional memory */
#if false            
            memcpy( ( void * )0x8000, &ap_trampoline, 4096 );
#endif

            /* See: https://github.com/maxdev1/ghost/blob/master/kernel/src/kernel/system/system.cpp , 'systemInitializeBsp()' */

            /* TODO: Check CPUID support */

            /* TODO: Enable SSE */

            /* TODO: Check APIC availability */

            /* TODO: GDT */

            /* TODO: IDT specific for BSP */

        } else {
            /* See: https://github.com/maxdev1/ghost/blob/master/kernel/src/kernel/system/system.cpp , 'systemInitializeAp()' */

            /* TODO: Enable SSE */

            /* TODO: GDT */

            /* TODO: IDT specific for AP */
        }
    }

    bool x86::is_bootstrap( void ) const noexcept {
        return ia32_apic_base{}.bsp;
    }

}