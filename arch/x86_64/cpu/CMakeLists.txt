set( TARGET_ARCH_CPU "cpu-x86_64" )

add_library( ${TARGET_ARCH_CPU} STATIC )

target_include_directories( ${TARGET_ARCH_CPU}
PUBLIC
	$<$<COMPILE_LANGUAGE:C,CXX>:${CMAKE_CURRENT_LIST_DIR}>
)

target_sources( ${TARGET_ARCH_CPU}
PUBLIC
	x86_64.hpp
PRIVATE
	x86_64.cpp
)

target_link_libraries( ${TARGET_ARCH_CPU}
PUBLIC
	apic-x86_64
#	interrupts-x86_64
	memory-x86_64						# links also with memory-x86_64 (aka paging and segmentation)
	config
PRIVATE
	cpuid-x86_64
	kernel-build-config
)