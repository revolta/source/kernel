#pragma once

#include "stream.hpp"
#include "bits/move.hpp"
#include "framebuffer.hpp" /* FIXME: Just because the definition of ForegroundColor and BackgroundColor */

namespace ReVolta::Kernel {

	enum class NumericBase : unsigned char {
		BINARY = 2,
		OCTAL = 8,
		DECIMAL = 10,
		HEXADECIMAL = 16
	};

	struct ColorConfig {
		ForegroundColor fg;
		BackgroundColor bg;
	};

    template<typename TRAITS>
    class output_stream : public stream {
		/* Operator to write to assigned devices */
		friend output_stream & operator<< <>( output_stream &, const char * ) noexcept;

		/* Color management */
		/* TODO: Maybe redesign color management, see https://stackoverflow.com/a/17469726/5677080 */
		friend output_stream & operator<< <>( output_stream &, ColorConfig ) noexcept;
		friend output_stream & reset_colors <>( output_stream & ) noexcept;

		/* Numeric base manipulation */
		friend output_stream & numeric_base_prefix <>( output_stream & ) noexcept;

		/* TODO: Try to move the stuff arond numeric base out of the output_stream itself to avoid
		 * the friendship of so many classes */

		/* These manipulators manage used numberic base */
		friend output_stream & bin<>( output_stream & ) noexcept;
		friend output_stream & oct<>( output_stream & ) noexcept;
		friend output_stream & dec<>( output_stream & ) noexcept;
		friend output_stream & hex<>( output_stream & ) noexcept;

    public:
        using traits_type = TRAITS;

        template<typename... DEVICES>
        output_stream( DEVICES & ... devices ) noexcept : m_device( devices... ) {}

        /* TODO: Make this function private */
		NumericBase get_numeric_base( void ) const noexcept {
			return m_numeric_base;
		}
		/* TODO: Make those functions private */
		const char * get_numeric_prefix( void ) const noexcept {
			return m_numeric_prefix;
		}

		uint8_t get_fixed_length( void ) const noexcept {
			return m_fixed_lenght;
		}

		/* TODO: Resolve friendship of fixed_length manipulator and make this function private */
		void set_fixed_length( uint8_t length ) noexcept {
			m_fixed_lenght = length;
		}

    private:
        void write( const char * str, const ForegroundColor fg, const BackgroundColor bg ) noexcept {
			m_device.write( str, fg, bg );
		}

		void write( const char * str, const ForegroundColor fg ) noexcept {
			write( str, fg, m_bg );
		}

		void write( const char * str, const BackgroundColor bg ) noexcept {
			write( str, m_fg, bg );
		}

        void write( const char * str ) noexcept {
			write( str, m_fg, m_bg );
		}

		void set_numeric_base( const NumericBase base ) noexcept {
			m_numeric_base = base;
		}

		void set_numeric_prefix( const char * prefix ) noexcept {
			m_numeric_prefix = prefix;
		}

        traits_type::output_device m_device;

        ForegroundColor m_fg { traits_type::foreground_color };
        BackgroundColor m_bg { traits_type::background_color };

        uint8_t	m_fixed_lenght { 0 };

		/* TODO: Think about how to elegantly combine the numeric base and it's prefix into one
		 * Maybe a class template having the template parameter the NumericBase and having one member - the prefix */
		NumericBase m_numeric_base { NumericBase::DECIMAL };
		const char * m_numeric_prefix { "" };
    };


	inline ColorConfig set_colors( const ForegroundColor fg, const BackgroundColor bg ) noexcept {
		return { fg, bg };
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & reset_colors( output_stream<TRAITS> & ostream ) noexcept {
        /* FIXME: Rework hard coded values to get the ones defined in traits */
		ostream.m_fg = ForegroundColor( TRAITS::foreground_color );
		ostream.m_bg = BackgroundColor( TRAITS::background_color );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & numeric_base_prefix( output_stream<TRAITS> & ostream ) noexcept {
		ostream << ostream.get_numeric_prefix();
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & bin( output_stream<TRAITS> & ostream ) noexcept {
		ostream.set_numeric_base( NumericBase::BINARY );
		ostream.set_numeric_prefix( "0b" );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & oct( output_stream<TRAITS> & ostream ) noexcept {
		ostream.set_numeric_base( NumericBase::OCTAL );
		ostream.set_numeric_prefix( "0o" );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & dec( output_stream<TRAITS> & ostream ) noexcept {
		ostream.set_numeric_base( NumericBase::DECIMAL );
		ostream.set_numeric_prefix( "" );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & hex( output_stream<TRAITS> & ostream ) noexcept {
		ostream.set_numeric_base( NumericBase::HEXADECIMAL );
		ostream.set_numeric_prefix( "0x" );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & highlight( output_stream<TRAITS> & ostream ) noexcept {
		ostream << set_colors( ForegroundColor( Color::WHITE ), BackgroundColor( Color::BLACK ) );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, const char * str ) noexcept {
		ostream.write( str );
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, const bool value ) noexcept {
		if( value )
			ostream << set_colors( ForegroundColor( Color::GREEN ), BackgroundColor( Color::BLACK ) ) << "true" << reset_colors;
		else
			ostream << set_colors( ForegroundColor( Color::RED ), BackgroundColor( Color::BLACK ) ) << "false" << reset_colors;
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & message( output_stream<TRAITS> & ostream ) noexcept {
		ostream << "         ";
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & success( output_stream<TRAITS> & ostream ) noexcept {
		ostream << "[" << set_colors( ForegroundColor( Color::GREEN ), BackgroundColor( Color::BLACK ) ) << "  OK  " << reset_colors << "] ";
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & warning( output_stream<TRAITS> & ostream ) noexcept {
		ostream << "[" << set_colors( ForegroundColor( Color::YELLOW ), BackgroundColor( Color::BLACK ) ) << " WARN " << reset_colors << "] ";
		return ostream;
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & error( output_stream<TRAITS> & ostream ) noexcept {
		ostream << "[" << set_colors( ForegroundColor( Color::RED ), BackgroundColor( Color::BLACK ) ) << "FAILED" << reset_colors << "] ";
		return ostream;
	}

	template<uint8_t FIXED_LENGTH, typename TRAITS>
	inline output_stream<TRAITS> & fixed_length( output_stream<TRAITS> & ostream ) noexcept {
		ostream.set_fixed_length( FIXED_LENGTH );
		return ostream;
	}

	template<typename T, typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, T const * pointer ) noexcept {
		/* 32bit pointers are represented by fixed length of 8 ( 32bit = 4Bytes represented by 2 character each -> 8characters (numeric prefix excluded) ) */
		ostream << hex << fixed_length<8> << reinterpret_cast<uintptr_t>( pointer ) << fixed_length<0> << dec;
		return ostream;
	}

	/* TODO: Add an option to have fixed length numeric string, for instance instead of 0xFF write 0x000000FF - have fixed length of 8 */
	/* Specialization for all integral types (fulfilling the concept 'integral') */
	template<integral T, typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, T value ) noexcept {
		/* Once the value is negative, stream out minus sign. This makes sense only in decimal numeric base. */
		if( ( value < 0 ) && ( ostream.get_numeric_base() == NumericBase::DECIMAL ) ) ostream << "-";

		/* Make the value positive */
		typename make_unsigned<T>::type unsigned_value = ( value < 0 ) ? static_cast<typename make_unsigned<T>::type>( ~value + 1u ) : value;

		/* TODO: Replace by dynamic stack allocation - buffer size shall be equal to sizeof(T) * <number of characters to represent byte>
		 * This temporary solution uses the binary representation as the most digits consuming representation of numbers */
		char buffer[ sizeof( T ) * 8 ] = { 0 };
		uint8_t idx { 0 };

		/* Generate digits in reverse order */
		do {
			buffer[ idx++ ] = "0123456789ABCDEF"[ unsigned_value % to_underlying( ostream.get_numeric_base() ) ];
		} while( ( unsigned_value /= to_underlying( ostream.get_numeric_base() ) ) > 0 );

		/* Once fixed length is set to be higher than the current lenght of the string representation of integral value, extend the string by zeroes
		 * to reach required length */
		while( idx < ostream.get_fixed_length() ) {
			buffer[ idx++ ] = '0';
		}

		/* Revert the digits order */
		uint8_t i {0}, j {0};
		for( i = 0, j = ( idx - 1 ); i < j; i++, j-- ) {
			swap( buffer[i], buffer[j] );
		}
		/* Terminate the string */
		buffer[idx] = '\0';
		/* Stream out the converted string integral number representation including the numeric base prefix */
		return ostream << numeric_base_prefix << buffer;
	}

	/* Overload used to output stream manipulator functions
	 * See: https://stackoverflow.com/questions/213907/c-stdendl-vs-n */
	template<typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, output_stream<TRAITS> & (* function)( output_stream<TRAITS> & ) noexcept ) noexcept {
		return function( ostream );
	}

	template<typename TRAITS>
	inline output_stream<TRAITS> & operator<<( output_stream<TRAITS> & ostream, ColorConfig manipulator ) noexcept {
		ostream.m_fg = manipulator.fg;
		ostream.m_bg = manipulator.bg;
		return ostream;
	}

	/* Templated end of line stream manipulator */
	template<size_t N = 1, size_t INDEX = 0, typename TRAITS>
	constexpr output_stream<TRAITS> & endl( output_stream<TRAITS> & ostream ) noexcept {
		/* Insert N new lines */
		if constexpr ( INDEX < N - 1 ) {
			ostream << "\n" << endl<N,INDEX + 1>;
		}
		else {
			ostream << "\n";
		}
		/* Once the line is ended, the OutputStream is about to be reset - its color and numeric base */
		ostream << dec << reset_colors;
		return ostream;
	}

}