
#include "stream.hpp"

namespace ReVolta::Kernel {

    struct good_state_transition : public stream::stream_state::transition {
        good_state_transition( stream::stream_state::state & state ) noexcept : transition( make_observer( &state ) ) {}

        /* failed -> good */
        void operator()( stream::failed & ) noexcept {
            perform<stream::good>();
        }

        /* Invalid transitions */
        template<typename T>
        void operator()( T & ) noexcept {}
    };

    template<>
    void stream::switch_state<stream::good>( void ) noexcept {
        m_state.perform_transition<good_state_transition>();
    }

    void stream::clear( void ) noexcept {
        switch_state<stream::good>();
    }
}