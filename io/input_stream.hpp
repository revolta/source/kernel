#pragma once

#include "stream.hpp"
#include "output_stream.hpp"

namespace ReVolta::Kernel {

    template<typename TRAITS>
    class input_stream : public stream {

    };

    template<typename TRAITS>
    class input_output_stream : public input_stream<TRAITS>, public output_stream<TRAITS> {

    };

}