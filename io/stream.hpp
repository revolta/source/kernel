
#pragma once

#include "state_machine.hpp"

namespace ReVolta::Kernel {

#if flase
    enum class open_mode {
        BINARY,
        READ,
        WRITE,
        APPEND
    };
#endif

    class stream {
        friend class good_state_transition;

    public:
        enum class open_mode {
            BINARY,
            READ,
            WRITE,
            APPEND
        };

        bool is_good( void ) noexcept {
            return m_state.in_state<good>();
        }

        bool is_failed( void ) noexcept {
            return m_state.in_state<failed>();
        }

        bool is_eof( void ) noexcept {
            return m_state.in_state<eof>();
        }

        bool is_bad( void ) noexcept {
            return m_state.in_state<bad>();
        }

    protected:
        class good {};
        class failed {};
        class eof {};
        class bad {};

        /* FIXME: NEXT state must be a valid state machine state (must be either 'good', 'failed', 'eof' or 'bad' */
        template<typename NEXT_STATE>
        void switch_state( void ) noexcept;

        void clear( void ) noexcept;
        
    private:
        using stream_state = state_machine<good, failed, eof, bad>;

        stream_state m_state { initial_state<good> };

    };

}