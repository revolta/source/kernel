#include "framebuffer.hpp"
#include "control_registers/cr0.hpp"
#include "utility.hpp"

namespace ReVolta::Kernel {

	void Framebuffer::write( const char * str, const ForegroundColor foreground, const BackgroundColor background ) noexcept {
		size_t i { 0 };
		while( str[i] ) {
			write_char( str[i++], foreground, background );
		}
	}

	void Framebuffer::clear( ForegroundColor foreground, BackgroundColor background ) noexcept {
		/* Fill the whole framebuffer with blanks */
		for( unsigned int idx = 0; idx < ( m_cursor.get_width() * m_cursor.get_height() ); idx++ ) {
			this->get()[ idx ] = { 0x20, foreground, background };
		}
		m_cursor.reset();
	}

	void Framebuffer::write_char( const char c, const ForegroundColor foreground, const BackgroundColor background ) noexcept {
		switch( c ) {
		/* Carriage return special character */
		case '\r' :	m_cursor.cr();  break;

		/* New line (Line Feed) special character */
		case '\n' :	m_cursor.lf();  break;

		/* Backspace special character */
		case '\b' :	m_cursor.bs();  break;

		default :
			/* Check for printable characters */
			if( ( c >= 0x20 ) && ( c < 0x7F ) ) {
				this->get()[ m_cursor.get_framebuffer_index() ] = { c, foreground, background };
				/* Move the cursor by one position */
				m_cursor.next();
			}
		break;
		}
		/* Once the bottom of the screen is reached, scroll it */
		this->scroll( foreground, background );
	}

	void Framebuffer::scroll( const ForegroundColor foreground, const BackgroundColor background ) noexcept {
		if( m_cursor.is_scrolling_required() ) {
			/* Move the framebuffer content by one line upwards */
			for( unsigned int idx = 0; idx < ( m_cursor.get_width() * m_cursor.get_height() ); idx++ ) {
				this->get()[ idx ] = this->get()[ idx + m_cursor.get_width() ];
			}
			/* Fill the last line being "freed" by the blanks to make it empty */
			for( unsigned int idx = ( m_cursor.get_width() * ( m_cursor.get_height() - 1 ) ); idx < ( m_cursor.get_width() * m_cursor.get_height() ); idx++ ) {
				this->get()[ idx ] = { 0x20, foreground, background };
			}
			/* Move the cursor as well */
			m_cursor.scroll();
		}
	}

	void Framebuffer::Cursor::reset( void ) noexcept {
		this->set_coordinates( 0U, 0U );
	}

	void Framebuffer::Cursor::cr( void ) noexcept {
		coordinate_t y = m_y;
		this->set_coordinates( 0U, y );
	}

	void Framebuffer::Cursor::lf( void ) noexcept {
		coordinate_t y = m_y;
		this->set_coordinates( 0U, ++y );
	}

	void Framebuffer::Cursor::bs( void ) noexcept {
		coordinate_t x = m_x;
		coordinate_t y = m_y;
		this->set_coordinates( --x, y );
	}

	void Framebuffer::Cursor::next( void ) noexcept {
		coordinate_t x = m_x;
		coordinate_t y = m_y;
		this->set_coordinates( ++x, y);
	}

	bool Framebuffer::Cursor::is_scrolling_required( void ) const noexcept {
		return ( m_y >= m_textmode_screen_height ) ? true : false;
	}

	void Framebuffer::Cursor::scroll( void ) noexcept {
		coordinate_t x = m_x;
		coordinate_t y = m_y;
		this->set_coordinates( x, --y);
	}

	void Framebuffer::Cursor::set_coordinates( coordinate_t x, coordinate_t y ) noexcept {
		m_y = y;

		if( x >= m_textmode_screen_width ) {
			this->m_x = 0;
			this->m_y++;
		}
		else {
			this->m_x = x;
		}
		/* Set HW cursor */
		uint16_t idx = this->get_framebuffer_index();

		port_write_byte( 0x3D4, 0x0F );
		port_write_byte( 0x3D5, static_cast<uint8_t>( idx & 0xFF ) );
		port_write_byte( 0x3D4, 0x0E );
		port_write_byte( 0x3D5, static_cast<uint8_t>( (idx >> 8) & 0xFF ) );
	}

	Framebuffer::Character * Framebuffer::get( void ) const noexcept {
		return { reinterpret_cast<Character *>( ( cr0{}.pg == ENABLED ) ? 0xC00B8000 : 0xB8000 ) };
	}

}
