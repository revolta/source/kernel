/*
 * framebuffer.hpp
 *
 *  Created on: 24. 2. 2020
 *      Author: martin
 */

#pragma once

#include "cstddef.hpp"
#include "bits/color.hpp"

namespace ReVolta::Kernel {

	class Framebuffer {
	public:
		static constexpr bool supports_color_configuration { true };

		/* Write null-terminated ASCII string */
		void write( const char * str, const ForegroundColor foreground, const BackgroundColor background ) noexcept;

		void clear( ForegroundColor foreground, BackgroundColor background ) noexcept;

		/* Framebuffer as a kind of standard output device shall be configured using such generic structure applicable to all
		 * devices of such kind	 */
		Framebuffer( void ) noexcept : m_cursor( 80, 25 ) {
			/* Clear the buffer prior doing anything else */
			this->clear( ForegroundColor( Color::GRAY ), BackgroundColor( Color::BLACK ) );
		}

	private:
		class Character {
		public:
			Character( char code, ForegroundColor foreground, BackgroundColor background ) noexcept
				: m_character_code( code ), m_foreground( foreground ), m_background( background )
			{}

		private:
			Character( void ) = delete;

			unsigned char  m_character_code : 8;		/* bits 0..7 */
			ForegroundColor::type m_foreground : 4;		/* bits 8..11 */
			BackgroundColor::type m_background : 4;		/* bits 12..15 */
		};

		static_assert( sizeof( Character ) == 2, "character must be 16bit of size");

		class Cursor {
		public:
			Cursor( size_t screen_width, size_t screen_height )
				: m_textmode_screen_width( screen_width ),
				  m_textmode_screen_height( screen_height ),
				  m_x( 0U ), m_y( 0U )
			{}

			/* Move cursor to the origin [0,0] */
			void reset( void ) noexcept;

			/* Carriage return - "\r" */
			void cr( void ) noexcept;

			/* Line feed - Unix like OS interprets "\n" as LF - indicates a new line */
			void lf( void ) noexcept;

			/* Backspace */
			void bs( void ) noexcept;

			/* Move the cursor to the next position */
			void next( void ) noexcept;

			/* Recalculate the [x,y] coordinates to framebuffer offset */
			inline unsigned int get_framebuffer_index( void ) noexcept {
				return ( m_y * m_textmode_screen_width ) + m_x;
			}

			size_t const & get_width( void ) const noexcept { return m_textmode_screen_width; }

			size_t const & get_height( void ) const noexcept { return m_textmode_screen_height; }

			inline bool is_scrolling_required( void ) const noexcept;

			void scroll( void ) noexcept;

		private:
			size_t m_textmode_screen_width;
			size_t m_textmode_screen_height;

			using coordinate_t = unsigned char;

			void set_coordinates( coordinate_t x, coordinate_t y ) noexcept;

			coordinate_t m_x;
			coordinate_t m_y;
		};

		void write_char( const char c, const ForegroundColor foreground, const BackgroundColor background ) noexcept;

		void scroll( const ForegroundColor foreground, const BackgroundColor background ) noexcept;

		Character * get( void ) const noexcept;

		Cursor m_cursor;

	};
}
