/*
 * syscall.hpp
 *
 *  Created on: 7. 1. 2020
 *      Author: martin
 */

#include <sys/types.h>

#pragma once

namespace ReVolta::Kernel {

	enum class SyscallNr : unsigned int {
		FORK,
		EXIT,
		KILL,
		READ,
		WRITE,
		OPEN,
		CLOSE,
		MAX_SYSCALL_NR
	};

	template<SyscallNr SYSCALL> struct SyscallData;

	template<>
	struct SyscallData<SyscallNr::FORK> {
		pid_t pid;				/* Identifier of process created */
	} __attribute__((packed));

	template<>
	struct SyscallData<SyscallNr::EXIT> {
		int status;				/* Return value of process being exitted */
	} __attribute__((packed));

	template<>
	struct SyscallData<SyscallNr::KILL> {
		/* TODO: Define syscall status */
#if false
		enum class Status : unsigned char { /* T.B.D. */ };
		Status status;			/* Status */
#endif
		pid_t pid;				/* Identifier of process to be killed */
		int signal;				/* Signal to be sent to the process */
	} __attribute__((packed));

	template<>
	struct SyscallData<SyscallNr::READ> {
		enum class Status : unsigned char { SUCCESS, INVALID_FD, BUSY, ERROR };
		Status status;			/* Status */
		fd_t fd; 				/* File descriptor */
		void * buffer; 			/* Buffer to store read data */
		size_t count; 			/* Amount of bytes to read */
		size_t bytes_read; 		/* Amount of bytes read */
	} __attribute__((packed));

	template<>
	struct SyscallData<SyscallNr::WRITE> {
		enum class Status : unsigned char { SUCCESS, INVALID_FD, NOT_SUPPORTED, BUSY, ERROR };
		Status status;			/* Status */
		/* TODO: Shall it be FILE as this is the file descriptor type? */
		/* The values 0, 1, 2 can also be given, for standard input, standard output & standard error */
		fd_t fd; 				/* File descriptor which has been obtained from the call to open. */
		const void * buffer; 	/* Pointer to character array with content to be written to gived file descriptor */
		size_t size; 			/* Size of buffer to be written */
		size_t bytes_written;	/* Number of bytes successfully written into the array */
	} __attribute__((packed));

	template<>
	struct SyscallData<SyscallNr::OPEN> {
		enum class Status : unsigned char { SUCCESS, NOT_FOUND, BUSY, ERROR };
		Status status;			/* Status */
		fd_t fd;				/* File descriptor */
		const char * path;		/* Path name */
		int flags { 0 };		/* Flags */
	} __attribute__((packed));

	template<>
	struct SyscallData<SyscallNr::CLOSE> {
		enum class Status : unsigned char { SUCCESS, INVALID_FD, BUSY, ERROR };
		Status status;			/* Status */
		fd_t fd; 				/* File descriptor of file to be closed */
	} __attribute__((packed));

}
