#pragma once

#include "config_features.hpp"

namespace ReVolta::Kernel {

    template<>
	class config<Feature::MODULE> {
	public:
		struct Module {
			/* TODO: Replace by span<> */
			void * address;
			size_t size;
			const char * command_line;
		};

		class iterator {
		public:
			/* TODO: Shall the module here be of config<Feature::MODULE>::Module instead? */
			iterator( void * mod ) noexcept : m_entry( mod ) {}

			iterator & operator++( void ) noexcept;

			/* TODO: Shall it be returned by value or by reference? */
			const Module operator*( void ) const noexcept;

			bool operator!=( const iterator & other ) const noexcept {
				return m_entry != other.m_entry;
			}

		private:
			void * m_entry;
		};

		iterator begin( void ) const noexcept;

		iterator end( void ) const noexcept;
	};

}