#include "config.hpp"

#include "paging.hpp"
#include "paging_enabled.hpp"
#include "memory_map.hpp"
#include "frame_allocator.hpp"
#include "page.hpp"

namespace ReVolta::Kernel {

    sysconf::~sysconf( void ) noexcept {}

    unique_ptr<sysconf, forward_deleter<sysconf>> sysconf::m_self { nullptr };

    observer_ptr<sysconf> sysconf::get( void ) noexcept {
        if( is_paging_enabled() == true ) {
            return sysconf_mapping;
        } else {
            /* If m_self is nullptr, then the instance has not been created yet so let's make it allocated and constructed yet */
            if( m_self == nullptr ) {
                /* Get memory configuration from multiboot configuration provider */
				config<Feature::MEMORY> memory_config { multiboot::get_config<Feature::MEMORY>() };
                /* Allocate new sysconf instance by defined allocator for sysconf at the address provided by memory configuration */
                m_self = unique_ptr<sysconf, forward_deleter<sysconf>>{ allocator_traits<allocator<sysconf>>::allocate( allocator<sysconf>{}, 1, memory_config.head_frame().get() ) };

                /* As allocation by hint is used to forward the partition start address, the head frame must be manually updated/incremented to refer to the next frame
                 * and not to overwrite the frame where the sysconf itself is allocated */
                /* TODO: Find a better way to increment the head frame - maybe using the frame construction or something? */
                frame * updated_head_frame { memory_config.head_frame().release() };
                memory_config.head_frame().reset( updated_head_frame + 1 );

                /* Once m_self is valid... */
                if( m_self != nullptr ) {
                    /* ...default construct the sysconf at the m_self pointee location */
                    allocator_traits<allocator<sysconf>>::construct( allocator<sysconf>{}, m_self.get() );
                    /* Store memory configuration */
                    m_self->set_config<Feature::MEMORY>( move( memory_config ) );
                    /* TODO: Store other configurations */
                }
            }
            return make_observer( m_self );
        }
    }

    void sysconf::map( observer_ptr<page_directory> page_directory ) noexcept {
            /* Map the sysconf singleton instance to given fixed mapping address */
            page::map( page_directory, sysconf_mapping, Access::READ_WRITE, Mode::SUPERVISOR );
            /* NOTE: From this point on the m_self is nullptr as the ownership of sysconf object is sunken in the paging structures thanks
             * to the page::map() */ 
    }

}

namespace ReVolta {

    template<>
    void forward_deleter<Kernel::frame>::operator()( pointer ptr ) noexcept {
        deleter<allocator<Kernel::frame>>{}( ptr );
    }

    template<>
    void forward_deleter<Kernel::sysconf>::operator()( pointer ptr ) noexcept {
        deleter<allocator<Kernel::sysconf>>{}( ptr );
    }

}