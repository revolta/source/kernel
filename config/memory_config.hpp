#pragma once

#include "config_features.hpp"

#include <mutex.hpp>
#include <memory.hpp>
#include <span.hpp>

namespace ReVolta::Kernel {

	class frame;

	constexpr const size_t page_size { 4096 };

	template<>
	class config<Feature::MEMORY> {
		friend class multiboot;

		struct partition {
			enum class type : unsigned char {
				AVAILABLE		 = 1,
				RESERVED 		 = 2,
				ACPI_RECLAIMABLE = 3,
				NVS				 = 4,
				BAD_RAM			 = 5
			};

			/* Move the partition pointer to the first aligned address upwards from 'ptr' and adjust the partition 
			 * size accordingly */
			inline void shrink( void * ptr ) noexcept;

			span<frame> m_data;
			type m_type;
		};

		/* Inspired by https://www.justsoftwaresolutions.co.uk/threading/thread-safe-copy-constructors.html */
		config( config && other, const lock_guard<mutex> & ) noexcept
			:	m_partition( move( other.m_partition ) ),
				m_head_frame( move( other.m_head_frame ) )
		{}

	public:
		config( void ) noexcept = default;

		/* Delegate to private constructor which is fully protected against race conditions */
		config( config && other ) noexcept : config( move( other ), lock_guard<mutex>{ other.m_head_frame_mutex } ) {}

		/* TODO See: https://stackoverflow.com/a/29988626/5677080 */
		/* Move assignment operator */
		config & operator=( [[maybe_unused]] config && other ) noexcept {
			if( this != addressof( other ) ) {
				unique_lock<mutex> this_lock { m_head_frame_mutex, defer_lock };
				unique_lock<mutex> other_lock { other.m_head_frame_mutex, defer_lock };
				/* Lock the two mutexes simultaneously to prevent dead lock */
				lock( this_lock, other_lock );
				/* Move the data */
				m_partition = move( other.m_partition );
				m_head_frame = move( other.m_head_frame );
			}
			return *this;
		};

		/* Memory configuration is non copyable */
		config( const config & ) noexcept = delete;

		/* Memory configuration is non-copy-assignable */
		config & operator=( const config & ) noexcept = delete;

		/* All the accesses to head_frame() must be:
		 * 1) covered by locking the head frame mutex
		 * 2) checked for validity as 'head_frame() != nullptr'
		 */
		inline unique_ptr<frame, forward_deleter<frame>> & head_frame( void ) noexcept {
			return m_head_frame;
		}

		mutex & head_frame_mutex( void ) noexcept {
			return m_head_frame_mutex;
		}

		span<frame> get_partition( void ) const noexcept {
			return m_partition.m_data;
		}

		template<typename T>
		bool fits_partition( T * ptr ) const noexcept {
			return ( ( ptr >= m_partition.m_data.begin().get() ) && ( ptr < m_partition.m_data.end().get() ) );
		}

	private:

		/* Iterator implementation is multiboot specific - is capable to iterate over multiboot header custom structures */
		class iterator {
		public:
			iterator( void * memory_map ) noexcept;
			
			iterator & operator++( void ) noexcept;
			
			/* TODO: Shall it be returned by value or by reference? */
			const partition operator*( void ) const noexcept;
			
			bool operator!=( const iterator & other ) const noexcept {
				return m_entry != other.m_entry;
			}

		private:
			void * m_entry;
		};

		/* NOTE: Function must be instantiated within multiboot as it contains Multiboot specific code */
		iterator begin( void ) const noexcept;

		/* NOTE: Function must be instantiated within multiboot as it contains Multiboot specific code */
		iterator end( void ) const noexcept;

		partition m_partition;

		unique_ptr<frame, forward_deleter<frame>> m_head_frame { nullptr };

		mutable mutex m_head_frame_mutex;
	};

}