#pragma once

#include "config_features.hpp"

namespace ReVolta::Kernel {

	struct multiboot {
		static bool is_valid( void ) noexcept;

		template<Feature T> static config<T> get_config( void ) noexcept;
	};

}
