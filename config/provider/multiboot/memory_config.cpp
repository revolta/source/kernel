#include "memory_config.hpp"
#include "module_config.hpp"
#include "multiboot.hpp"
#include "multiboot_header.h"
#include "frame.hpp"

/* Multiboot info structure initialized in boot.S */
extern multiboot_info_t * multiboot_info;

namespace ReVolta::Kernel {

    config<Feature::MEMORY>::iterator::iterator( void * memory_map ) noexcept : m_entry( memory_map ) {}

	config<Feature::MEMORY>::iterator & config<Feature::MEMORY>::iterator::operator++( void ) noexcept {
		m_entry = static_cast<multiboot_memory_map_t *>( m_entry ) + 1;
		return *this;
	}

	const config<Feature::MEMORY>::partition config<Feature::MEMORY>::iterator::operator*( void ) const noexcept {
		multiboot_memory_map_t * entry { static_cast<multiboot_memory_map_t *>( m_entry ) };
		return partition { 
			.m_data = { reinterpret_cast<frame *>( entry->addr_low ), static_cast<size_t>( entry->len_low ) / sizeof( frame ) },
			.m_type = static_cast<typename partition::type>( entry->type )
		};
	}

	config<Feature::MEMORY>::iterator config<Feature::MEMORY>::begin( void ) const noexcept {
		return { reinterpret_cast<void *>( multiboot_info->mmap_addr ) };
	}

	config<Feature::MEMORY>::iterator config<Feature::MEMORY>::end( void ) const noexcept {
		return { reinterpret_cast<void *>( ( reinterpret_cast<multiboot_memory_map_t *>( multiboot_info->mmap_addr )) + ( multiboot_info->mmap_length / sizeof( multiboot_memory_map_t ) ) ) };
	}

	void config<Feature::MEMORY>::partition::shrink( void * ptr ) noexcept {
		/* Calculate the distance between the given 'ptr' and partition's address */
		const ptrdiff_t diff = ( reinterpret_cast<uintptr_t>( ptr ) - reinterpret_cast<uintptr_t>( m_data.data() ) ) / sizeof( frame );
		/* Calculate the shrinked partition parameters. It must be done using local variables as it goes later to alignment correction */
		void * _address { m_data.data() + diff };
		size_t  _size { m_data.size() - diff };

		/* Properly align */
		if( !is_aligned<page_size>( _address ) ) {
			align( page_size, page_size, _address, _size );
		}

		m_data = { reinterpret_cast<frame *>( _address ), _size };
	}

	/* FIXME: Is duplicit to the same in bootstrap.cpp */
	extern "C" frame _bootstrap_start;
	extern "C" frame _bootstrap_end;

	template<>
	config<Feature::MEMORY> multiboot::get_config<Feature::MEMORY>( void ) noexcept {
		config<Feature::MEMORY> cfg;

		if( multiboot_info->flags & MULTIBOOT_INFO_MEM_MAP ) {
			/* Iterate through all the memory partitions to identify the one suitable */
			for( config<Feature::MEMORY>::partition partition : cfg ) {
				if( ( partition.m_type == config<Feature::MEMORY>::partition::type::AVAILABLE ) && ( reinterpret_cast<uintptr_t>( partition.m_data.data() ) >= 1_MB ) ) {
					/* Suitable partition identified so make it the one used */
					cfg.m_partition = partition;
				}
			}
			/* If bootstrap binary is loaded in within the partition being analysed, move the Frame allocable range right after the bootstrap binary aligned upwards to page size */
			if( &_bootstrap_end >= cfg.m_partition.m_data.data() ) {
				cfg.m_partition.shrink( &_bootstrap_end );
			}
			/* Additionally, check whether there are some modules within the selected partion which occupy some address space considered as free to use */
			for( config<Feature::MODULE>::Module mod : get_config<Feature::MODULE>() ) {
				if( mod.address >= cfg.m_partition.m_data.data() ) {
					cfg.m_partition.shrink( reinterpret_cast<void *>( reinterpret_cast<uintptr_t>( mod.address ) + mod.size ) );
				}
			}
			
			cfg.m_head_frame.reset( cfg.m_partition.m_data.data() );
		}
		else {
			/* FIXME: This reaction is not appropriate as just writing to error stream is not yet working in here (function is called before bootstrap_main and thus prior
			 * running global constructors ) */
		}
		return cfg; 
	}

}