#include "multiboot.hpp"
#include "multiboot_header.h"

/* Multiboot info structure initialized in boot.S */
multiboot_info_t * multiboot_info;

/* Multiboot magic number - bootloader identification */
multiboot_uint32_t multiboot_magic;

namespace ReVolta::Kernel {

	bool multiboot::is_valid( void ) noexcept {
		return ( multiboot_magic == MULTIBOOT_BOOTLOADER_MAGIC );
	}

}
