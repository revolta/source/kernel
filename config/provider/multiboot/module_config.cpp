#include "module_config.hpp"
#include "multiboot.hpp"
#include "multiboot_header.h"

/* Multiboot info structure initialized in boot.S */
extern multiboot_info_t * multiboot_info;

namespace ReVolta::Kernel {

    config<Feature::MODULE>::iterator & config<Feature::MODULE>::iterator::operator++( void ) noexcept {
		m_entry = static_cast<multiboot_module_t *>( m_entry ) + 1;
		return *this;
	}

	const config<Feature::MODULE>::Module config<Feature::MODULE>::iterator::operator*( void ) const noexcept {
		multiboot_module_t * entry { static_cast<multiboot_module_t *>( m_entry ) };
		return { reinterpret_cast<void *>( entry->mod_start ), ( entry->mod_end - entry->mod_start), reinterpret_cast<const char *>( entry->cmdline ) };
	}

	config<Feature::MODULE>::iterator config<Feature::MODULE>::begin( void ) const noexcept {
		return { reinterpret_cast<void *>( multiboot_info->mods_addr ) };
	}

	config<Feature::MODULE>::iterator config<Feature::MODULE>::end( void ) const noexcept {
		return { reinterpret_cast<void *>( ( reinterpret_cast<multiboot_module_t *>( multiboot_info->mods_addr )) + multiboot_info->mods_count ) };
	}

	template<>
	config<Feature::MODULE> multiboot::get_config<Feature::MODULE>( void ) noexcept {
		if( multiboot_info->flags & MULTIBOOT_INFO_MODS ) {
			/* Modules avaiable */
		}
		else {
			/* TODO: No modules available */
		}
		return {};
	}

}