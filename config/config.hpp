#pragma once

#include "memory_config.hpp"
#include "module_config.hpp"

#include "provider/multiboot/multiboot.hpp"

#include "memory.hpp"
#include "utility.hpp"

namespace ReVolta::Kernel {

	/* TODO: Move to process config? */
    constexpr const size_t max_process_count { 1024 };
	
	/* TODO: Move to thread config? */
	constexpr const size_t max_thread_count { 1024 };
	
	/* TODO: Move to cpu config? */
	constexpr const size_t max_cpu_count { 16 };

	/* TODO: Constrain by concept - check for availability of get_config<Feature>() and is_valid() */
	template<typename T>
	concept ConfigProvider = true;

	/* tag<T> alias */
	template<ConfigProvider T>
	inline constexpr tag_type<T> config_provider {};

	/* config_collection tail expansion */
	template<Feature... FEATURES>
	class config_collection {
	public:
		config_collection( void ) {}

		template<typename CONFIG_PROVIDER>
		config_collection( tag_type<CONFIG_PROVIDER> ) noexcept {}
	};

	template<Feature FEATURE, Feature... OTHERS>
	class config_collection<FEATURE, OTHERS...> : public config_collection<OTHERS...> {
	public:
		/* Default constructor */
		config_collection( void ) noexcept {}

		/* NOTE: In fact this is a templated constructor. As it is not possible to specify template arguments when calling
		 * the constructor template, it has to be deduced through contructor argument deduction. The ConfigProvider<CONFIG_PROVIDER>
		 * type is just a variable with no state besides the type it carries.
		 * See: https://stackoverflow.com/a/31616949/5677080 */
		 
		/* TODO: Extend to have variadic CONFIG_PROVIDERS to have the option to define more than one configuration provider. Then 
		 * search at compile time which provider provides which kind of configuration (the function get_config<> is defined within or not),
		 * maybe using something analogical to 'is_supported' implemented in feature.hpp */
		template<typename CONFIG_PROVIDER>
		config_collection( tag_type<CONFIG_PROVIDER> ) noexcept
			:	/* Call base class' constructors */
				config_collection<OTHERS...>( tag<CONFIG_PROVIDER> ),
				/* Initialize the feature configuration using the configuration provider */
				m_feature_config( CONFIG_PROVIDER::template get_config<FEATURE>() )
		{}

		config_collection( const config<FEATURE> & feature_config, const config<OTHERS> &... others ) noexcept
			:	config_collection<OTHERS...>( others... ),
				m_feature_config( feature_config )
		{}

		template<Feature REQUESTED_FEATURE, typename... DETAILS>
		inline config<REQUESTED_FEATURE> & get_config( const DETAILS... details ) noexcept {
			if constexpr ( REQUESTED_FEATURE == FEATURE ) {
				if constexpr ( sizeof...( DETAILS ) == 0 ) {
					return m_feature_config;
				} else {
					return m_feature_config.get( details... );
				}			
			} else {
				return config_collection<OTHERS...>::template get_config<REQUESTED_FEATURE>( details... );
			}
		}

#if false
		template<Feature STORED_FEATURE>
		inline void set_config( const config<STORED_FEATURE> & config ) noexcept {
			if constexpr ( STORED_FEATURE == FEATURE ) {
				m_feature_config = config;
			} else {
				config_collection<OTHERS...>::set_config( config );
			}
		}
#else
		template<Feature STORED_FEATURE>
		inline void set_config( config<STORED_FEATURE> && config ) noexcept {
			if constexpr ( STORED_FEATURE == FEATURE ) {
				m_feature_config = move( config );
			} else {
				config_collection<OTHERS...>::set_config( move( config ) );
			}
		}
#endif

	private:
		config<FEATURE> m_feature_config;
	};

	/* Forward declare frame allocator which is used below to define default allocator for sysconf */
	template<typename> class frame_allocator;

	class page_directory;

	/* TODO: Isn't the config_collection<> similar implementation to tuple<>? May the tuple<> be used intead? */
	struct alignas( page_size ) sysconf : public config_collection<Feature::MEMORY, Feature::MODULE> {
		/* Singleton instance accessor */
		static observer_ptr<sysconf> get( void ) noexcept;

		/* Non-copyable */
		sysconf( const sysconf & ) noexcept = delete;

		/* Non-copy-assignable */
		sysconf & operator=( const sysconf & ) noexcept = delete;

		/* TODO: Find better name more descriptive one */
		void map( observer_ptr<page_directory> page_directory ) noexcept;

		~sysconf( void ) noexcept;

	private:
		/* Let the 'construct_at' function template access the private default constructor */
		/* FIXME: This is potential security flaw - it allows to construct sysconf from everywhere by just using the construct_at
		 * function template which effectively brings the private default constructor to public scope 
		 * --> maybe to use placement new direclty instead? */
		friend constexpr sysconf * construct_at<>( sysconf * ) noexcept;

		sysconf( void ) noexcept {}

	/* FIXME m_self could not be public */
	public:
		static unique_ptr<sysconf, forward_deleter<sysconf>> m_self;
	};

	/* Check the correct sysconf size */
	static_assert( sizeof( sysconf ) == page_size );

}

namespace ReVolta {

	template<>
	struct default_allocator<Kernel::sysconf> {
		using type = Kernel::frame_allocator<Kernel::sysconf>;
	};

}