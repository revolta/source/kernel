#pragma once

#include "cstddef.hpp"
#include "cstdint.hpp"

namespace ReVolta::Kernel {

	/* TODO: Thinking about how to make enum iterable for automatic expansion in tuple instantiations, see
	 * http://loungecpp.wikidot.com/tips-and-tricks:indices */

	enum class Feature : unsigned char {
		OUTPUT,
		MEMORY,
		MODULE,
	};

    template<Feature T> class config;

}