#include "process.hpp"

namespace ReVolta::Kernel {

    /* CRTP base class implementing the functions of process management to be used in bootstrap and kernel */
    template<typename T>
    class process_management {
    public:
        ~process_management( void ) noexcept = default;

        /* Pointer to currently active process */
        using process_iterator = forward_list<process>::iterator;

        process_iterator get_process( const process::pid_type pid ) noexcept {
            process_iterator it = m_processes.begin();
            /* TODO: Maybe other container than forward_list might be used which supports
             * faster search within - maybe hash map to speed up the search in case of many processes */
            while( ( it->get_pid() != pid ) || ( it == m_processes.end() ) ) { it++; }
            return it;
        }

        inline process_iterator get_process( const process::reserved_pid pid ) noexcept {
            return get_process( to_underlying( pid ) );
        }

        /* Fork nothing means to create initial process. The CPU is required to create initial page directory */
        process & fork( unique_ptr<cpu> cpu ) noexcept {
            return m_processes.emplace_front( move( process{ move( cpu ) } ) );
        }

        process & fork( unique_ptr<cpu> cpu, const process::pid_type pid ) noexcept {
            return m_processes.emplace_front( get_process( pid )->fork( move( cpu ) ) );
        }

        process & fork( unique_ptr<cpu> cpu, process & other ) noexcept {
            return m_processes.emplace_front( other.fork( move( cpu ) ) );
        }

        /* TODO: When a process completes, it invokes the exit system call, thus entering the states "kernel running" and, finally, the "zombie" state */
        void exit( void ) noexcept {
            /* Enter process zombie state to prepare it for destruction */
            m_current_process->exit();
            /* Destroy current process - erase the instance form the process list */
            /* Set an interator pointing to the first element in process list */
            process_iterator it = m_processes.begin();
            /* TODO: Maybe other container than forward_list might be used which supports
             * faster search within - maybe hash map to speed up the search in case of many processes */
            /* Iterate over all the processes to find the one which holds the one being removed as the next one */
            while( ( it.get_next() != m_current_process ) || ( it.get_next() == m_processes.end() ) ) { it++; }
            /* Erase the process being the next one to the process referenced by 'it' */
            m_processes.erase_after( it );
        }

        process & get_current_process( void ) noexcept {
            return *m_current_process;
        }
   
    protected:
        process_management( void ) noexcept {}

        forward_list<process> & process_list( void ) noexcept {
            return m_processes;
        }

        /* TODO: Shall be rather held in scheduler? */
        static process_iterator m_current_process;

    private:
        /* List structure to manage the existing processes */
        /* TODO: Think about the container type. Alternative is list (double linked list) having the option to directly reference the elements by the iterator.
         * So it is the question whether to have more memory space consuming container but faster or slightly more memory friendly but requiring looping through the whole one */
        /* TODO: Maybe other container than forward_list might be used which supports
         * faster search within - maybe hash map to speed up the search in case of many processes */
        forward_list<process> m_processes;
    };

}