#include "process.hpp"
#include "cpu.hpp"
#include "forward_list.hpp"
#include "memory.hpp"

namespace ReVolta {

    namespace Kernel {

        process::process( unique_ptr<cpu> cpu ) noexcept 
            :   /* The only process being created from scratch is the bootstrap process */
                m_pid{ to_underlying( reserved_pid::BOOTSTRAP ) },
                /* Create empty process context - just to create the structures within */
                m_context{},
                /* Assign the CPU to construct the process */
                m_cpu{ move( cpu ) },
                /* Set invalid program break - no binary defined yet. Valid program break is set in execute() function */
                m_program_break{ nullptr },
                /* Set the process to 'created' state */
                m_state{ initial_state<created> }
        {
            /* Once the process is constructed, the CPU remains assigned while staying in 'created' state. To get
             * to ready in memory state, the CPU must be returned back to the scheduler which shall call the process.ready()
             * function */
        }

        process::process( unique_ptr<cpu> cpu, clone_current_context_t ) noexcept
            :   m_pid{ generate_pid() },
                m_context{ clone_current_context },
                /* Assign the CPU to construct the process */
                m_cpu{ move( cpu ) },
#if false
                /* Set program break */
                m_program_break{ other.m_program_break },
#else
                m_program_break{ nullptr },
#endif
                /* Once the process is copied, its creation is finished, not assigned, thus it is waiting for execution in ready_memory state */
                m_state{ initial_state<created> }
        {}

#if false
        unique_ptr<cpu> process::execute( span<frame> process_image, [[maybe_unused]] char * const argv [] ) noexcept {
        }


        unique_ptr<cpu> process::execute( const char * path, char * const argv [], char * const envp [] ) noexcept {
            
        }
#endif

        /* Process storage where all the process instances are held within memory pool, managed by forward list owned by 
         * either boostrap or kernel - the derived classes of process_management CRTP base */
        memory_pool<forward_list_node<process>> process_storage;

    }

    template<>
    memory_pool<forward_list_node<Kernel::process>> & pool_allocator<forward_list_node<Kernel::process>>::get_pool( void ) noexcept {
        return Kernel::process_storage;
    }

}