#pragma once

#include "process_state_machine.hpp"
#include "memory_pool.hpp"
#include "forward_list.hpp"
#include "span.hpp"
#include "memory.hpp"
#include "cpu.hpp"
#include "config.hpp"
#include "limits.hpp"

namespace ReVolta::Kernel {

    /* TODO: How about to implement static interfaces to give access to only certain functional groups of quite
     * big and feature rich process - to divide it into:
     * - process management - creation / fork etc.
     * - process execution - run / preempt / ready etc.
     * - process metadata - PID etc.
     */
    class process : public Detail::process_state_machine<process> {
        /* The states shall have an access to private properties of the process */
        friend class Detail::process_state_machine<process>;
        /* Process management is allowed to create the process from scratch */
        template<typename T>
        friend class process_management;

    public:
        using pid_type = size_t;

        enum class reserved_pid : pid_type {
            BOOTSTRAP   = 0,
            INIT        = 1,
            INVALID     = numeric_limits<pid_type>::max()
        };

        /* Pure copy constructor is explicitly deleted as being substituted by private one having CPU as other parameter */
        process( const process & ) noexcept = delete;

        /* Move constructor - to have the ability to manipulate the process itself avoiding unwanted copies */
        process( process && other ) noexcept = default;

        /* Default destructor */
        ~process( void ) noexcept = default;

        /* Move assignment operator */
        process & operator=( process && other ) noexcept = default;

        inline pid_type get_pid( void ) const noexcept {
            return m_pid;
        }

        /* Fork/copy self */
        template<typename CPU>
        inline process fork( unique_ptr<cpu<CPU>> cpu ) noexcept {
            return process{ move( cpu ), clone_current_context };
        }
        
#if false
        unique_ptr<cpu> execute( span<frame> process_image, char * const argv [] = nullptr ) noexcept;

        /* TODO: Replace the type of 'path' to filesystem implementation object equal to path once available */
        unique_ptr<cpu> execute( const char * path, char * const argv [] = nullptr, char * const envp [] = nullptr ) noexcept;
#endif

        /* TODO: When a process completes, it invokes the exit system call, thus entering the states "kernel running" and, finally, the "zombie" state */
        template<typename CPU>
        inline unique_ptr<cpu<CPU>> exit( void ) noexcept {
            /* If the process is in kernel_running state (checked internally), switch it to zombie state being ready to be destructed */
            this->switch_state<zombie>();
            return move( m_cpu );
        }

        /* By providing the CPU as resource the process is allowed to run it's thread(s) */
        template<typename CPU>
        inline void run( unique_ptr<cpu<CPU>> cpu ) noexcept {
            m_cpu = move( cpu );
            this->switch_state<kernel_running>( *this );
        }

        /* Move the process into preempted state and request the CPU to be returned back */
        template<typename CPU>
        inline unique_ptr<cpu<CPU>> preempt( void ) noexcept {
            this->switch_state<preempted>();
            return move( m_cpu );
        }

        /* Move the process into ready in memory state and request the CPU to be returned back */
        template<typename CPU>
        inline unique_ptr<cpu<CPU>> ready( void ) noexcept {
            this->switch_state<ready_memory>();
            return move( m_cpu );
        }

        inline bool is_ready( void ) noexcept {
            return this->m_state.in_state<ready_memory>();
        }

        /* TODO: Shall not be compiled into production code */
        const char * get_state_string( void ) const noexcept {
            if( this->in_state<created>() ) return "[created]";
            if( this->in_state<ready_memory>() ) return "[ready in memory]";
            if( this->in_state<ready_swapped>() ) return "[ready swapped]";
            if( this->in_state<kernel_running>() ) return "[kernel running]";
            if( this->in_state<user_running>() ) return "[user running]";
            if( this->in_state<preempted>() ) return "[preempted]";
            if( this->in_state<asleep_memory>() ) return "[asleep in memory]";
            if( this->in_state<asleep_swapped>() ) return "[asleep swapped]";
            if( this->in_state<zombie>() ) return "[zombie]";
            return "[unknown]";
        }

        const process_context & get_context( void ) const noexcept {
            return m_context;
        }

    private:
        static pid_type generate_pid( void ) noexcept {
            static pid_type pid { to_underlying( reserved_pid::INIT ) };
            return pid++;
        }

        /* Construct process from scratch. The CPU is required to create process context */
        template<typename CPU>
        process( unique_ptr<cpu<CPU>> cpu ) noexcept;

        template<typename CPU>
        process( unique_ptr<cpu<CPU>> cpu, clone_current_context_t ) noexcept;

        /* Let's use default copy assignment operator */
        process & operator=( const process & other ) noexcept = default;

    protected:
        /* Process identifier */
        const pid_type m_pid { to_underlying( reserved_pid::INVALID ) };

        /* Process context holding all the data exchanged during process switch */
        process_context m_context;

        /* TODO: Shall the CPU be generic resource handled by the process or shall be maintained only in several states */
        /* TODO: Is it a design choice the process may have more than one CPU assigned? If so, this must be an array of pointers to CPU,
         * or shall be organized in forward_list somehow to know the amount of assigned CPUs, retrieve them easily and return them back... */
        unique_ptr<cpu> m_cpu { nullptr };

        /* The value of program break depends on process executable image size */
        observer_ptr<page> m_program_break { nullptr };
        
        /* Process state - the state machine variable */
        process_state m_state;
    };

}

namespace ReVolta {

    template<>
    struct memory_pool_traits<forward_list_node<Kernel::process>> {
        using element_type = forward_list_node<Kernel::process>;
        static constexpr size_t capacity { Kernel::max_process_count };
    };

    /* Define the default allocator for process */
    /* NOTE: The processes are not allocated directly. They're organized in forward_list and thus allocated as member of forward_list_node,
     * so internally the allocator is rebound to 'pool_allocator<foward_list_node<process>>' */
    template<>
    struct default_allocator<Kernel::process> {
        using type = pool_allocator<Kernel::process>;
    };

}