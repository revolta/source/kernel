#pragma once

#include "paging.hpp"
#include "state_machine.hpp"

namespace ReVolta::Kernel::Detail {

    template<typename PROCESS>
    class process_state_machine {
    private:
        class ready_memory_transition;
        class ready_swapped_transition;
        class kernel_running_transition;
        class user_running_transition;
        class preempted_transition;
        class asleep_memory_transition;
        class asleep_swapped_transition;
        class zombie_transition;

    protected:
        process_state_machine( void ) noexcept = default;

        /* TODO: If the design decision is made to store the member data in corresponding states locally, the way to share the data
         * among several states is by inheritance - hold the data in base class out of which the sharing states are derived */

        /* The process is newly created and is in a transition state; the process exists,
         * but it is not ready to run, nor is it sleeping. This state is the start state for
         * all processes except process 0 */
        class created {
        public:
            using transition = void;

            created( void ) noexcept {}

            ~created( void ) noexcept {}
        };

        /* The process is not executing but is ready to run as soon as the kernel schedules it */
        class ready_memory {
        public:
            using transition = ready_memory_transition;

            ready_memory( void ) noexcept {}

            ~ready_memory( void ) noexcept {}
        };

        /* Process is ready to run, swapped out, and must be swapped in before the kernel
         * can schedule it to execute */
        class ready_swapped {
        public:
            using transition = ready_swapped_transition;

            ready_swapped( void ) noexcept {}

            ~ready_swapped( void ) noexcept {}
        };

        /* Process is running in kernel mode */
        class kernel_running {
        public:
            using transition = kernel_running_transition;

            kernel_running( PROCESS & p ) noexcept {
                /* Once the process is requested to run, the context shall be restored */
                /* Check the validity of CPU assigned */
                if( p.m_cpu != nullptr ) {
    /* FIXME */                    
#if false                    
                    /* Restore process context */ 
                    p.m_cpu->restore_process_context( p.m_context );
#endif
                } else {
                    /* FIXME: What to do once no CPU is available? */
                }
            }

            ~kernel_running( void ) noexcept {}
        };

        /* Process is running in user mode */
        class user_running {
        public:
            using transition = user_running_transition;

            user_running( void ) noexcept {}

            ~user_running( void ) noexcept {}
        };

        /* The process is returning from the kernel to user mode, but the kernel preempts it
         * and does a context switch to schedule another process.
         * The state "preempted" is really the same as the state "ready to run in memory",
         * but they are depicted separately to stress that a process executing in kernel mode 
         * can be preempted only when it is about to return to user mode. Consequently, 
         * the kernel could swap a process from the state "preempted" if necessary.*/
        /* TODO: How to express the equivalence? By inheritance which represents a "is-a" relationship? 
         * class preempted : public ready_memory {} */
        class preempted {
        public:
            using transition = preempted_transition;

            preempted( void ) noexcept {}

            ~preempted( void ) noexcept {}
        };

        /* Process is sleeping and resides in memory */
        class asleep_memory {
        public:
            using transition = asleep_memory_transition;

            asleep_memory( void ) noexcept {}

            ~asleep_memory( void ) noexcept {}
        };

        /* The proccss is steeping, and the swapper has swapped the process to secondary storage 
         * to make room for other processes in main memory. */
        class asleep_swapped {
        public:
            using transition = asleep_swapped_transition;

            asleep_swapped( void ) noexcept {}

            ~asleep_swapped( void ) noexcept {}
        };

        /* The proccss executed the exit system call and is in the zombie state. The process no 
         * longer exists, but it leaves a record containing an exit code and maybe some timing 
         * statistics for its parent process to collect. The zombie state is the final state of a process */
        class zombie {
        public:
            using transition = zombie_transition;

            zombie( void ) noexcept {}

            ~zombie( void ) noexcept {}
        };

        /** Available process states:
         * 'created'
         * 'ready_memory'
         * 'ready_swapped'
         * 'kernel_running'
         * 'user_running'
         * 'preempted'
         * 'asleep_memory'
         * 'asleep_swapped'
         * 'zombie'
         */
        /* TODO: Contrain NEXT_STATE to be only the state machine's valid state only.
         * Isn't it already checked within the state machine itself? */
        /* TODO: Can it be moved to state machine itself? */
        template<typename NEXT_STATE, typename... ARGUMENTS>
        inline void switch_state( ARGUMENTS &&... arguments ) noexcept {
            static_cast<PROCESS *>( this )->m_state.template perform_transition<typename NEXT_STATE::transition>( forward<ARGUMENTS>( arguments )... );
        }

        template<typename STATE>
        inline bool in_state( void ) const noexcept {
            return static_cast<const PROCESS *>( this )->m_state.template in_state<STATE>();
        }

        using process_state = state_machine<created, ready_memory, ready_swapped, kernel_running, user_running, preempted, asleep_memory, asleep_swapped, zombie>;
        
    private:
        /* Transitions to 'ready_memory' state */
        struct ready_memory_transition : public process_state::transition {
            ready_memory_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* There is enough memory available. Transition 'created' -> 'ready_memory' */
            template<typename... ARGUMENTS>
            void operator()( created &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<ready_memory>( forward<ARGUMENTS>( arguments )... );
            }

            /* Wake up of process fallen asleep in memory */
            template<typename... ARGUMENTS>
            void operator()( asleep_memory &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<ready_memory>( forward<ARGUMENTS>( arguments )... );
            }

            /* Swap in - switch process from swapped to memory */
            template<typename... ARGUMENTS>
            void operator()( ready_swapped &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<ready_memory>( forward<ARGUMENTS>( arguments )... );
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct ready_swapped_transition : public process_state::transition {
            ready_swapped_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* There is not enough memory available. Transition 'created' -> 'ready_swapped' */
            template<typename... ARGUMENTS>
            void operator()( created &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<ready_swapped>( forward<ARGUMENTS>( arguments )... );
            }

            /* Wake up of process fallen asleep swapped */
            template<typename... ARGUMENTS>
            void operator()( asleep_swapped &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<ready_swapped>( forward<ARGUMENTS>( arguments )... );
            }

            /* Swap out - switch process from memory to swap */
            template<typename... ARGUMENTS>
            void operator()( ready_memory &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<ready_swapped>( forward<ARGUMENTS>( arguments )... );
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct kernel_running_transition : public process_state::transition {
            kernel_running_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            template<typename... ARGUMENTS>
            void operator()( ready_memory &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<kernel_running>( forward<ARGUMENTS>( arguments )... );
            }

            template<typename... ARGUMENTS>
            void operator()( user_running &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<kernel_running>( forward<ARGUMENTS>( arguments )... );
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct user_running_transition : public process_state::transition {
            user_running_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* Return from syscall event */
            template<typename... ARGUMENTS>
            void operator()( kernel_running &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<user_running>( forward<ARGUMENTS>( arguments )... );
            }

            /* Return to user from preempted state */
            template<typename... ARGUMENTS>
            void operator()( preempted &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<user_running>( forward<ARGUMENTS>( arguments )... );
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct preempted_transition : public process_state::transition {
            preempted_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* Preempt */
            template<typename... ARGUMENTS>
            void operator()( kernel_running &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<preempted>( forward<ARGUMENTS>( arguments )... );
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct asleep_memory_transition : public process_state::transition {
            asleep_memory_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* Sleep */
            template<typename... ARGUMENTS>
            void operator()( kernel_running &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<asleep_memory>();
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct asleep_swapped_transition : public process_state::transition {
            asleep_swapped_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* Swap out - switch process from memory to swap */
            template<typename... ARGUMENTS>
            void operator()( asleep_memory &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<asleep_swapped>();
            }
            
            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };

        struct zombie_transition : public process_state::transition {
            zombie_transition( process_state::state & state ) noexcept : process_state::transition( make_observer( addressof( state ) ) ) {}

            /* Exit */
            template<typename... ARGUMENTS>
            void operator()( kernel_running &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<zombie>();
            }

            /* Once something happens in process creation, there is an allowed swap directly to zombie, e.g. it was unable to load
             * process image */
            template<typename... ARGUMENTS>
            void operator()( created &, ARGUMENTS &&... arguments ) noexcept {
                this->template perform<zombie>();
            }

            /* Invalid transitions */
            template<typename T, typename... ARGUMENTS>
            void operator()( T &, ARGUMENTS &&... ) noexcept {}
        };
    
    };

}